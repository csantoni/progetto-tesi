'''
Created on 20/mag/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot
import os
import winsound

n_N = {1:0, 2:0 ,3:0 ,4:0}
N_data = {}
for seg in n_N.keys():
    N_data[seg] = None

base_path = '../res/tele-aliens/data/'
N_max = np.load(base_path + 'n2N_max.npy')
for v in N_max:
    n_N[v[0]] = v[1] + 1
    
for seg in n_N.keys():
    N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
    
n_R = 2

r_val = np.array( (0, 1, 0 ,1 , 0, 0, 0) )

p_R = mc.Dirichlet( name ='p_R', theta = [1.0,] * n_R, trace = True, plot = True)
#p_R = mc.DiscreteUniform( 'p_R', lower = 0, upper = n_R - 1)

R = mc.Categorical('R', p = p_R, observed = True, value = r_val )



p_N1 = [ mc.Dirichlet( name ='p_N1_R%i' % j, theta = [1.0,] * (n_N[1] + 1) , trace = True, plot = True) for j in range(n_R) ]

p_N2 = [ mc.Dirichlet( name ='p_N2_R%i' % j, theta = [1.0,] * (n_N[2] + 1) , trace = True, plot = True) for j in range(n_R) ]


N_data[1] = np.concatenate( ( N_data[1], [ x-1 for x in N_data[1] ] ) )

print np.concatenate( ( N_data[1], [ x-1 for x in N_data[1] ] ) )
print N_data[2]
print n_N

@mc.deterministic(name = 'etaN1')
def etaN1(p_N1 = p_N1, R = R):
    print R
    return np.append(p_N1[R], ( 1.0 - np.sum(p_N1[R]) ))
    #return p_N1[R]

N1 = mc.Categorical('N1', p = etaN1, value = N_data[1], observed = True) 

@mc.deterministic(name = 'etaN2')
def etaN2(p_N2 = p_N2, R = R):
    return np.append(p_N2[R], ( 1.0 - np.sum(p_N2[R]) ))
    #return p_N2[R]

N2 = mc.Categorical('N2', p = etaN2, value = N_data[2], observed = True) 

#@mc.stochastic(dtype=int)
#def switchpoint(value = 0, R = R, p_N1 = p_N1):
#    
#    def logp(value, t_l, t_h):
#        if value > t_h or value < t_l:
#            return -np.inf
#        else:
#            return -np.log(t_h - t_l + 1)
#
#    def random(t_l, t_h):
#        from numpy.random import random
#        return np.round( (t_l - t_h) * random() ) + t_l

#exit(0)

#A = [ p_R, R, [p_N1, etaN1, N1] , [p_N2, etaN2, N2] ]
A = [ p_R, R, [p_N1, p_N2], [etaN1, etaN2], [N1, N2] ]
sampler = mc.MCMC(A)
#sampler.sample(iter = 100000, burn=20000, thin=10)
sampler.sample(iter = 10000, burn=2000, thin=10)
mod = mc.Model(A)
dag(mod, format="png", name="arch_toy", path="graph/")
    
directory = 'graph/prova_toy'
if( not os.path.isdir(directory) ):
    os.mkdir(directory)

plot(sampler, path=directory)