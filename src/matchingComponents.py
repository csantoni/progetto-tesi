'''
Created on 30/mag/2013

@author: Christian
'''

from OpenGL.GLUT import *
from OpenGL.GL import *
from OpenGL.GLU import *

from OpenGL.arrays import vbo

from OpenGL.GL.ARB.vertex_buffer_object import *

from utility.check_extension import *

from utility.mvertex    import mVertex
from utility.mnormal    import mNormal
from utility.mtexcoord  import mTexCoord
from utility.mface      import mFace
from utility.mmesh      import mMesh

from utility.drawfunctions import *

from utility.gui.button import Button
from utility.gui.slider import Slider

from mouseInteractor import MouseInteractor

import numpy
import math

from time import time


#Find biggest triangle
def findBiggestTriangle(points):
    maxA = -1
    maxTrA = [None, None, None]
    for p1 in points:
        for p2 in points:
            for p3 in points:
                p2p1 = numpy.array([ p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2] ])
                l_p2p1 = numpy.linalg.norm(p2p1)
                p3p1 = numpy.array([ p3[0] - p1[0], p3[1] - p1[1], p3[2] - p1[2] ])
                l_p3p1 = numpy.linalg.norm(p3p1)
                p3p2 = numpy.array([ p3[0] - p2[0], p3[1] - p2[1], p3[2] - p2[2] ])
                l_p3p2 = numpy.linalg.norm(p3p2)
                s = (l_p2p1 + l_p3p1 + l_p3p2) / 2.0
                area = math.sqrt( s * (s - l_p2p1) * (s - l_p3p1) * (s - l_p3p2) )
                if area > maxA:
                    maxA = area
                    maxTrA = [ p1, p2, p3 ]
    
    return maxTrA

def arrayToPolydata(verts, faces):
    pts = vtk.vtkPoints()
    tris = vtk.vtkCellArray()

    for v in verts:
        pts.InsertNextPoint( [v[0], v[1], v[2]] )
    for f in faces:
        poly = vtk.vtkPolygon()
        poly.GetPointIds().InsertId(0,f[0])
        poly.GetPointIds().InsertId(1,f[1])
        poly.GetPointIds().InsertId(2,f[2])
        tris.InsertNextCell(poly.GetPointIds())
        
    return pts, tris

def arrayToPolyPoints(verts):
    pts = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()

    for v in verts:
        id = pts.InsertNextPoint(v[0], v[1], v[2])
        vertices.InsertNextCell(1)
        vertices.InsertCellPoint(id)
        
    return pts, vertices

def getTriangle(pts):
    points = vtk.vtkPoints()
    for p in pts:
        points.InsertNextPoint(p)
     
    triangle = vtk.vtkTriangle()
    triangle.GetPointIds().SetId ( 0, 0 )
    triangle.GetPointIds().SetId ( 1, 1 )
    triangle.GetPointIds().SetId ( 2, 2 )
     
    triangles = vtk.vtkCellArray()
    triangles.InsertNextCell(triangle)
     
    trianglePolyData = vtk.vtkPolyData()
    trianglePolyData.SetPoints( points )
    trianglePolyData.SetPolys( triangles )
    
    return trianglePolyData

'''
    ========== GLOBAL VARIABLES & CONSTANTS ==========
'''

color_map = [
             [1.0, 0.0, 0.0],
             [0.0, 1.0, 0.0],
             [0.0, 0.0, 1.0],
             [1.0, 1.0, 0.0],
             [1.0, 0.0, 1.0],
             [0.0, 1.0, 1.0],
             [1.0, 1.0, 1.0],
             [0.2, 0.2, 0.2],
             [1.0, 1.0, 0.4],
             [0.4, 1.0, 1.0]
             ]

ESCAPE = '\033'             # Octal value for 'esc' key
SCREEN_SIZE = (800, 600)
SHAPE = ''
lastx=0
lasty=0

meshes_loaded = []

gui_objects = []
thread = None
buttonThread = None

'''
    ========== UTILITY FUNCTIONS ==========
'''

def drawAxis():
    glColor(1.0, 0.0, 0.0)
    glBegin(GL_LINE_STRIP)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.5, 0.0, 0.0)
    glEnd()
    glBegin(GL_LINE_STRIP)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 0.5, 0.0)
    glEnd()
    glBegin(GL_LINE_STRIP)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 0.0, 0.5)
    glEnd()
    glColor(0.0, 0.0, 0.0)
    glRasterPos3f( 0.5, 0.0, 0.0 )
    glutBitmapCharacter( GLUT_BITMAP_TIMES_ROMAN_10, ord('x') )
    glRasterPos3f( 0.0, 0.5, 0.0 )
    glutBitmapCharacter( GLUT_BITMAP_TIMES_ROMAN_10, ord('y') )
    glRasterPos3f( 0.0, 0.0, 0.5 )
    glutBitmapCharacter( GLUT_BITMAP_TIMES_ROMAN_10, ord('z') )

def drawBBox(bbox):
    bbox_idx = [
                [0, 3, 2, 1],
                [4, 5, 6, 7],
                [0, 4, 7, 3],
                [1, 2, 6, 5],
                [0, 1, 5, 4],
                [2, 3, 7, 6]
               ]
    
    for face in bbox_idx:
        glBegin(GL_LINE_STRIP)
        for idx in face:
            glVertex3f(*bbox[idx])
        glVertex3f(*bbox[face[0]])   
        glEnd()

'''
    =========== MAIN FUNCTIONS ============
'''

def loadComponent(m, path, segment_number):
    m.loadAsComponent(path, segment_number)
    m.VBOVertices = vbo.VBO(m.seqVertices)
    m.VBONormals = vbo.VBO(m.normals)
    m.VBOColors = vbo.VBO(m.colors)

def loadModel(m, path, segpath):
    m.loadModel(path, segpath)
    m.VBOVertices = vbo.VBO(m.seqVertices)
    m.VBONormals = vbo.VBO(m.normals)
    m.VBOColors = vbo.VBO(m.colors)

def drawModel(m, quadric):
    m.VBOVertices.bind()
    glEnableClientState(GL_VERTEX_ARRAY)
    glVertexPointer(3, GL_FLOAT, 0, m.VBOVertices)

    m.VBONormals.bind()
    glEnableClientState(GL_NORMAL_ARRAY)
    glNormalPointer(GL_FLOAT, 0, m.VBONormals)
        
    if (m.VBOColors is not None):
        m.VBOColors.bind()
        glEnableClientState(GL_COLOR_ARRAY)
        glColorPointer(3, GL_FLOAT, 0, m.VBOColors)
    else:
        glDisableClientState(GL_COLOR_ARRAY)
        pass
    
    glDrawArrays(GL_TRIANGLES, 0, len(m.seqVertices))
    
    if True:
        points = m.contactSlots
        for data in points:
            color = color_map[ data['seg'] - 1 ]
            for p in data['points']:
                glPushMatrix()
                glTranslatef(*p)
                glColor(color)
                gluSphere(quadric,0.1,16,16)
                glPopMatrix()

def drawBBoxes(m):
    for s in m.components.keys():
        for c in m.components[s]:
            drawBBox(c.bbox)

def init(width, height):

    global g_fVBOSupported, meshes_loaded, gui_objects, mouseInteractor, quadric
    #Check for VBOs Support
    g_fVBOSupported = IsExtensionSupported("GL_ARB_vertexbuffer_object")
    quadric = gluNewQuadric()
    #Defining all interface objects
    buttonColor = (0.7, 0.7, 0.7)
    buttonOutlineColor = (0.8, 0.8, 0.8)
    
#    b1 = Button( 20, 20, 160, 30, buttonColor, buttonOutlineColor, 'Load Elephant')
#    b1.setCallback(loadElephant)
#    b2 = Button( 20, 60, 160, 30, buttonColor, buttonOutlineColor, 'Try.')
#    b2.setCallback(try1)
#    sl1 = Slider( 150, SCREEN_SIZE[0] - 50, 50 )
    
#    gui_objects.append(b1)
#    gui_objects.append(b2)
#    gui_objects.append(sl1)
    
    glClearColor(0.1, 0.1, 0.2, 0.0)
    
    #Define openGL rendering behaviours
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)
    glEnable(GL_COLOR_MATERIAL)

    #Define lightning
    lP = 7
    lA = 0.1
    lD = 0.2
    lS = 0.3
    glEnable( GL_LIGHTING )
    glEnable( GL_LIGHT0 )
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, [0.8, 0.8, 0.8, 1] )
    glLightfv( GL_LIGHT0, GL_POSITION, [lP, lP, lP, 1] )
    glLightfv( GL_LIGHT0, GL_AMBIENT, [lA, lA, lA, 1] )
    glLightfv( GL_LIGHT0, GL_DIFFUSE, [lD, lD, lD, 1] )
    glLightfv( GL_LIGHT0, GL_SPECULAR, [lS, lS, lS, 1] )
    
    glEnable( GL_LIGHT1 )
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, [0.4, 0.4, 0.4, 1] )
    glLightfv( GL_LIGHT1, GL_POSITION, [-lP, lP, lP, 1] )
    glLightfv( GL_LIGHT1, GL_AMBIENT, [lA, lA, lA, 1] )
    glLightfv( GL_LIGHT1, GL_DIFFUSE, [lD, lD, lD, 1] )
    glLightfv( GL_LIGHT1, GL_SPECULAR, [lS, lS, lS, 1] )
    
    glMaterialfv( GL_FRONT_AND_BACK, GL_AMBIENT, [0.8, 0.8, 0.8, 1] )
    glMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, [0.8, 0.8, 0.8, 1] )
    glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, [1.0, 1.0, 1.0, 1] )
    glMaterialf( GL_FRONT_AND_BACK, GL_SHININESS, 50 )
    
    mouseInteractor = MouseInteractor( .01, 1 , gui_objects)

    #LOAD MODEL
    start = time()
    
    candidates =   [
                    [ [ 164, 1, 0 ],[ 137, 1, 0 ],[ 187, 1, 0 ] ],
                    [ [ 136, 1, 1 ],[ 135, 1, 1 ],[ 166, 1, 1 ] ],
                    [ [ 182, 2, 1 ],[ 191, 2, 2 ],[ 191, 2, 5 ] ],
                    [ [ 172, 2, 3 ],[ 197, 2, 0 ],[ 133, 2, 1 ] ],
                    [ [ 169, 3, 0 ],[ 196, 3, 0 ],[ 168, 3, 0 ] ],
                    [ [ 178, 4, 0 ],[ 195, 4, 3 ],[ 143, 4, 0 ] ],
                    [ [ 177, 4, 0 ],[ 168, 4, 1 ],[ 169, 4, 1 ] ]
                    ]
    candidates =   [
        [   [ 154, 1, 0 ],[ 165, 1, 2 ],[ 182, 1, 0 ] ],
            [ [ 154, 1, 0 ],[ 187, 1, 2 ],[ 198, 1, 0 ] ],
            [ [ 166, 1, 1 ],[ 166, 1, 3 ],[ 165, 1, 2 ] ],
            [ [ 176, 2, 0 ],[ 178, 2, 0 ],[ 176, 2, 1 ],[ 193, 2, 0 ],[ 193, 2, 1 ],[ 178, 2, 1 ] ],
            [ [ 176, 2, 0 ],[ 178, 2, 0 ],[ 176, 2, 1 ],[ 193, 2, 0 ],[ 193, 2, 1 ],[ 178, 2, 1 ] ],
            [ [ 198, 3, 0 ],[ 161, 3, 0 ],[ 163, 3, 0 ] ],
            [ [ 127, 4, 0 ],[ 183, 4, 0 ],[ 157, 4, 0 ] ]
        ]
    
    indices = [1,0,0,5,2,0,0]
    
    components = []
    for x in range(len(indices)):
        components.append(candidates[x][indices[x]])
    
    components_path = []
    
    base_path = '../res/tele-aliens/data/'
    for c in components:
        model_path = base_path + str(c[0]) + '/'
        path = model_path + "_".join(["seg", str(c[1])]) + "/" + "_".join(["comp", str(c[2])]) + "/"
        components_path.append(path)
        
    
    
    meshes = [None, ] * len(components)
    for x in range(len(components)):
        meshes[x] = mMesh(g_fVBOSupported)
    
    for x in range(len(components)):
        loadComponent(meshes[x], components_path[x], components[x][1] - 1)

    for x in range(len(components)):
        print "Component #" + str(x) +" - Segment " + str(components[x][1])
        print meshes[x].contactSlots
    print
    
    for x in range(len(components)):
        meshes_loaded.append(meshes[x])



    comp1 = 4
    comp2 = 5
    
    resA = None
    for c_points in meshes[comp1].contactSlots:
        if c_points['seg'] == candidates[comp2][0][1]:
            print "found"
            resA = numpy.array([ [float("%.4f" % a) for a in c] for c in c_points['points']])
            print resA
            
    resB = None
    for c_points in meshes[comp2].contactSlots:
        if c_points['seg'] == candidates[comp1][0][1]:
            print "found"
            #resB = numpy.array(c_points['points'])
            resB = numpy.array([ [float("%.4f" % a) for a in c] for c in c_points['points']])
            print resB
    
    resA = findBiggestTriangle(resA)
    resB = findBiggestTriangle(resB)
    
    print "Models loaded in %f" %(time() - start)
    print
    
def drawScene():
    
    global gui_objects, meshes_loaded, quadric
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_BLEND)
    glEnable(GL_LINE_SMOOTH)
    glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE)

    glMatrixMode( GL_PROJECTION )
    glLoadIdentity()
    xSize, ySize = glutGet( GLUT_WINDOW_WIDTH ), glutGet( GLUT_WINDOW_HEIGHT )
    gluPerspective(60, float(xSize) / float(ySize), 0.1, 1000)
    glMatrixMode( GL_MODELVIEW )
    glLoadIdentity()

    glTranslatef( 0, 0, -80 )
    mouseInteractor.applyTransformation()

    #Draw axis (for reference)
    drawAxis()
    
    #Draw all the stuff here
    i = 0
    #x_offsets = [ -30, -15, 0, 15, 30 ]
    x_offsets = []
    n_comp = len(meshes_loaded)
    dist = 15
    if n_comp % 2 == 0:
        start = - dist * ((n_comp)/2 + 0.5)
    else:
        start = - dist * ((n_comp -1)/2)
        
    x_offsets.append(start)
    for i in range(n_comp-1):
        x_offsets.append(start + (i+1) * dist)
            
    #x_offsets = [ 0, 0, 0, 0, 0 ]
    for m in meshes_loaded:
        glPushMatrix()
        glTranslatef( x_offsets[i % len(x_offsets)], 0, 0 )
        drawModel(m, quadric)
        if (False):
            drawBBoxes(m)
        glPopMatrix()
        i = i + 1
            
    #Draw all the interface here
    glDisable( GL_LIGHTING )
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0, float(xSize), float(ySize), 0, -1, 1)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    for obj in gui_objects:
        obj.draw()
    glEnable( GL_LIGHTING )
        
    glutSwapBuffers()

def resizeWindow(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0 , float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)

def mainLoop():
    glutInit(sys.argv)
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH )
    glutInitWindowSize(*SCREEN_SIZE)
    glutInitWindowPosition(1000, 200)

    window = glutCreateWindow("MyMesh 0.1")
    
    init(*SCREEN_SIZE)
    mouseInteractor.registerCallbacks()

    glutDisplayFunc(drawScene)
    glutIdleFunc(drawScene)
    glutReshapeFunc(resizeWindow)

    glutMainLoop()
    
if __name__ == "__main__":
    mainLoop()
