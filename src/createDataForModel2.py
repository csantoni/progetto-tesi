'''
Created on 07/mag/2013

@author: Christian
'''
import numpy
import sys
import pickle

from os import walk
from os import mkdir
from os.path import isdir

from time import time

def createData(base_path, model_list, seg_num):
    path_list = []
    
    #Massimo numero di componenti per segmento
    n_N = { }
    
    # N[i] = lista di numero di componenti per segmento i per ogni modello
    N_data = { }
    
    # D[i] = lista di adiacenza tra componenti dei segmenti i per ogni modello
    D_data = { }
    D_data_arr = { }
    D_data_final = { }
    # lista delle pcs features per componente di segmento i per modello
    C_data = { }
    C_subdivisions = {}
    
    max_C = None
    min_C = None
    
    #seg_names = [ 'seg_1', 'seg_2', 'seg_3', 'seg_4' ]
    for model_name in model_list:
        model_path = base_path + str(model_name) + '/'
        
        comp_list = []
        comp_file = open(model_path + 'list.txt', 'r')
        for line in comp_file:
            comp_list.append(line[:-1])
        comp_file.close()
        
        N_item = {}
        for s in range(seg_num):
            N_item[s+1] = 0
        
        for comp in comp_list:
            path = comp.split('_')
            seg_number = int(path[3])
            comp_number = int(path[5][:-4])
            path = model_path + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"
            
            N_item[seg_number] += 1
                
            scale_arr = numpy.load(path + 'scale.npy')
            geom_arr = numpy.load(path + '70pca_geom.npy')
            lf_arr = numpy.load(path + '70pca_lf.npy')
            
            scale_arr = numpy.array([ numpy.linalg.norm(scale_arr[0]), numpy.linalg.norm(scale_arr[1]), numpy.linalg.norm(scale_arr[2]) ])
            
            c = numpy.concatenate((scale_arr, geom_arr, lf_arr))
            
            if max_C == None:
                max_C = [sys.float_info.min, ] * len(c)
                min_C = [sys.float_info.max, ] * len(c)
            
            max_C = numpy.maximum(max_C, c)
            min_C = numpy.minimum(max_C, c)
            
            if seg_number in C_data:
                C_data[seg_number].append(c)
            else:
                C_data[seg_number] = []
                C_data[seg_number].append(c)
            
        for i in N_item.keys():
            if i in C_subdivisions:
                C_subdivisions[i].append( C_subdivisions[i][-1] + N_item[i] )
            else:
                C_subdivisions[i] = [0, ]
                C_subdivisions[i].append( C_subdivisions[i][-1] + N_item[i] )
                    
        
        for i in N_item.keys():
            if i in N_data:
                N_data[i].append(N_item[i])
            else:
                N_data[i] = []
                N_data[i].append(N_item[i])
        
        adj_output = open(model_path + 'adj.pkl', 'r')
        adj2 = pickle.load(adj_output)
        adj_output.close()
        adjacency_matrix = adj2
        
        complete_adj = {  }
        for s in range(seg_num):
            complete_adj[s+1] = {}
            for s_2 in range(seg_num):
                complete_adj[s+1][s_2+1] = 0
            
        for k in adj2.keys():
            for k_2 in adj2.keys():
                #print str(k) + ' ' + str(k_2)
                complete_adj[k][k_2] = adjacency_matrix[k][k_2]
          
        #print "Adjacency info:"
        for k in adj2.keys():
            s = "\t %d | " % k
            for k_2 in adj2.keys():
                s += " %d |" % (adj2[k][k_2])
    #        print s
    #    print
        for k in complete_adj.keys():
            s = "\t %d | " % (k)
            d_item = ''
            for k_2 in complete_adj.keys():
                s += " %d |" % (complete_adj[k][k_2])
                d_item += str(complete_adj[k][k_2])
    #        print s
            
            if k in D_data:
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            else:
                D_data[k] = {}
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            
            if k in D_data_arr:
                D_data_arr[k].append( complete_adj[k].values() )
            else:
                D_data_arr[k] = []
                D_data_arr[k].append( complete_adj[k].values() )
            
            if k in D_data_final:
                D_data_final[k].append(d_item)
            else:
                D_data_final[k] = []
                D_data_final[k].append(d_item)
            
    map = {}
    for seg in D_data.keys():
        for val in D_data[seg].keys():
            if val in map:
                map[val] += 1
            else:
                map[val] = 1
    
    print map
    print sorted(map.keys())
    print len(sorted(map.keys()))
    
    map_final = {}
    v = 0
    for val in sorted(map.keys()):
        map_final[val] = v
        v+=1
    print map_final
    for seg in D_data_final.keys():
        for i in xrange( len(D_data_final[seg]) ):
            D_data_final[seg][i] = map_final[D_data_final[seg][i]]
    
    N_max = []
    for seg in N_data.keys():
        N_data[seg] = numpy.array(N_data[seg])
        N_max.append( [seg, numpy.max(N_data[seg])] )
    N_max = numpy.array(N_max)
    for seg in C_data.keys():
        C_data[seg] = numpy.array(C_data[seg])
    for seg in D_data_final.keys():
        D_data_final[seg] = numpy.array(D_data_final[seg])
    
    print "N_data"
    print N_data
    print "N_max"
    print N_max
    print "C_data"
    print C_data
    print "D_data_final"
    print D_data_final
    print "D_data_arr"
    print D_data_arr
    print "Sample c length: " + str(len(C_data[1][0]))
    
    for seg in N_data.keys():
        numpy.save(base_path + "n2N" + str(seg), N_data[seg])
    numpy.save(base_path + "n2N_max", N_max)
    numpy.save(base_path + "n2C_max", max_C)
    numpy.save(base_path + "n2C_min", min_C)
    for seg in C_data.keys():
        numpy.save(base_path + "70n2C" + str(seg), C_data[seg])
    for seg in D_data_final.keys():
        numpy.save(base_path + "n2D" + str(seg), D_data_final[seg])
        numpy.save(base_path + "n2Darr" + str(seg), D_data_arr[seg])
    print
    for seg in C_subdivisions.keys():
        #print N_data[seg]
        print len(C_subdivisions[seg])
        print C_subdivisions[seg]
        print
    for seg in C_subdivisions.keys():
        numpy.save(base_path + "subC" + str(seg), numpy.array(C_subdivisions[seg]))

def writeODCFiles(basePath):
    
    files = []
    
    for (_, _, filenames) in walk(basePath):
        files.extend(filenames)
        break
    print files
    
    basePathBUGS = basePath + "openBUGS/"
    if( not isdir(basePathBUGS) ):
        mkdir(basePathBUGS)
    
    features = open( basePathBUGS + "features_NCD.txt", "wb")
    
    features.write("list(\n")
    
    seg_num = 0
    n_samples = 0
    feat_len = 0
    n_max = []
    d_max = -1
    
    for f in files:
        filename = f.split('.')[0]
        if filename[:2] == '70':
            #C features
            seg_num += 1
            features.write("C" + filename[-1] + " = structure( .Data = c(\n")
            temp = numpy.load(basePath + f)
            for el in temp[:-1]:
                feat_len = len(el)
                for x in el:
                    features.write("%f, " % x)
                features.write("\n")
            for x in temp[-1][:-1]:
                features.write("%f, " % x)
            features.write("%f),\n " % temp[-1][-1])
            features.write(".Dim = c(" + str(len(temp)) + ", " + str(len(temp[0])) + ")), \n")
            
        elif filename[:3] == 'n2D' and filename[:4] != 'n2Da':
            #D feature
            features.write("D" + filename[-1] + " = c(")
            temp = numpy.load(basePath + f)
            for x in temp[:-1]:
                features.write(str(x+1) + ", ")
                if x+1 > d_max: d_max = x+1
            features.write(str(temp[-1]+1) + "), \n")
            if temp[-1]+1 > d_max: d_max = temp[-1]+1
        
        elif filename[:3] == 'n2N' and filename[:4] != 'n2N_':
            #N feature
            features.write("N" + filename[-1] + " = c(")
            temp = numpy.load(basePath + f)
            n_samples = len(temp)
            for x in temp[:-1]:
                features.write(str(x+1) + ", ")
            features.write(str(temp[-1]+1) + "), \n")
        
        elif filename[:4] == 'n2N_':
            #N_max feature
            temp = numpy.load(basePath + f)
            for x in temp:
                n_max.append(x[1]+1)
            
        elif filename[:3] == 'sub':
            #SUBS feature
            features.write("sub_C" + filename[-1] + " = c(")
            temp = numpy.load(basePath + f)
            for x in temp[:-1]:
                features.write(str(x) + ", ")
            features.write(str(temp[-1]) + "), \n")
    
    features.write("end = 1)")
    features.close()
    
    model = open( basePathBUGS + "model.txt", "wb")
    
    model.write("model\n")
    model.write("{\n")
    model.write("  for( i in 1 : N_samples ){\n")
    model.write("    R[i] ~ dcat( p_R[ ] )\n")
    for k in range(seg_num):
        model.write("    N%d[i] ~ dcat( p_N%d[ R[i], ] )\n" % ( k+1, k+1 ))
    model.write("\n")
    for k in range(seg_num):
        model.write("    S%d[i] ~ dcat( p_S%d[ R[i], ] )\n" % ( k+1, k+1 ))
    model.write("\n")    
    for k in range(seg_num):
        model.write("    D%d[i] ~ dcat( p_D%d[ S%d[i], ] )\n" % ( k+1, k+1, k+1 ))
    model.write("\n")    
    for k in range(seg_num):
        model.write("    for ( j in (sub_C%d[i] + 1) : sub_C%d[i+1] ){\n" % ( k+1, k+1 ))
        model.write("        C%d[ j , 1:C_feat_len] ~ dmnorm( mu_C%d[ S%d[i], ], tau_C%d[ S%d[i], , ] )\n" % ( k+1, k+1 , k+1 , k+1 , k+1 ))
        model.write("    }\n")
    model.write("  }\n")
    model.write("\n")
    model.write("  # ========== VARIABLE R ==========\n")
    
    model.write("  for ( i in 1 : R_styles ){\n")
    model.write("          alpha_R[i] <- 1\n")
    model.write("  }\n")
    model.write("  p_R[1 : R_styles] ~ ddirich(alpha_R[ ])\n")    
    model.write("\n")
    
    model.write("  # ========== VARIABLES N AND S ==========\n")
    
    model.write("  for ( i in 1 : segment_number ){\n")
    model.write("      for ( j in 1 : R_styles ){\n")
    model.write("          for ( k in 1 : N_max[i] ){\n")
    model.write("              alpha_N[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("          for ( k in 1 : S_styles[i] ){\n")
    model.write("              alpha_S[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("      }\n")
    model.write("  }\n")
    
    model.write("  for ( k in 1:R_styles){\n")
    for k in range(seg_num):
        model.write("    p_N%d[k, 1 : N_max[%d] ] ~ ddirich( alpha_N[ %d, k,1 : N_max[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
    model.write("\n")
    for k in range(seg_num):
        model.write("    p_S%d[k, 1 : S_styles[%d] ] ~ ddirich( alpha_S[%d, k,1 : S_styles[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
    model.write("  }\n")
    
    model.write("\n")
    model.write("\n")
    
    model.write("  # ========== VARIABLES D AND C ==========\n")
    
    model.write("  for ( i in 1 : segment_number ){\n")
    model.write("      for ( j in 1 : S_styles[i] ){\n")
    model.write("          for ( k in 1 : D_max[i] ){\n")
    model.write("              alpha_D[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("      }\n")
    model.write("  }\n")
    model.write("\n")
    for k in range(seg_num):
        model.write("  for ( k in 1 : S_styles[%d]){\n" % (k+1))
        model.write("      p_D%d[k, 1 : D_max[%d] ] ~ ddirich( alpha_D[ %d, k,1 : D_max[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
        model.write("\n")
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          mu_norm_C%d[k, i] <- 0\n" % ( k+1 ))
        model.write("      }\n")
        model.write("\n")    
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          for ( j in 1 : C_feat_len ){\n")
        model.write("              prec_norm_C%d[k, i , j] <- equals(i,j) * 0.001\n" % ( k+1 ))
        model.write("          }\n")
        model.write("      }\n")
        model.write("      mu_C%d[k, 1: C_feat_len ] ~ dmnorm( mu_norm_C%d[ k, ], prec_norm_C%d[ k, , ] )\n" % ( k+1, k+1, k+1 ))
        model.write("\n")    
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          for ( j in 1 : C_feat_len ){\n")
        model.write("              phi_wish_C%d[k, i , j] <- equals(i,j) * 0.0001\n" % ( k+1 ))
        model.write("          }\n")
        model.write("      }\n")
        model.write("      tau_C%d[k, 1: C_feat_len, 1: C_feat_len ] ~ dwish(phi_wish_C%d[k, , ],  wish_nu )\n" % ( k+1, k+1 ))
        model.write("  }\n")
        model.write("\n")
    model.write("  wish_nu <- C_feat_len + 1\n")
    model.write("}")
    
    model.close()
    
    starter = open( basePathBUGS + "starter.txt", "wb")
    print seg_num
    print max(n_max)
    print n_samples
    print feat_len
    starter.write( "%d %d %d %d %d" % ( seg_num, max(n_max), d_max, n_samples, feat_len ) )
    starter.close()
    
if __name__ == "__main__":
    
    meshesDirectory     = '../res/chairs/data/'
    
    basePath = '../res/chairs/data/'
    basePathSDF = '../res/chairs/data/SDF_files/'
    basePathOBJ = '../res/chairs/data/OBJ_files/'
    basePathLD = '../res/chairs/data/ld/'
    
    meshesList = []
    meshesPath = []
    
    for (_, dirnames, filenames) in walk(meshesDirectory):
        meshesList.extend(dirnames)
        break
    #print meshesList
    
    meshesList = [ x for x in meshesList if (x != 'ld' and x != 'OBJ_files' and x != 'SDF_files')]
    
    begin = time()
    createData(basePath, meshesList, 3)
    end = time()
    
    writeODCFiles(basePath)
    
    print "Data for model created in: " + str(end - begin)
    
    