'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot
import os
  
def getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ):
    n_R = input_R
    m = 35
    f_len = 33
    
    n_segments = 4
    n_N = {1:0, 2:0 ,3:0 ,4:0}
    n_S = input_S
    N_data = {}
    D_data = {}
    C_data = {}
    
    for seg in n_N.keys():
        N_data[seg] = None
        D_data[seg] = None
        C_data[seg] = None
    
    base_path = '../res/tele-aliens/data/'
    N_max = np.load(base_path + 'n2N_max.npy')
    for v in N_max:
        n_N[v[0]] = v[1] + 1
        
    for seg in n_N.keys():
        N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
        D_data[seg] = np.load(base_path + 'n2D' + str(seg) + '.npy')
        C_data[seg] = np.load(base_path + 'n2C' + str(seg) + '.npy')
    
    
    C_max = np.max( np.load(base_path + 'n2C_max.npy') )
    C_min = np.min( np.load(base_path + 'n2C_min.npy') )
    
    alpha_N = {}
    theta_N = {}
    cpt_N_R = {}
    cpt_N = {}
    N = {}
    predN = {}
    
    alpha_S = {}
    theta_S = {}
    cpt_S = {}
    cpt_S_R = {}
    S = {}
    
    alpha_D = {}
    theta_D = {}
    cpt_D = {}
    cpt_D_S = {}
    D = {}
    predD = {}
    
    taus_C = {}
    n_mus_C = {}
    n_tau_C = {}
    t_t = {}
    m_n = {}
    t_n = {}
    tau = {}
    mu = {}
    C = {}
    predC = {}
    
    # ======= R VARIABLE ========
    #cpt_R = mc.Dirichlet(name='cpt_R', theta = [1.0, ] * n_R)
    #alpha_R = mc.Beta( name = 'alpha_R', alpha = 2.0, beta = 2.0, size = n_R )
    #alpha_R = mc.Uninformative( name = 'alpha_R' , value = [0.5, ] * n_R)
    cpt_R = mc.Dirichlet(name='cpt_R', theta = [1.0, ] * n_R, value = [ 1.0 / (n_R - 1), ] * (n_R - 1))
    R = mc.Categorical('R', p = cpt_R)
    
    print n_N
    print
    # ======= N VARIABLE ========
    for i in n_N.keys():
        #alpha_N[i] = [ [1.0, ] * n_N[i] for _ in range(n_R) ]
        #alpha_N[i] = [ mc.Uninformative( name = 'alpha_N%i_R%i' % (i, j) , value = [1.0, ] * n_N[i]) for j in range(n_R) ]
        #alpha_N[i] = [ mc.Normal( name = 'alpha_N%i_R%i' % (i, j) , mu = 1.0, tau = 0.1, value = [1.0, ] * n_N[i]) for j in range(n_R) ]
        #alpha_N[i] = [ mc.Poisson( name = 'alpha_N%i_R%i' % (i, j) , mu = 1.0, value = [1.0, ] * n_N[i]) for j in range(n_R) ]
        #alpha_N[i] = [ mc.Beta( name = 'alpha_N%i_R%i' % (i, j) , alpha = 2.0, beta = 2.0, size = n_N[i]) for j in range(n_R) ]
        
#        theta_N[i] = mc.Lambda('theta_N%i'%i, lambda alpha_N=alpha_N[i], R=R: alpha_N[R])
#        cpt_N[i] = mc.Dirichlet(name='cpt_N%i'%i, theta = theta_N[i])
        
        cpt_N_R[i] = [ mc.Dirichlet(name = 'cpt_N%i_R%i' % (i, j), theta = [1.0, ] * (n_N[i] + 1), value = [1.0 / n_N[i] , ] * n_N[i] ) for j in range(n_R)]
        #cpt_N[i] = mc.Lambda('cpt_N%i'%i, lambda cpts=cpt_N_R[i], R=R:  mc.CompletedDirichlet(name = 'ccpt_N%i' % i, doc = 'ccpt_N%i' % i, D = cpts[R]))
        
        @mc.deterministic(name = 'cpt_N%i'%i)
        def cpt_N_i(cpts = cpt_N_R[i], R = R):
            return cpts[R]
        
        N[i] = mc.Categorical('N%i'%i, cpt_N_i, observed=True, value=N_data[i])
        predN[i] = mc.Categorical('predN%i'%i, cpt_N_i, value = 0)
    
    # ======= S VARIABLE ========
    for i in n_N.keys():
        #alpha_S[i] = [ [1.0, ] * n_S[i] for j in range(n_R) ]
        #alpha_S[i] = [ mc.Exponential(name = 'alpha_S%i_R%i' % (i, j), beta = 1.0, size = n_S[i]) for j in range(n_R) ]
        #alpha_S[i] = [ mc.Uninformative( name = 'alpha_S%i_R%i' % (i, j) , value = [1.0, ] * n_S[i]) for j in range(n_R) ]
        #alpha_S[i] = [ mc.Normal( name = 'alpha_S%i_R%i' % (i, j) , mu = 1.0, tau = 0.1, value = [1.0, ] * n_S[i]) for j in range(n_R) ]
        #alpha_S[i] = [ mc.Poisson( name = 'alpha_S%i_R%i' % (i, j) , mu = 1.0, value = [1.0, ] * n_S[i]) for j in range(n_R) ]
        #alpha_S[i] = [ mc.Beta( name = 'alpha_S%i_R%i' % (i, j) , alpha = 2.0, beta = 2.0, size = n_S[i]) for j in range(n_R) ]
        
#        theta_S[i] = mc.Lambda('theta_S%i'%i, lambda alpha_S=alpha_S[i], R=R: alpha_S[R])
#        cpt_S[i] = mc.Dirichlet(name='cpt_S%i'%i, theta = theta_S[i])
        
        cpt_S_R[i] = [ mc.Dirichlet(name = 'cpt_S%i_R%i' % (i, j), theta = [1.0, ] * n_S[i], value = [1.0 / (n_S[i] - 1) , ] * (n_S[i] - 1) ) for j in range(n_R)]
        @mc.deterministic(name = 'cpt_S%i'%i)
        def cpt_S_i(cpts = cpt_S_R[i], R = R):
            return cpts[R]
        
        S[i] = mc.Categorical('S%i'%i, cpt_S_i)
    
    d_arr = [1.0 / (m + 2) , ] * (m - 1)
    d_arr.append( 1.0 - np.sum(d_arr) )
    
    # ======= D VARIABLE ========
    for i in n_N.keys():
        #alpha_D[i] = [ [1.0, ] * m for _ in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Exponential(name = 'alpha_D%i_S%i' % (i, j), beta = 1.0, size = m) for j in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Uninformative( name = 'alpha_D%i_S%i' % (i, j) , value = [1.0, ] * m ) for j in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Normal( name = 'alpha_D%i_S%i' % (i, j) , mu = 1.0, tau = 0.1, value = [1.0, ] * m ) for j in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Poisson( name = 'alpha_D%i_S%i' % (i, j) , mu = 1.0, value = [1.0, ] * m ) for j in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Beta( name = 'alpha_D%i_S%i' % (i, j) , alpha = 2.0, beta = 2.0, size = m ) for j in range(n_S[i]) ]
        
#        theta_D[i] = mc.Lambda('theta_D%i'%i, lambda alpha_D=alpha_D[i], S=S[i]: alpha_D[S])
#        cpt_D[i] = mc.Dirichlet(name='cpt_D%i'%i, theta = theta_D[i])
        
        cpt_D_S[i] = [ mc.Dirichlet(name = 'cpt_D%i_S%i' % (i, j), theta = [1.0, ] * (m + 1), value = d_arr ) for j in range(n_S[i])]
        @mc.deterministic(name = 'cpt_D%i'%i)
        def cpt_D_i(cpts = cpt_D_S[i], S = S[i]):
            return cpts[S]
        
        D[i] = mc.Categorical('D%i'%i, cpt_D_i, observed=True, value=D_data[i])
        predD[i] = mc.Categorical('predD%i'%i, cpt_D_i)
    
    # ======= C VARIABLE ========
    for i in n_N.keys():
        taus_C[i] = [np.eye(f_len) for _ in range(n_S[i])]
        n_mus_C[i] = [mc.Uniform('mC%i%i'%(i,j), lower=C_min, upper=C_max) for j in range(n_S[i])]
        n_tau_C[i] = [mc.Uniform('tC%i%i'%(i,j), lower=0.0001, upper=1.0) for j in range(n_S[i])]
        
        t_t[i] = mc.Lambda('t_t%i'%i, lambda taus_C = taus_C[i], S=S[i]: taus_C[S])
        m_n[i] = mc.Lambda('m_n%i'%i, lambda n_mus_C = n_mus_C[i], S=S[i]: n_mus_C[S])
        t_n[i] = mc.Lambda('t_n%i'%i, lambda n_tau_C = n_tau_C[i], S=S[i]: n_tau_C[S])
        
        tau[i] = mc.Wishart('tau%i'%i, n=f_len + 1, Tau=t_t[i])
        mu[i] = mc.Normal('mu%i'%i, mu=m_n[i], tau=t_n[i], size=f_len)
        
        C[i] = mc.MvNormalCov('C%i'%i, mu=mu[i], C=tau[i], observed=True, value=C_data[i]) 
        predC[i] = mc.MvNormalCov('predC%i'%i, mu=mu[i], C=tau[i])
    
    return locals()
    
if __name__ == "__main__":
#    A = mc.Model(getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ))
#    dag(A, format="png", name="arch_def_3", path="graph/")
#    
#    mod = mc.MAP(getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ))
#    mod.fit(iterlim = 10)
#    print "AIC: " + str(mod.AIC)
#    print "BIC: " + str(mod.BIC)
#    print
    
    #mcmcSampler = mc.MCMC( mod.variables , db='pickle', dbname = 'prova_beta.pickle')
    mcmcSampler = mc.MCMC( getModel( input_R = 3, input_S = {1:2, 2:2 ,3:2 ,4:2} ) , db='pickle', dbname = 'prova_beta.pickle')
    mcmcSampler.sample(iter=10000, burn=5000, thin=20)
    mcmcSampler.db.close()
    
    directory = 'graph/prova_beta'
    if( not os.path.isdir(directory) ):
        os.mkdir(directory)
    
    plot(mcmcSampler, path=directory)
    
    