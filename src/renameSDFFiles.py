'''
Created on 12/apr/2013

@author: Christian
'''

from os import walk
from os import rename
from os import remove
from os import mkdir
from os.path import isdir
from shutil import copy

import convertToOBJ

from time import time

basePath = '../res/chairs/data/'
basePathSDF = '../res/chairs/data/SDF_files/'
basePathOBJ = '../res/chairs/data/OBJ_files/'

def renameSDF(model_list):
    for model in model_list:
        f = []
        for (dirpath, dirname, filenames) in walk(basePath + str(model) + '/'):
            f.extend(filenames)
            break
        
        for file in f:
            if not file.endswith('obj'):
                if file.startswith('seg_'):
                    file_parts = file.split('.')
                    if file_parts[-2] == 'log':
                        rename( basePath + str(model) + '/' + file, basePath + str(model) + '/log_'+ str(model) + '_' + file )
                    else:
                        rename( basePath + str(model) + '/' + file, basePath + str(model) + '/sdf_'+ str(model) + '_' + file )

    print "Done!"
    
def moveSDF(model_list):
    for model in model_list:
        f = []
        for (dirpath, dirname, filenames) in walk(basePath + str(model) + '/'):
            f.extend(filenames)
            break

        for file in f:
            if file.startswith('log_') or file.startswith('sdf_'):
                copy(basePath + str(model) + '/' + file , basePath + 'SDF_files/' + file)

    print "Done!"

def renameBackSDF():
    f = []
    for (dirpath, dirname, filenames) in walk(basePathSDF + '/'):
        f.extend(filenames)
        break
    
    for file in f:
        if file.endswith('txt'):
            file_parts = file.split('_')
            copy(basePathSDF + '/' + file , basePath + file_parts[1]+ '/' + '_'.join(file_parts[2:]))
#            print basePathSDF + '/' + file
#            print basePath + file_parts[1]+ '/' + '_'.join(file_parts[2:])
#            print
            
    print "Done!"
    
def moveOBJ(model_list):
    mega_list = []
    for model in model_list:
        f = []
        for (dirpath, dirname, filenames) in walk(basePath + str(model) + '/'):
            f.extend(filenames)
            break

        for file in f:
            if file.endswith('obj'):
                copy(basePath + str(model) + '/' + file , basePathOBJ + file)
                mega_list.append(file[:-4] + '\n')

    mega_list.append('\n')
    names_file = open(basePathOBJ + 'list.txt', 'wb' )
    for x in mega_list:
        names_file.write(x)
    
    print "Done!"

def cleanUp(model_list):
    for model in model_list:
        f = []
        for (dirpath, dirname, filenames) in walk(basePath + str(model) + '/'):
            f.extend(filenames)
            break

        for file in f:
            if file.endswith('obj') or file.endswith('off'):
                remove(basePath + str(model) + '/' + file)
            if file.endswith('txt') and file != 'list.txt':
                remove(basePath + str(model) + '/' + file)

    print "Done!"
    
def cleanOBJ(model_list):
    for model in model_list:
        f = []
        print basePath + str(model) + '/'
        for (dirpath, dirname, filenames) in walk(basePath + str(model) + '/'):
            f.extend(filenames)
            break
        print f
        for file in f:
            if file.endswith('obj'):
                remove(basePath + str(model) + '/' + file)
    print "Done!"

if __name__ == "__main__":
    #global basePath, basePathSDF, basePathOBJ
    
    meshesDirectory     = '../res/chairs/data/'
    
    basePath = '../res/chairs/data/'
    basePathSDF = '../res/chairs/data/SDF_files/'
    basePathOBJ = '../res/chairs/data/OBJ_files/'
    
    meshesList = []
    meshesPath = []
    
    for (_, dirnames, filenames) in walk(meshesDirectory):
        meshesList.extend(dirnames)
        break
    print meshesList
    
    if( not isdir(basePathSDF) ):
        mkdir(basePathSDF)
    if( not isdir(basePathOBJ) ):
        mkdir(basePathOBJ)
    
#    cleanOBJ(meshesList)
#    exit(0)
    startRenSDF = time()
    #Rename delle mesh per SDF (aggiunta nome modello)
    renameSDF(meshesList)
    #Raccolta in una singola directory
    moveSDF(meshesList)
    endRenSDF = time()
    
    #Converting component meshes in OBJ format, for LightField extraction
    startOBJ = time()
    for model in meshesList:
        convertToOBJ.convertMeshes(meshesDirectory + str(model) + "/")
        
    moveOBJ(meshesList)
    endOBJ = time()
    
    print "Rename and Moving SDF in: " + str(endRenSDF - startRenSDF)
    print "Conversion to OBJ in: " + str(endOBJ - startOBJ)