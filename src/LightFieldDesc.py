'''
Created on 20/feb/2013

@author: Christian
'''

import numpy
from os import walk

def computeLightFieldFeatureVector(directory, directoryLD):
    
    comp_file = open(directoryLD + 'list.txt', 'r')
    comp_list = []
    for line in comp_file:
        comp_list.append(line[:-1])
    
    for filename in comp_list[:-1]:
        print "Parsing descriptor for file: " + filename
        
        art_q4_fn = directoryLD + filename + '_q4_v1.8.art'
        art_q8_fn = directoryLD + filename + '_q8_v1.8.art'
        cir_q8_fn = directoryLD + filename + '_q8_v1.8.cir'
        ecc_q8_fn = directoryLD + filename + '_q8_v1.8.ecc'
        fd_q8_fn =  directoryLD + filename + '_q8_v1.8.fd'
        
        art_q4_f = open(art_q4_fn, 'r')
        art_q4_dt = numpy.dtype((numpy.ubyte, (1800)))
        art_q4_n_arr = numpy.fromfile(art_q4_f, art_q4_dt)
        art_q4_n_arr = art_q4_n_arr[0]
        art_q4_f.close()
        
        art_q8_f = open(art_q8_fn, 'r')
        art_q8_dt = numpy.dtype((numpy.ubyte, (3500)))
        art_q8_n_arr = numpy.fromfile(art_q8_f, art_q8_dt)
        art_q8_n_arr = art_q8_n_arr[0]
        art_q8_f.close()
        
        cir_q8_f = open(cir_q8_fn, 'r')
        cir_q8_dt = numpy.dtype((numpy.ubyte, (100)))
        cir_q8_n_arr = numpy.fromfile(cir_q8_f, cir_q8_dt)
        cir_q8_n_arr = cir_q8_n_arr[0]
        cir_q8_f.close()
        
        ecc_q8_f = open(ecc_q8_fn, 'r')
        ecc_q8_dt = numpy.dtype((numpy.ubyte, (100)))
        ecc_q8_n_arr = numpy.fromfile(ecc_q8_f, ecc_q8_dt)
        ecc_q8_n_arr = ecc_q8_n_arr[0]
        ecc_q8_f.close()
        
        fd_q8_f = open(fd_q8_fn, 'r')
        fd_q8_dt = numpy.dtype((numpy.ubyte, (1000)))
        fd_q8_n_arr = numpy.fromfile(fd_q8_f, fd_q8_dt)
        fd_q8_n_arr = fd_q8_n_arr[0]
        fd_q8_f.close()
        
        feature_vector = numpy.concatenate((art_q4_n_arr, art_q8_n_arr, cir_q8_n_arr, ecc_q8_n_arr, fd_q8_n_arr))
        
        path = filename.split('_')
        path = directory + str(path[1]) + '/' + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"
        
        numpy.save(path + "lf", feature_vector)

if __name__ == "__main__":
    basePath = "../res/tele-aliens/data/"
    basePathLD = "../res/tele-aliens/data/ld/"
    
    computeLightFieldFeatureVector(basePath, basePathLD)
    print "done!"