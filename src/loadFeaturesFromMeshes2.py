'''
Created on 06/giu/2013

@author: Christian
'''

from os import walk
from os import rename
from os import remove
from os import mkdir
from os.path import isdir
from shutil import copy

import convertToOBJ
import LightFieldDesc
import saveSDHist
import GeomFeatureVector
import PCAFeatures

from time import time

basePath = '../res/chairs/data/'
basePathSDF = '../res/chairs/data/SDF_files/'
basePathOBJ = '../res/chairs/data/OBJ_files/'

def renameBackSDF(basePathSDF):
    f = []
    for (dirpath, dirname, filenames) in walk(basePathSDF + '/'):
        f.extend(filenames)
        break
    
    for file in f:
        if file.endswith('txt'):
            file_parts = file.split('_')
            copy(basePathSDF + '/' + file , basePath + file_parts[1]+ '/' + '_'.join(file_parts[2:]))
#            print basePathSDF + '/' + file
#            print basePath + file_parts[1]+ '/' + '_'.join(file_parts[2:])
#            print
            
    print "Done!"


if __name__ == "__main__":
    #global basePath, basePathSDF, basePathOBJ
    
    meshesDirectory     = '../res/chairs/data/'
    
    basePath = '../res/chairs/data/'
    basePathSDF = '../res/chairs/data/SDF_files/'
    basePathOBJ = '../res/chairs/data/OBJ_files/'
    basePathLD = '../res/chairs/data/ld/'
    
    meshesList = []
    meshesPath = []
    
    for (_, dirnames, filenames) in walk(meshesDirectory):
        meshesList.extend(dirnames)
        break
    print meshesList
    
    startLD = time()
    #LightFieldDesc.computeLightFieldFeatureVector(basePath, basePathLD)
    endLD = time()
    print "FINISHED computeLightFieldFeatureVector in: " + str(endLD - startLD)
    
    startSDF = time()
    #renameBackSDF(basePathSDF)
    print "FINISHED renameBackSDF"
#    for model in meshesList:
#        if model == 'ld' or model == 'OBJ_files' or model == 'SDF_files':
#            continue
#        else:
#            saveSDHist.parseHistograms(basePath + str(model) + "/")
    endSDF = time()
    print "FINISHED parseHistograms " + str(endSDF - startSDF)
    
    startGeom = time()
#    for model in meshesList:
#        if model == 'ld' or model == 'OBJ_files' or model == 'SDF_files':
#            continue
#        else:
#            GeomFeatureVector.saveGeometricFeatureVector(basePath + str(model) + "/")
    endGeom = time()
    print "FINISHED saveGeometricFeatureVector in: " + str(endGeom - startGeom)
    
    startPCA = time() 
    #PCAFeatures.computePCAVectors(basePath, meshesList)
    endPCA = time()
    print "FINISHED computePCAVectors " + str(endPCA - startPCA)
    
    
    
    print "computeLightFieldFeatureVector in: " + str(endLD - startLD)
    print "parseHistograms SDF in: " + str(endSDF - startSDF)
    print "saveGeometricFeatureVector in: " + str(endGeom - startGeom)
    print "computePCAVectors in: " + str(endPCA - startPCA)
    
    print "Total time: " + str(endPCA - startLD)
    
    
    