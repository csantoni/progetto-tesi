'''
Created on 30/apr/2013

@author: Christian
'''

import arch_definitive

from pymc import MCMC, MAP
from pymc.graph import dag
from pymc.Matplot import plot
import pymc
import numpy
import winsound
import time
import os
import pylab
#from pylab import hist, show

configurations = {
                  0 : { 'R':2, 'S': [2,2,2,2] , 'basename': 'arch_conf_'},
                  1 : { 'R':2, 'S': [3,2,2,2] , 'basename': 'arch_conf_'},
                  2 : { 'R':2, 'S': [3,3,2,2] , 'basename': 'arch_conf_'},
                  3 : { 'R':2, 'S': [3,3,3,2] , 'basename': 'arch_conf_'},
                  4 : { 'R':2, 'S': [3,3,3,3] , 'basename': 'arch_conf_'},
                  5 : { 'R':3, 'S': [2,2,2,2] , 'basename': 'arch_conf_'},
                  6 : { 'R':3, 'S': [3,2,2,2] , 'basename': 'arch_conf_'},
                  7 : { 'R':3, 'S': [3,3,2,2] , 'basename': 'arch_conf_'},
                  8 : { 'R':3, 'S': [3,3,3,2] , 'basename': 'arch_conf_'},
                  9 : { 'R':3, 'S': [3,3,3,3] , 'basename': 'arch_conf_'},
                  10: { 'R':2, 'S': [2,3,2,2] , 'basename': 'arch_conf_'},
                  11: { 'R':2, 'S': [2,2,3,2] , 'basename': 'arch_conf_'},
                  12: { 'R':2, 'S': [2,2,2,3] , 'basename': 'arch_conf_'},
                  13: { 'R':3, 'S': [2,3,2,2] , 'basename': 'arch_conf_'},
                  14: { 'R':3, 'S': [2,2,3,2] , 'basename': 'arch_conf_'},
                  15: { 'R':3, 'S': [2,2,2,3] , 'basename': 'arch_conf_'},
                  16: { 'R':3, 'S': [3,3,2,3] , 'basename': 'arch_conf_'},
                  17: { 'R':3, 'S': [2,2,2,4] , 'basename': 'arch_conf_'},
                  }

# ==== TRACING ====
idx_to_load = [15]

db_loaded =     [None, ] * len(idx_to_load)
model_loaded =  [None, ] * len(idx_to_load)
logp_loaded =   [None, ] * len(idx_to_load)

for i in range(len(idx_to_load)):
    key = idx_to_load[i]
    
    input_S = {1:0, 2:0, 3:0, 4:0}
    for x in range(len(configurations[key]['S'])):
        input_S[x+1] = configurations[key]['S'][x]
    
    signature = str(configurations[key]['R']) + '_' 
    for x in range(len(configurations[key]['S'])):
        signature += str(configurations[key]['S'][x])
    model_name = configurations[key]['basename'] + signature
    
    print "Loading model: " + model_name
    db_loaded[i] = pymc.database.pickle.load( model_name + '.pickle' )
    model_loaded[i] = pymc.Model(arch_definitive.getModel( configurations[key]['R'] , input_S))
    
    R_value = 0
    S_values = {1:0, 2:0, 3:0, 4:0}
    
    print model_loaded[i].stochastics

    #set the value of all stochastic to their last value
    for stochastic in model_loaded[i].stochastics:
        try:
            value = db_loaded[i].trace(stochastic.__name__)[-1]
            stochastic.value = value
            print "Setting value for " + stochastic.__name__ + ": " + str(value)
        except KeyError:
            print "No trace available for %s. " % stochastic.__name__
    
    model_loaded[i]
    for stochastic in model_loaded[i].stochastics:
        try:
            value = db_loaded[i].trace(stochastic.__name__)[-1]
            stochastic.value = value
            print "Setting value for " + stochastic.__name__ + ": " + str(value)
        except KeyError:
            print "No trace available for %s. " % stochastic.__name__
    
    # Setting values for S and R
    for stochastic in model_loaded[i].stochastics:
        if stochastic.__name__ == 'R':
            stochastic.value = R_value
        if stochastic.__name__ == 'S1':
            stochastic.value = S_values[1]
        if stochastic.__name__ == 'S2':
            stochastic.value = S_values[2]
        if stochastic.__name__ == 'S3':
            stochastic.value = S_values[3]
        if stochastic.__name__ == 'S4':
            stochastic.value = S_values[4]
    
    
    
    #Sampling
    #model_loaded[i].sample(iter=10000, burn=2000, thin=10)
    
#    directory = 'graph/newData/' + model_name
#    if( not os.path.isdir(directory) ):
#        os.mkdir(directory)
#    
#    mcmcSampler = MCMC( model_loaded[i] , db= db_loaded[i] )
#    
#    dag(model_loaded[i], format = "png", name = "graph", path = directory)
#    plot(mcmcSampler, path=directory)
    
    
print "done"
Freq = 2400 # Set Frequency To 2500 Hertz
Dur = 400 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)
winsound.Beep(Freq,Dur)