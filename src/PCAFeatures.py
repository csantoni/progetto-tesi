'''
Created on 11/apr/2013

@author: Christian
'''

import numpy
from PCA import *

def computePCAVectors(base_path, model_list):
    geom_vectors = []
    lf_vectors = []
    path_list = []
    
    for model_name in model_list:
        if model_name == 'ld' or model_name == 'OBJ_files' or model_name == 'SDF_files':
            continue
        else:
            model_path = base_path + str(model_name) + '/'
            
            comp_list = []
            comp_file = open(model_path + 'list.txt', 'r')
            for line in comp_file:
                comp_list.append(line[:-1])
            comp_file.close()
            
            for comp in comp_list:
                path = comp.split('_')
                path = model_path + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"
                path_list.append(path)
                geom_arr = numpy.load(path + 'geom.npy')
                geom_vectors.append(geom_arr)
                lf_arr = numpy.load(path + 'lf.npy')
                lf_vectors.append(lf_arr)

    print "Vector loaded: %d" % len(geom_vectors) 
    #print geom_vectors
    
    geom_A = numpy.array(geom_vectors)
    Center(geom_A)
    print "PCA geom..." ,
    geom_p = PCA( geom_A, fraction=0.7 )
    print "npc:", geom_p.npc
    print "% variance:", geom_p.sumvariance * 100
    pca_geom = geom_p.pc()
    print "pc:", pca_geom 
    print "pc sample:", pca_geom[0]
    print "pc #:", len(pca_geom)
    print "=========================================="
    lf_A = numpy.array(lf_vectors)
    Center(lf_A)
    print "PCA lf..." ,
    lf_p = PCA( lf_A, fraction=0.7 )
    print "npc:", lf_p.npc
    print "% variance:", lf_p.sumvariance * 100
    pca_lf = lf_p.pc()
    print "pc:", pca_lf 
    print "pc sample:", pca_lf[0]
    print "pc #:", len(pca_lf)
    
    for i in xrange(len(pca_lf)):
        numpy.save(path_list[i] + "70pca_geom", pca_geom[i])
        numpy.save(path_list[i] + "70pca_lf", pca_lf[i])
    
    
if __name__ == "__main__":
    base_path = '../res/tele-aliens/data/'
    model_list = [x for x in range(126, 201)]
    
    geom_vectors = []
    lf_vectors = []
    
    path_list = []
    
    for model_name in model_list:
        model_path = base_path + str(model_name) + '/'
        
        comp_list = []
        comp_file = open(model_path + 'list.txt', 'r')
        for line in comp_file:
            comp_list.append(line[:-1])
        comp_file.close()
        
        for comp in comp_list:
            path = comp.split('_')
            path = model_path + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"
            path_list.append(path)
            geom_arr = numpy.load(path + '/geom.npy')
            geom_vectors.append(geom_arr)
            lf_arr = numpy.load(path + '/lf.npy')
            lf_vectors.append(lf_arr)
        
    print "Vector loaded: %d" % len(geom_vectors) 
    #print geom_vectors
    
    geom_A = numpy.array(geom_vectors)
    Center(geom_A)
    print "PCA geom..." ,
    geom_p = PCA( geom_A, fraction=0.7 )
    print "npc:", geom_p.npc
    print "% variance:", geom_p.sumvariance * 100
    pca_geom = geom_p.pc()
    print "pc:", pca_geom 
    print "pc sample:", pca_geom[0]
    print "pc #:", len(pca_geom)
    print "=========================================="
    lf_A = numpy.array(lf_vectors)
    Center(lf_A)
    print "PCA lf..." ,
    lf_p = PCA( lf_A, fraction=0.7 )
    print "npc:", lf_p.npc
    print "% variance:", lf_p.sumvariance * 100
    pca_lf = lf_p.pc()
    print "pc:", pca_lf 
    print "pc sample:", pca_lf[0]
    print "pc #:", len(pca_lf)
    
    #for i in xrange(len(pca_lf)):
    #    numpy.save(path_list[i] + "/70pca_geom", pca_geom[i])
    #    numpy.save(path_list[i] + "/70pca_lf", pca_lf[i])