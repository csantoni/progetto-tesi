'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot
import os
import winsound

def getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ):
    n_R = input_R
    m = 35
    f_len = 33
    
    n_segments = 4
    n_N = {1:0, 2:0 ,3:0 ,4:0}
    n_S = input_S
    N_data = {}
    D_data = {}
    C_data = {}
    
    for seg in n_N.keys():
        N_data[seg] = None
        D_data[seg] = None
        C_data[seg] = None
    
    base_path = '../res/tele-aliens/data/'
    N_max = np.load(base_path + 'n2N_max.npy')
    for v in N_max:
        n_N[v[0]] = v[1] + 1
        
    for seg in n_N.keys():
        N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
        D_data[seg] = np.load(base_path + 'n2D' + str(seg) + '.npy')
        C_data[seg] = np.load(base_path + 'n2C' + str(seg) + '.npy')
    
    
    C_max = np.max( np.load(base_path + 'n2C_max.npy') )
    C_min = np.min( np.load(base_path + 'n2C_min.npy') )
    
    alpha_N = [None, ] * len(n_N.keys())
    theta_N = [None, ] * len(n_N.keys())
    cpt_N = [None, ] * len(n_N.keys())
    N = [None, ] * len(n_N.keys())
    
    alpha_S = [None, ] * len(n_N.keys())
    theta_S = [None, ] * len(n_N.keys())
    cpt_S = [None, ] * len(n_N.keys())
    S = [None, ] * len(n_N.keys())
    
    alpha_D = [None, ] * len(n_N.keys())
    theta_D = [None, ] * len(n_N.keys())
    cpt_D = [None, ] * len(n_N.keys())
    D = [None, ] * len(n_N.keys())
    
    taus_C = {}
    n_mus_C = {}
    n_tau_C = {}
    t_t = {}
    m_n = {}
    t_n = {}
    
    tau = [None, ] * len(n_N.keys())
    mu = [None, ] * len(n_N.keys())
    taus = [None, ] * len(n_N.keys())
    mus = [None, ] * len(n_N.keys())
    C = [None, ] * len(n_N.keys())
    
#    f_len = 10
#    true_cov = np.eye(f_len) * 10.0
#    true_mean = np.ones(f_len) 
#    fake_C_data = { 1: np.random.multivariate_normal(true_mean, true_cov, 90), 
#                    2: np.random.multivariate_normal(true_mean, true_cov, 90),  
#                    3: np.random.multivariate_normal(true_mean, true_cov, 90),  
#                    4: np.random.multivariate_normal(true_mean, true_cov, 90)
#                  }
    
    predN = {}
    predD = {}
    predC = {}
    
    # ======= R VARIABLE ========
#    alpha_R = mc.Uniform( name = 'alpha_R', lower = 1.0, upper = 10.0, size = n_R, trace = True, plot = True)
#    cpt_R = mc.Dirichlet( name ='cpt_R', theta = alpha_R , trace = True, plot = True)
#    R = mc.Categorical('R', p = cpt_R)
    R = mc.DiscreteUniform('R', lower = 0, upper = n_R - 1)
    
    
    # ======= N VARIABLE ========
    for i in n_N.keys():
        #alpha_N[i] = [ [1.0, ] * n_N[i] for _ in range(n_R) ]
        #alpha_N[i] = [ mc.Uninformative( name = 'alpha_N%i_R%i' % (i, j) , value = [1.0, ] * n_N[i]) for j in range(n_R) ]
        #alpha_N[i] = [ mc.Normal( name = 'alpha_N%i_R%i' % (i, j) , mu = 1.0, tau = 0.1, value = [1.0, ] * n_N[i]) for j in range(n_R) ]
        #alpha_N[i] = [ mc.Poisson( name = 'alpha_N%i_R%i' % (i, j) , mu = 1.0, value = [1.0, ] * n_N[i]) for j in range(n_R) ]
        
        alpha_N[i-1] = [ mc.Uniform( name = 'alpha_N%i_R%i' % (i, j), lower = 0.1, upper = 10.0, value = [1.0, ] * n_N[i], plot = True) for j in range(n_R) ]
        theta_N[i-1] = mc.Lambda('theta_N%i'%i, lambda alpha_N = alpha_N[i-1], R = R: alpha_N[R], plot = False)
        cpt_N[i-1]   = mc.Dirichlet(name='cpt_N%i' % i, theta = theta_N[i-1], plot = False)
        N[i-1]       = mc.Categorical('N%i'%i, cpt_N[i-1], observed=True, value=N_data[i])
        
        #predN[i] = mc.Categorical('predN%i'%i, cpt_N[i])
    
    # ======= S VARIABLE ========
    for i in n_N.keys():
        #alpha_S[i] = [ [1.0, ] * n_S[i] for j in range(n_R) ]
        #alpha_S[i] = [ mc.Exponential(name = 'alpha_S%i_R%i' % (i, j), beta = 1.0, size = n_S[i]) for j in range(n_R) ]
        #alpha_S[i] = [ mc.Uninformative( name = 'alpha_S%i_R%i' % (i, j) , value = [1.0, ] * n_S[i]) for j in range(n_R) ]
        #alpha_S[i] = [ mc.Normal( name = 'alpha_S%i_R%i' % (i, j) , mu = 1.0, tau = 0.1, value = [1.0, ] * n_S[i]) for j in range(n_R) ]
        #alpha_S[i] = [ mc.Poisson( name = 'alpha_S%i_R%i' % (i, j) , mu = 1.0, value = [1.0, ] * n_S[i]) for j in range(n_R) ]
        
#        alpha_S[i-1] = [ mc.Uniform( name = 'alpha_S%i_R%i' % (i, j), lower = 1.0, upper = 10.0, value = [1.0, ] * n_S[i], plot = False) for j in range(n_R) ]
#        theta_S[i-1] = mc.Lambda('theta_S%i'%i, lambda alpha_S = alpha_S[i-1], R = R: alpha_S[R], plot = False)
#        cpt_S[i-1] = mc.Dirichlet(name = 'cpt_S%i' % i, theta = theta_S[i-1], plot = False)
#        S[i-1] = mc.Categorical('S%i'%i, cpt_S[i-1])
        
        S[i-1] = mc.DiscreteUniform('S%i'%i, lower = 0, upper = n_S[i] - 1)
    
    # ======= D VARIABLE ========
    for i in n_N.keys():
        #alpha_D[i] = [ [1.0, ] * m for _ in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Exponential(name = 'alpha_D%i_S%i' % (i, j), beta = 1.0, size = m) for j in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Uninformative( name = 'alpha_D%i_S%i' % (i, j) , value = [1.0, ] * m ) for j in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Normal( name = 'alpha_D%i_S%i' % (i, j) , mu = 1.0, tau = 0.1, value = [1.0, ] * m ) for j in range(n_S[i]) ]
        #alpha_D[i] = [ mc.Poisson( name = 'alpha_D%i_S%i' % (i, j) , mu = 1.0, value = [1.0, ] * m ) for j in range(n_S[i]) ]
        
        #alpha_D[i] = [ mc.Uninformative( name = 'alpha_D%i_S%i' % (i, j) , value = [1.0, ] * m ) for j in range(n_S[i]) ]
        alpha_D[i-1] = [ mc.Uniform( name = 'alpha_D%i_S%i' % (i, j), lower = 0.1, upper = 10.0, value = [1.0, ] * m , plot = False) for j in range(n_S[i]) ]
        theta_D[i-1] = mc.Lambda('theta_D%i'%i, lambda alpha_D = alpha_D[i-1], S = S[i-1]: alpha_D[S], plot = True)
        cpt_D[i-1] = mc.Dirichlet(name='cpt_D%i'%i, theta = theta_D[i-1], plot = False)
        D[i-1] = mc.Categorical('D%i'%i, cpt_D[i-1], observed=True, value = D_data[i])
        
        #predD[i] = mc.Categorical('predD%i'%i, cpt_D[i])
    
    # ======= C VARIABLE ========
    
    for i in n_N.keys():
        
#        taus_C[i] =  [ np.eye(f_len) for _ in range(n_S[i]) ]
#        t_t[i] = mc.Lambda('t_t%i' % i, lambda taus_C  = taus_C[i],  S = S[i]: taus_C[S])
#        
#        n_mus_C[i] = [ mc.Uniform('mC%i_S%i' % (i,j), lower = C_min,  upper = C_max) for j in range(n_S[i]) ]
#        n_tau_C[i] = [ mc.Uniform('tC%i_S%i' % (i,j), lower = 0.0001, upper = 1.0)   for j in range(n_S[i]) ]
#        
#        m_n[i] = mc.Lambda('mu_norm_C%i' % i,  lambda n_mus = n_mus_C[i], S = S[i]: n_mus[S])
#        t_n[i] = mc.Lambda('tau_norm_C%i' % i, lambda n_tau = n_tau_C[i], S = S[i]: n_tau[S])
#        
#        tau[i] = mc.Wishart('tau_C%i' % i, n = f_len + 1, Tau = t_t[i])
#        mu[i] = mc.Normal('mu_C%i' % i, mu = m_n[i], tau = t_n[i], size = f_len)
        
#        C[i] = mc.MvNormalCov('C%i' % i, mu = mu[i], C = tau[i], observed = True, value = C_data[i]) 
#        predC[i] = mc.MvNormalCov('predC%i' % i, mu = mu[i], C = tau[i])

#         n=4, Tau=np.eye(2), value=inv(np.eye(2)*10.0)) 
#         mu=0, tau=1e-5, value=[0,0],size=2) 

        taus[i-1] = [ mc.Wishart('tau_C%i_S%i' % (i, j),
                                n = f_len + 1,
                                Tau = np.eye(f_len), 
                                value = np.linalg.inv( np.eye(f_len  ) * 10.0 ) )            
                   for j in range(n_S[i]) ]
        
        mus[i-1] =  [ mc.Normal( 'mu_C%i_S%i' % (i, j),
                               mu = 0.0 ,
                               tau = 1e-5,
                               size = f_len, 
                               value = [0, ] * f_len)
                   for j in range(n_S[i]) ]
        
        mu[i-1] = mc.Lambda('mu_norm_C%i' % i,    lambda n_mus = mus[i-1],      Si = S[i-1]: n_mus[Si])
        tau[i-1] = mc.Lambda('tau_norm_C%i' % i,  lambda n_taus = taus[i-1],    Si = S[i-1]: n_taus[Si])
        
        C[i-1] = mc.MvNormal('C%i' % i, mu = mu[i-1], tau = tau[i-1], observed = True, value = C_data[i]) 
        #predC[i] = mc.MvNormal('predC%i' % i, mu = mu[i], tau = tau[i])
    
    print locals()
    
    #return [ R, S, alpha_N, theta_N, cpt_N, N, alpha_S, theta_S, cpt_S, alpha_D, theta_D, cpt_D, D ]
    return locals()
    
if __name__ == "__main__":
    A = mc.Model(getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ))
    dag(A, format="png", name="arch_def_2", path="graph/")

#    mod = mc.MAP(getModel( input_R = 3, input_S = {1:2, 2:2 ,3:2 ,4:2} ))
#    mod.fit()
#    print "AIC: " + str(mod.AIC)
#    print "BIC: " + str(mod.BIC)
#    print
#    mcmcSampler = mc.MCMC( mod.variables , db='pickle', dbname = 'prova_beta.pickle')
#
    locals = getModel( input_R = 3, input_S = {1:2, 2:2 ,3:2 ,4:2} )
    #mcmcSampler = mc.MCMC( locals , db='pickle', dbname = 'prova_beta.pickle')
    mcmcSampler = mc.MCMC(locals)
    mcmcSampler.sample(iter = 1000)
    #mcmcSampler.sample(iter = 5000, burn = 1000, thin=10)
    
    #mcmcSampler.db.close()
    
    directory = 'graph/prova_arch2'
    if( not os.path.isdir(directory) ):
        os.mkdir(directory)
    
    plot(mcmcSampler, path=directory)
    
    print "done"
    Freq = 2400 # Set Frequency To 2500 Hertz
    Dur = 400 # Set Duration To 1000 ms == 1 second
    winsound.Beep(Freq,Dur)
    winsound.Beep(Freq,Dur)