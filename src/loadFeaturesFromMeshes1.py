'''
Created on 06/giu/2013

@author: Christian
'''

from utility.mmesh      import mMesh
from utility.check_extension import *

from OpenGL.GLUT import *
from OpenGL.GL import *
from OpenGL.GLU import *

from OpenGL.arrays import vbo

from OpenGL.GL.ARB.vertex_buffer_object import *

from os import walk
from os import rename
from os import remove
from shutil import copy
from time import time

import posNormalizer

def loadModel(m, path, segpath):
    m.loadModel(path, segpath)

meshesDirectory     = '../res/chairs/shapes/'
segmentsDirectory   = '../res/chairs/gt/'

meshesList = []
segmentsList = []
meshesPath = []

meshesToBeSelected = 100

for (_, _, filenames) in walk(meshesDirectory):
    meshesList.extend(filenames)
    break

#Check for VBOs Support
g_fVBOSupported = False

start = time()
meshes = [None, ] * len(meshesList)
for x in range(len(meshesList)):
    meshes[x] = mMesh(g_fVBOSupported)
print "Mesh allocation done in: "+ str( time() - start )

#POSITION NORMALIZATION - BRINGS CENTROIDS TO ORIGIN
start = time()
for x in range(len(meshesList)):
    posNormalizer.normalize(meshesDirectory + meshesList[x])
print "Normalization done in: "+ str( time() - start )

#FEATURE EXTRACTION - PHASE ONE
start = time()
for x in range(len(meshesList)):
    parts = meshesList[x].split('.')
    loadModel(meshes[x], meshesDirectory + meshesList[x], segmentsDirectory + parts[0] +  '.seg')
print "Feature extraction (phase 1) done in: "+ str( time() - start )