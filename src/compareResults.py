'''
Created on 30/apr/2013

@author: Christian
'''

import pymc
import numpy
import winsound
import pylab


def get_bayesian_factor(logp1, logp2):
    from numpy import exp, log
#    print '\t first  term: ' + str(pymc.flib.logsum(-logp1) - log(len(logp1)))
#    print '\t second term: ' + str(pymc.flib.logsum(-logp2) - log(len(logp2)))
#    print '\t difference: ' + str(pymc.flib.logsum(-logp1) - log(len(logp1)) - ( pymc.flib.logsum(-logp2) - log(len(logp2)) ))
    K = exp( pymc.flib.logsum(-logp1) - log(len(logp1)) - ( pymc.flib.logsum(-logp2) - log(len(logp2)) ) )
#    print '\t K value: ' + str(K)
    return K

configurations = {
                  0 : { 'R':2, 'S': [2,2,2,2] , 'basename': 'arch_conf_'},
                  1 : { 'R':2, 'S': [3,2,2,2] , 'basename': 'arch_conf_'},
                  2 : { 'R':2, 'S': [3,3,2,2] , 'basename': 'arch_conf_'},
                  3 : { 'R':2, 'S': [3,3,3,2] , 'basename': 'arch_conf_'},
                  4 : { 'R':2, 'S': [3,3,3,3] , 'basename': 'arch_conf_'},
                  5 : { 'R':3, 'S': [2,2,2,2] , 'basename': 'arch_conf_'},
                  6 : { 'R':3, 'S': [3,2,2,2] , 'basename': 'arch_conf_'},
                  7 : { 'R':3, 'S': [3,3,2,2] , 'basename': 'arch_conf_'},
                  8 : { 'R':3, 'S': [3,3,3,2] , 'basename': 'arch_conf_'},
                  9 : { 'R':3, 'S': [3,3,3,3] , 'basename': 'arch_conf_'},
                  10: { 'R':2, 'S': [2,3,2,2] , 'basename': 'arch_conf_'},
                  11: { 'R':2, 'S': [2,2,3,2] , 'basename': 'arch_conf_'},
                  12: { 'R':2, 'S': [2,2,2,3] , 'basename': 'arch_conf_'},
                  13: { 'R':3, 'S': [2,3,2,2] , 'basename': 'arch_conf_'},
                  14: { 'R':3, 'S': [2,2,3,2] , 'basename': 'arch_conf_'},
                  15: { 'R':3, 'S': [2,2,2,3] , 'basename': 'arch_conf_'},
                  16: { 'R':3, 'S': [3,3,2,3] , 'basename': 'arch_conf_'},
                  17: { 'R':3, 'S': [2,2,2,4] , 'basename': 'arch_conf_'},
                  }

# === SAMPLING ===

idx_to_load = [x for x in range(18)]

logp_loaded =   [None, ] * len(idx_to_load)

for i in range(len(idx_to_load)):
    key = idx_to_load[i]
    
    signature = str(configurations[key]['R']) + '_' 
    for x in range(len(configurations[key]['S'])):
        signature += str(configurations[key]['S'][x])
    model_name = configurations[key]['basename'] + signature
    
    path = "graph/" + model_name
    print "Loading logp: " + model_name
    logp_loaded[i] = numpy.load(path + "/summed_logp.npy")

#SANITATION
#deltas = numpy.zeros(len(logp_loaded[0]) - 1)
threshold = 1.0e+6
for i in range(len(idx_to_load)): 
    for i_sample in range(len(logp_loaded[i]) - 1):
        delta = numpy.abs( logp_loaded[i][i_sample] - logp_loaded[i][i_sample + 1] )
        if delta > threshold:
            print "oscillation found: " + str(delta)
            print "\t " + str(i_sample) + ": " + str(logp_loaded[i][i_sample]) 
            print "\t " + str(i_sample + 1) + ": "  + str(logp_loaded[i][i_sample + 1])
            logp_loaded[i][i_sample + 1] = logp_loaded[i][i_sample] 
       

pylab.figure(0)
for x in range(len(idx_to_load)):
    print logp_loaded[x][-5:]
    pylab.plot(logp_loaded[x][-500:], label="Model #" + str(idx_to_load[x]))
pylab.legend(loc = "lower right", shadow = True)

pylab.figure(1)
for x in range(len(idx_to_load)):
    pylab.plot(logp_loaded[x][-750:], label="Model #" + str(idx_to_load[x]))
pylab.legend(loc = "lower right", shadow = True)


results = numpy.zeros((len(idx_to_load), len(idx_to_load)))
for x in range(len(idx_to_load)):
    for y in range(len(idx_to_load)):
        print "Comparing %i, %i" % (x, y)
        results[x,y] = get_bayesian_factor( logp_loaded[x][-500:], logp_loaded[y][-500:] )
print

results_min = numpy.zeros((len(idx_to_load), len(idx_to_load)))
start = 300
end = 401
for x in range(len(idx_to_load)):
    for y in range(len(idx_to_load)):
        print "Comparing %i, %i" % (x, y)
        results_min[x,y] = get_bayesian_factor( logp_loaded[x][start:end], logp_loaded[y][start:end] )

for row in results:
    s = ''
    for cell in row:
        s += ' %.2e' % cell
    print s
print

for row in results_min:
    s = ''
    for cell in row:
        s += ' %.2e' % cell
    print s
print

for idx, row in enumerate(results):
    s = str(idx_to_load[idx]) + ' | ' if (idx_to_load[idx] < 10) else str(idx_to_load[idx]) + '| '
    for cell in row:
        if cell == 0.0:
            s += ' 0.0'
        elif cell == 1.0:
            s += ' 1.0'
        elif cell < 1.0:
            s += ' 0.0'
        elif cell >= 100.0:
            s += ' inf'
        else:
            s += ' %.2e' % cell
    print s
print

for idx, row in enumerate(results_min):
    s = str(idx_to_load[idx]) + ' | ' if (idx_to_load[idx] < 10) else str(idx_to_load[idx]) + '| '
    for cell in row:
        if cell == 0.0:
            s += ' 0.0'
        elif cell == 1.0:
            s += ' 1.0'
        elif cell < 1.0:
            s += ' 0.0'
        elif cell >= 100.0:
            s += ' inf'
        else:
            s += ' %.2e' % cell
    print s
print

pylab.show()
    
print "done"
Freq = 2400 # Set Frequency To 2500 Hertz
Dur = 400 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)
winsound.Beep(Freq,Dur)