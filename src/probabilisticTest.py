'''
Created on 30/apr/2013

@author: Christian
'''
import probabilisticModeltest
from pymc import MCMC, MAP, Metropolis, AdaptiveMetropolis
from pymc.graph import dag
from pymc.Matplot import plot
import pymc
from pylab import hist, show
import winsound


# === MAP FITTING ===
#M = MAP(probabilisticModeltest)
#M.fit()
#M.fit(method="fmin_powell", iterlim=20, tol=.001, verbose=2)

# === SAMPLING ===
##MC = MCMC(probabilisticModeltest)
MC = MCMC(probabilisticModeltest, db='pickle', dbname='rid2_model6_10000_0_10.pickle')
print MC.db
print MC.step_method_dict
##MC.use_step_method(AdaptiveMetropolis, probabilisticModeltest)
##MC.sample(iter=2000, burn=1000, thin=10)
MC.sample(iter=10000, burn=1000, thin=10)
MC.db.close()

# === LOADING FROM FILE ===
#db = pymc.database.pickle.load('model.pickle')


# === PLOTTING ===
dag(MC, format="png", name="graph", path="graph/")
plot(MC, path="graph/")

# === STATS ===
#print MC.step_method_dict
#print "Stats:"
#print MC.stats()

print "done"
Freq = 2400 # Set Frequency To 2500 Hertz
Dur = 400 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)
winsound.Beep(Freq,Dur)