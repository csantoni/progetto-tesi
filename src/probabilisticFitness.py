'''
Created on 30/apr/2013

@author: Christian
'''

import arch_definitive

from pymc import MCMC, MAP
from pymc.graph import dag
from pymc.Matplot import plot
import pymc
import numpy
import winsound
import time
from pylab import hist, show




# === MAP FITTING ===
#M = MAP(probabilisticModeltest)
#M.fit(iterlim=5)
#print "Total time: " + str(end - begin)
#print "AIC: " + str(M.AIC)
#print "BIC: " + str(M.BIC)

# === SAMPLING ===
if(False):
#    begin = time.time()
#    mod = MAP(arch_dir_1)
#    mod.fit(iterlim=5, tol=0.001)
#    print "AIC: " + str(mod.AIC)
#    print "BIC: " + str(mod.BIC)
#    print
#    print "total time: " + str( (time.time() - begin) / 60.0 )
    MC_3 = MCMC(arch_1, db='pickle', dbname='arch_1.pickle')
    #MC_3 = MCMC(mod.variables, db='pickle', dbname='arch_dir_1.pickle')
    MC_3.sample(iter=10000, burn=2000, thin=10)
    MC_3.db.close()
    dag(MC_3, format="png", name="graph", path="graph/arch_1")
    plot(MC_3, path="graph/arch_1")
    print "Plotting finished"
    print
    
# === LOADING FROM FILE ===
models = [
          ['arch_likelihood.pickle', arch_likelihood, 'arch_likelihood'],                       #0
          ['arch_likelihood_min.pickle', arch_likelihood, 'arch_likelihood_min'],               #1
          ['arch_like_0.pickle', arch_likelihood, 'arch_like_0'],                               #2
          ['arch_like_1.pickle', arch_likelihood_1, 'arch_like_1'],                             #3
          ['arch_like_2.pickle', arch_likelihood_2, 'arch_like_2'],                             #4
          ['arch_1.pickle', arch_1, 'arch_1'],                                                  #5
          ['arch_dir_1.pickle', arch_dir_1, 'arch_dir_1'],                                      #6
         ]

model_idx = 5

geweke = True
gelman = False
tracing = True

if (geweke or gelman or tracing):
    db = pymc.database.pickle.load(models[model_idx][0])
    print db

var = [ 'cpt_N1', 'cpt_N2', 'cpt_N3', 'cpt_N4',
        'cpt_S1', 'cpt_S2', 'cpt_S3', 'cpt_S4',
        'm_n1', 'm_n2', 'm_n3', 'm_n4', 
        't_n1', 't_n2', 't_n3', 't_n4',
        't_t1', 't_t2', 't_t3', 't_t4' ]

#    var = [ 'cpt_N1', 'cpt_N2', 'cpt_N3', 'cpt_N4',
#            'cpt_S1', 'cpt_S2', 'cpt_S3', 'cpt_S4',
#            'cpt_D1', 'cpt_D2', 'cpt_D3', 'cpt_D4',
#            'mu_Norm1', 'mu_Norm2', 'mu_Norm3', 'mu_Norm4']

if (geweke):
    for v in var:
        print "geweke " + v
        scores = pymc.geweke(db.trace(v)[:], first=0.2, last=0.5, intervals=20)
        pymc.Matplot.geweke_plot(scores, name=(str(model_idx) + '_' + v), path='./graph/' + models[model_idx][2] + '/geweke')

if(gelman):
    for v in var:
        print "gelman " + v
        print pymc.gelman_rubin(db.trace(v)[:])

if(tracing):
    model = MCMC(models[model_idx][1], db=db)

    print "deviance"
    print db.trace('deviance')[-30:]
    print
    n_samples = db.trace('deviance').length()
    logp = numpy.empty(n_samples, numpy.double)
    
    print model.stochastics
    print
    
    #loop over all samples
    for i_sample in xrange(n_samples):
        #set the value of all stochastic to their 'i_sample' value
        for stochastic in model.stochastics:
            try:
                value = db.trace(stochastic.__name__)[i_sample]
                stochastic.value = value
            except KeyError:
                print "No trace available for %s. " % stochastic.__name__
        #get logp
        logp[i_sample] = model.logp
    
    print logp[-30:]
#    hist(logp)
#    plot(logp, path='./graph/' + models[model_idx][2] + '/', name="logp")
#    show()
    

# === PLOTTING ===
#dag(MC, format="png", name="graph", path="graph/")
#plot(MC, path="graph/")

# === STATS ===
#print MC.step_method_dict
#print "Stats:"
#print MC.stats()

print "done"
Freq = 2400 # Set Frequency To 2500 Hertz
Dur = 400 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)
winsound.Beep(Freq,Dur)