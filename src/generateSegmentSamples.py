'''
Created on 29/mag/2013

@author: Christian
'''

from operator import itemgetter
import numpy
import random
import scipy.spatial
import sys
import pickle
from random import choice

nodes = [ 'predN1', 'predN2', 'predN3', 'predN4',
          'predD1', 'predD2', 'predD3', 'predD4',
          'predC1', 'predC2', 'predC3', 'predC4',
          'dev' ]

n_samples = 500
feat_len = 14

# FILES STRUCTURE 
# chain file: EPOCH<TAB>VALUE
# index file: VAR<TAB>BEG EPOCH<TAB>END EPOCH
'''
def getDatabase():
    base_path = '../res/tele-aliens/data/'
    model_list = [x for x in range(126, 138)]
    model_list.extend([x for x in range(143, 201)])
    # D[i] = lista di adiacenza tra componenti dei segmenti i per ogni modello
    D_data = { }
    D_data_arr = { }
    D_data_final = { }
    # lista delle pcs features per componente di segmento i per modello
    C_data = { }
    C_data_map = { }
    seg_num = 4 
    
    for model_name in model_list:
        model_path = base_path + str(model_name) + '/'
        
        comp_list = []
        comp_file = open(model_path + 'list.txt', 'r')
        for line in comp_file:
            comp_list.append(line[:-1])
        comp_file.close()
        
        for comp in comp_list:
            path = comp.split('_')
            seg_number = int(path[3])
            comp_number = int(path[5][:-4])
            path = model_path + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"
                
            scale_arr = numpy.load(path + 'scale.npy')
            geom_arr = numpy.load(path + '70pca_geom.npy')
            lf_arr = numpy.load(path + '70pca_lf.npy')
            
            scale_arr = numpy.array([ numpy.linalg.norm(scale_arr[0]), numpy.linalg.norm(scale_arr[1]), numpy.linalg.norm(scale_arr[2]) ])
            c = numpy.concatenate((scale_arr, geom_arr, lf_arr))
            c_map = numpy.array([model_name, seg_number, comp_number])
            
            if seg_number in C_data:
                C_data_map[seg_number].append(c_map)
                C_data[seg_number].append(c)
            else:
                C_data[seg_number] = []
                C_data_map[seg_number] = []
                C_data_map[seg_number].append(c_map)
                C_data[seg_number].append(c)
                  
        
        adj_output = open(model_path + 'adj.pkl', 'r')
        adj2 = pickle.load(adj_output)
        adj_output.close()
        adjacency_matrix = adj2
        
        complete_adj = {  }
        for s in range(seg_num):
            complete_adj[s+1] = {}
            for s_2 in range(seg_num):
                complete_adj[s+1][s_2+1] = 0
            
        for k in adj2.keys():
            for k_2 in adj2.keys():
                complete_adj[k][k_2] = adjacency_matrix[k][k_2]
          
        for k in adj2.keys():
            s = "\t %d | " % k
            for k_2 in adj2.keys():
                s += " %d |" % (adj2[k][k_2])
    
        for k in complete_adj.keys():
            s = "\t %d | " % (k)
            d_item = ''
            for k_2 in complete_adj.keys():
                s += " %d |" % (complete_adj[k][k_2])
                d_item += str(complete_adj[k][k_2])
            
            if k in D_data:
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            else:
                D_data[k] = {}
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            
            if k in D_data_arr:
                D_data_arr[k].append( complete_adj[k].values() )
            else:
                D_data_arr[k] = []
                D_data_arr[k].append( complete_adj[k].values() )
            
            if k in D_data_final:
                D_data_final[k].append(d_item)
            else:
                D_data_final[k] = []
                D_data_final[k].append(d_item)
            
    map = {}
    for seg in D_data.keys():
        for val in D_data[seg].keys():
            if val in map:
                map[val] += 1
            else:
                map[val] = 1
    
    map_final = {}
    v = 0
    for val in sorted(map.keys()):
        map_final[val] = v
        v+=1
    print "map final"
    print map_final
    for seg in D_data_final.keys():
        for i in xrange( len(D_data_final[seg]) ):
            D_data_final[seg][i] = map_final[D_data_final[seg][i]]
    
    for seg in C_data.keys():
        C_data[seg] = numpy.array(C_data[seg])
    for seg in D_data_final.keys():
        D_data_final[seg] = numpy.array(D_data_final[seg])
        
    return (C_data, C_data_map, map_final)

def getCategoricalSamples(path, styles, n_samples):
    file_chain = open(path, 'r')
    pred = numpy.zeros( (styles, n_samples) )
    for (idx, val) in enumerate(file_chain):
        pred[ int(idx / n_samples) ][ idx % n_samples ] = int(float(val.split('\t')[1][:-1]))
    return pred

def getMVNSamples(path, styles, n_samples, feat_len):
    file_chain = open(path, 'r')    
    temp_pred = numpy.zeros( (n_samples, feat_len * 2) )
    for (idx, val) in enumerate(file_chain):
        sample_idx = idx % n_samples
        col_idx = int(idx / n_samples)
        temp_pred[ sample_idx, col_idx  ] = float(val.split('\t')[1][:-1])
    pred = numpy.zeros( (styles, n_samples, feat_len) )
    for i in range(n_samples):
        for j in range(len(temp_pred[i])):
            pred[ int(j / feat_len), i, j % feat_len ] = temp_pred[i, j]
    return pred

def initData(r, s1,s2,s3,s4):
    predC1 = getMVNSamples('samples/fit_3_2222_predC1CODAchain1.txt', 2, 500, 14)
    predC2 = getMVNSamples('samples/fit_3_2222_predC2CODAchain1.txt', 2, 500, 14)
    predC3 = getMVNSamples('samples/fit_3_2222_predC3CODAchain1.txt', 2, 500, 14)
    predC4 = getMVNSamples('samples/fit_3_2222_predC4CODAchain1.txt', 2, 500, 14)
    
    s1_style = s1
    s2_style = s2
    s3_style = s3
    s4_style = s4
    
    samples_C1 = predC1[s1_style - 1]
    samples_C2 = predC2[s2_style - 1]
    samples_C3 = predC3[s3_style - 1]
    samples_C4 = predC4[s4_style - 1]
    
    C_data, C_data_map, map_final = getDatabase()
    
    return [samples_C1, samples_C2, samples_C3, samples_C4,
            C_data, C_data_map, map_final]
'''

def getDatabase(base_path, model_list, seg_num):
#    base_path = '../res/tele-aliens/data/'
#    model_list = [x for x in range(126, 138)]
#    model_list.extend([x for x in range(143, 201)])

    # D[i] = lista di adiacenza tra componenti dei segmenti i per ogni modello
    D_data = { }
    D_data_arr = { }
    D_data_final = { }
    # lista delle pcs features per componente di segmento i per modello
    C_data = { }
    C_data_map = { }
    
    for model_name in model_list:
        model_path = base_path + str(model_name) + '/'
        
        comp_list = []
        comp_file = open(model_path + 'list.txt', 'r')
        for line in comp_file:
            comp_list.append(line[:-1])
        comp_file.close()
        
        for comp in comp_list:
            path = comp.split('_')
            seg_number = int(path[3])
            comp_number = int(path[5][:-4])
            path = model_path + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"
                
            scale_arr = numpy.load(path + 'scale.npy')
            geom_arr = numpy.load(path + '70pca_geom.npy')
            lf_arr = numpy.load(path + '70pca_lf.npy')
            
            scale_arr = numpy.array([ numpy.linalg.norm(scale_arr[0]), numpy.linalg.norm(scale_arr[1]), numpy.linalg.norm(scale_arr[2]) ])
            c = numpy.concatenate((scale_arr, geom_arr, lf_arr))
            c_map = numpy.array([model_name, seg_number, comp_number])
            
            if seg_number in C_data:
                C_data_map[seg_number].append(c_map)
                C_data[seg_number].append(c)
            else:
                C_data[seg_number] = []
                C_data_map[seg_number] = []
                C_data_map[seg_number].append(c_map)
                C_data[seg_number].append(c)
                  
        
        adj_output = open(model_path + 'adj.pkl', 'r')
        adj2 = pickle.load(adj_output)
        adj_output.close()
        adjacency_matrix = adj2
        
        complete_adj = {  }
        for s in range(seg_num):
            complete_adj[s+1] = {}
            for s_2 in range(seg_num):
                complete_adj[s+1][s_2+1] = 0
            
        for k in adj2.keys():
            for k_2 in adj2.keys():
                complete_adj[k][k_2] = adjacency_matrix[k][k_2]
          
        for k in adj2.keys():
            s = "\t %d | " % k
            for k_2 in adj2.keys():
                s += " %d |" % (adj2[k][k_2])
    
        for k in complete_adj.keys():
            s = "\t %d | " % (k)
            d_item = ''
            for k_2 in complete_adj.keys():
                s += " %d |" % (complete_adj[k][k_2])
                d_item += str(complete_adj[k][k_2])
            
            if k in D_data:
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            else:
                D_data[k] = {}
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            
            if k in D_data_arr:
                D_data_arr[k].append( complete_adj[k].values() )
            else:
                D_data_arr[k] = []
                D_data_arr[k].append( complete_adj[k].values() )
            
            if k in D_data_final:
                D_data_final[k].append(d_item)
            else:
                D_data_final[k] = []
                D_data_final[k].append(d_item)
            
    map = {}
    for seg in D_data.keys():
        for val in D_data[seg].keys():
            if val in map:
                map[val] += 1
            else:
                map[val] = 1
    
    map_final = {}
    v = 0
    for val in sorted(map.keys()):
        map_final[val] = v
        v+=1
    print "map final"
    print map_final
    for seg in D_data_final.keys():
        for i in xrange( len(D_data_final[seg]) ):
            D_data_final[seg][i] = map_final[D_data_final[seg][i]]
    
    for seg in C_data.keys():
        C_data[seg] = numpy.array(C_data[seg])
    for seg in D_data_final.keys():
        D_data_final[seg] = numpy.array(D_data_final[seg])
        
    return (C_data, C_data_map, map_final)

def getCategoricalSamples(path, styles, n_samples):
    file_chain = open(path, 'r')
    pred = numpy.zeros( (styles, n_samples) )
    for (idx, val) in enumerate(file_chain):
        pred[ int(idx / n_samples) ][ idx % n_samples ] = int(float(val.split('\t')[1][:-1]))
    return pred

def getMVNSamples(path, styles, n_samples, feat_len):
    
    file_chain = open(path, 'r')    
    print n_samples, feat_len * 2
    temp_pred = numpy.zeros( (n_samples, feat_len * 2) )
    for (idx, val) in enumerate(file_chain):
        sample_idx = idx % n_samples
        col_idx = int(idx / n_samples)
        temp_pred[ sample_idx, col_idx  ] = float(val.split('\t')[1][:-1])
    pred = numpy.zeros( (styles, n_samples, feat_len) )
    for i in range(n_samples):
        for j in range(len(temp_pred[i])):
            pred[ int(j / feat_len), i, j % feat_len ] = temp_pred[i, j]
    return pred

def initData(basePathBUGS, basePath, model_list, n_samples, R_styles, S_styles, feat_len, R, S):
    
    predN = {}
    for x in range(len(S)):
        predN[x+1] = getCategoricalSamples(basePathBUGS + 'fit_predN%dCODAchain1.txt' % (x+1), R_styles, n_samples)
    
    predD = {}
    for x in range(len(S)):
        predD[x+1] = getCategoricalSamples(basePathBUGS + 'fit_predD%dCODAchain1.txt' % (x+1), S_styles[x], n_samples)
    
    predC = {}
    for x in range(len(S)):
        predC[x+1] = getMVNSamples(basePathBUGS + 'fit_predC%dCODAchain1.txt' % (x+1), S_styles[x], n_samples, feat_len)
        
#    predN1 =  getCategoricalSamples('samples/fit_3_2222_predN1CODAchain1.txt', 3, 500)
#    predN2 =  getCategoricalSamples('samples/fit_3_2222_predN2CODAchain1.txt', 3, 500)
#    predN3 =  getCategoricalSamples('samples/fit_3_2222_predN3CODAchain1.txt', 3, 500)
#    predN4 =  getCategoricalSamples('samples/fit_3_2222_predN4CODAchain1.txt', 3, 500)
#    predD1 =  getCategoricalSamples('samples/fit_3_2222_predD1CODAchain1.txt', 2, 500)
#    predD2 =  getCategoricalSamples('samples/fit_3_2222_predD2CODAchain1.txt', 2, 500)
#    predD3 =  getCategoricalSamples('samples/fit_3_2222_predD3CODAchain1.txt', 2, 500)
#    predD4 =  getCategoricalSamples('samples/fit_3_2222_predD4CODAchain1.txt', 2, 500)
#    predC1 = getMVNSamples('samples/fit_3_2222_predC1CODAchain1.txt', 2, 500, 14)
#    predC2 = getMVNSamples('samples/fit_3_2222_predC2CODAchain1.txt', 2, 500, 14)
#    predC3 = getMVNSamples('samples/fit_3_2222_predC3CODAchain1.txt', 2, 500, 14)
#    predC4 = getMVNSamples('samples/fit_3_2222_predC4CODAchain1.txt', 2, 500, 14)
#    r_style  = r
#    s1_style = s1
#    s2_style = s2
#    s3_style = s3
#    s4_style = s4
    
    samples_N = {}
    for x in range(len(S)):
        samples_N[x+1] = predN[x+1][R - 1]
    
    samples_D = {}
    for x in range(len(S)):
        samples_D[x+1] = predD[x+1][S[x] - 1]
    
    samples_C = {}
    for x in range(len(S)):
        samples_C[x+1] = predC[x+1][S[x] - 1]
    
#    samples_N1 = predN1[r_style - 1]
#    samples_N2 = predN2[r_style - 1]
#    samples_N3 = predN3[r_style - 1]
#    samples_N4 = predN4[r_style - 1]
#    
#    samples_D1 = predD1[s1_style - 1]
#    samples_D2 = predD2[s2_style - 1]
#    samples_D3 = predD3[s3_style - 1]
#    samples_D4 = predD4[s4_style - 1]
#    
#    samples_C1 = predC1[s1_style - 1]
#    samples_C2 = predC2[s2_style - 1]
#    samples_C3 = predC3[s3_style - 1]
#    samples_C4 = predC4[s4_style - 1]
    
    C_data, C_data_map, map_final = getDatabase(basePath, model_list, seg_num)
    
#    return [samples_N1, samples_N2, samples_N3, samples_N4, 
#            samples_D1, samples_D2, samples_D3, samples_D4, 
#            samples_C1, samples_C2, samples_C3, samples_C4,
#            C_data, C_data_map, map_final]

    return [ samples_N, samples_D, samples_C,
             C_data, C_data_map, map_final ]

def getSampleCandidates(segment_number, num_of_selected, 
                        samples_N, samples_D, samples_C,
                        C_data, C_data_map, map_final):
    
    random.seed()
    samples = None
    
    chosenComponents = []
    
#    if segment_number == 1:
#        samples = samples_C1
#    elif segment_number == 2:
#        samples = samples_C2
#    elif segment_number == 3:
#        samples = samples_C3
#    elif segment_number == 4:
#        samples = samples_C4     
#    
    samples = samples_C[segment_number]
    
    for _ in range( num_of_selected ):
        sample = random.choice(samples)
        distances = []
        for sample_idx in range(len(C_data[segment_number])):
            distances.append( (sample_idx, scipy.spatial.distance.cosine(sample, C_data[segment_number][sample_idx])) )
        
        selected = sorted(distances, key=itemgetter(1))[0]
        chosenComponents.append(tuple(numpy.array(C_data_map[segment_number][selected[0]], dtype=numpy.int32)))
        
    return chosenComponents

if __name__ == "__main__":
    from os import walk
    
    basePath = '../res/chairs/data/'
    basePathBUGS = basePath + "openBUGS/"
    
    meshesList = []
    meshesPath = []
    
    for (_, dirnames, filenames) in walk(basePath):
        meshesList.extend(dirnames)
        break
    #print meshesList
    
    meshesList = [ x for x in meshesList if (x != 'ld' and x != 'OBJ_files' and x != 'SDF_files' and x != 'openBUGS')]
    
    starter = open( basePathBUGS + "bestStarter.txt", "r")
    
    seg_num, bestR, bestS, feat_len = starter.read().split(' ')
    seg_num = int(seg_num)
    feat_len = int(feat_len)
    bestR = int(bestR)
    bestS = [ int(x) for x in bestS.split(',') ]
    
    R = 2
    S = [1,1,1]
    n_samples = 500
    segment = 1
    S[segment - 1] = 1
    
    data = initData(basePathBUGS, basePath, meshesList, n_samples, bestR, bestS, feat_len, R, S)
    
    numberOfSamples = 50
    finalSamples = getSampleCandidates(segment, numberOfSamples, *data )
    
#    seg_style = [1,1,1,1]
#    segment = 4
#    seg_style[segment - 1] = 2
#    data = initData(0, *seg_style)
#    
#    numberOfSamples = 50
#    finalSamples = getSampleCandidates(segment, numberOfSamples, *data )
    
    print "====================== RESULTS ======================"
    print len(finalSamples)
    print finalSamples
    print len(set(finalSamples))
    res =  numpy.array(list(set(finalSamples)))
    print res
    
    #numpy.save("samples/segments/samples_S%d_%d" % (segment, seg_style[segment - 1]), res)
    finalSamples = numpy.array(finalSamples)
    numpy.save(basePathBUGS + "full_samples_S%d_%d" % (segment, S[segment - 1]), finalSamples)
    
    