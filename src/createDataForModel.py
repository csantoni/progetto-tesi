'''
Created on 07/mag/2013

@author: Christian
'''
import numpy
import sys
import pickle

base_path = '../res/tele-aliens/data/'
model_list = [x for x in range(126, 201)]

path_list = []

#Massimo numero di componenti per segmento
n_N = { }

# N[i] = lista di numero di componenti per segmento i per ogni modello
N_data = { }

# D[i] = lista di adiacenza tra componenti dei segmenti i per ogni modello
D_data = { }
D_data_final = { }
# lista delle pcs features per componente di segmento i per modello
C_data = { }


max_C = None
min_C = None

seg_names = [ 'seg_1', 'seg_2', 'seg_3', 'seg_4' ]
seg_num = 4 

for model_name in model_list:
    model_path = base_path + str(model_name) + '/'
    
    comp_list = []
    comp_file = open(model_path + 'list.txt', 'r')
    for line in comp_file:
        comp_list.append(line[:-1])
    comp_file.close()
    
    N_item = {}
    
    for comp in comp_list:
        path = comp.split('_')
        seg_number = int(path[3])
        comp_number = int(path[5][:-4])
        path = model_path + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"
        
        if seg_number in N_item:
            N_item[seg_number] += 1
        else:
            N_item[seg_number] = 1
            
        scale_arr = numpy.load(path + '/scale.npy')
        geom_arr = numpy.load(path + '/pca_geom.npy')
        #lf_arr = numpy.load(path + '/pca_lf.npy')
        
        #c = numpy.concatenate((scale_arr.flatten(), geom_arr, lf_arr))
        c = numpy.concatenate((scale_arr.flatten(), geom_arr))
        
        if max_C == None:
            max_C = [sys.float_info.min, ] * len(c)
            min_C = [sys.float_info.max, ] * len(c)
        
        max_C = numpy.maximum(max_C, c)
        min_C = numpy.minimum(max_C, c)
        
        if seg_number in C_data:
            C_data[seg_number].append(c)
        else:
            C_data[seg_number] = []
            C_data[seg_number].append(c)
        
        
    for i in N_item.keys():
        if i in N_data:
            N_data[i].append(N_item[i])
        else:
            N_data[i] = []
            N_data[i].append(N_item[i])
    
    adj_output = open(model_path + 'adj.pkl', 'r')
    adj2 = pickle.load(adj_output)
    adj_output.close()
    adjacency_matrix = adj2
    for k in adj2.keys():
        d_item = ''
        for k_2 in adj2.keys():
            d_item += str(adjacency_matrix[k][k_2])
        
        if len(d_item) == 4:
            if k in D_data:
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            else:
                D_data[k] = {}
                
            if k in D_data_final:
                D_data_final[k].append(d_item)
            else:
                D_data_final[k] = []
                D_data_final[k].append(d_item)
                
#print D_data
#print D_data_final

map = {}
for seg in D_data.keys():
    for val in D_data[seg].keys():
        if val in map:
            map[val] += 1
        else:
            map[val] = 1

#print map
#print sorted(map.keys())
#print len(sorted(map.keys()))

map_final = {}
v = 0
for val in sorted(map.keys()):
    map_final[val] = v
    v+=1
#print map_final
for seg in D_data_final.keys():
    for i in xrange( len(D_data_final[seg]) ):
        D_data_final[seg][i] = map_final[D_data_final[seg][i]]

N_max = []
for seg in N_data.keys():
    N_data[seg] = numpy.array(N_data[seg])
    N_max.append( [seg, numpy.max(N_data[seg])] )
N_max = numpy.array(N_max)
for seg in C_data.keys():
    C_data[seg] = numpy.array(C_data[seg])
for seg in D_data_final.keys():
    D_data_final[seg] = numpy.array(D_data_final[seg])

print N_data
print N_max
print C_data
print D_data_final
print "Sample c length: " + str(len(C_data[1][0]))
for seg in N_data.keys():
    numpy.save(base_path + "nN" + str(seg), N_data[seg])
numpy.save(base_path + "nN_max", N_max)
numpy.save(base_path + "nC_max", max_C)
numpy.save(base_path + "nC_min", min_C)
for seg in C_data.keys():
    numpy.save(base_path + "nC" + str(seg), C_data[seg])
for seg in D_data_final.keys():
    numpy.save(base_path + "nD" + str(seg), D_data_final[seg])