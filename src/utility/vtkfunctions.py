'''
Created on 03/giu/2013

@author: Christian
'''

import vtk

def arrayToPolyPoints(verts):
    pts = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()

    for v in verts:
        id = pts.InsertNextPoint(v[0], v[1], v[2])
        vertices.InsertNextCell(1)
        vertices.InsertCellPoint(id)
        
    return pts, vertices

def polyDataToSequentialArray(polydata):
    nbpts = polydata.GetNumberOfPoints()
    nbcls = polydata.GetNumberOfCells()
    
    verts = []
    faces = []
   
    for i in range(nbpts):
        P = polydata.GetPoint(i)
        verts.append( [P[0],P[1],P[2]] )

    nbPtsCell = 3
    ptIdList = vtk.vtkIdList()
    # * write the faces *
    for i in range(nbcls):
        polydata.GetCellPoints(i,ptIdList)
        temp = []
        for j in range(nbPtsCell):
            temp.append(ptIdList.GetId(j))
        faces.append(temp)
    
    return verts, faces
        