'''
Created on 03/giu/2013

@author: Christian
'''
from OpenGL.GLUT import *
from OpenGL.GL import *
from OpenGL.GLU import *

import random

class MultiButton(object):

    def __init__(self, x, y, w, h, c, oc, label):
        self.x = x      #X position on the screen
        self.y = y      #Y position on the screen
        self.w = w      #Width
        self.h = h      #Height
        self.middle = int(self.y + self.h / 2.0)
        self.c = c      #Colour (r,g,b)
        self.oc = oc    #Outline Colour (r,g,b)
        self.colors = []
        self.label = label
        self.fontx = self.x + (self.w - len(self.label) * glutBitmapWidth(GLUT_BITMAP_TIMES_ROMAN_24, ord('a'))) / 2
        self.fonty = self.y + (self.h + 14) / 2;
        
        self.enableHighlighting = True
        self.enableCallback = True
        self.type = "MultiButton"
                
        self.highlighted = False
        self.disabled = False
        self.callback = None
        
        self.indices = None
        self.max = 9
        self.step = 0

    def setIndices(self, indices):
        self.indices = indices
        self.step = int(self.w / float(len(indices)))
        self.w = self.step * len(self.indices)

    def setColors(self, seg_list):
        color_map_comp = [
             #[0.8, 0.8, 0.8],[0.8, 0.8, 0.8],[0.8, 0.8, 0.8],[0.8, 0.8, 0.8],[0.8, 0.8, 0.8],
             [0.5, 0.0, 0.0],
             [0.0, 0.5, 0.1],
             [0.0, 0.0, 0.8],
             [0.8, 0.8, 0.0],
             [1.0, 1.0, 0.4],
             [1.0, 1.0, 0.4],
             [1.0, 1.0, 0.4],
             [1.0, 1.0, 0.4],
             [1.0, 1.0, 0.4],
             ]
        
        for s in seg_list:
            self.colors.append( color_map_comp[s-1] )

    def setCallback(self, c):
        self.callback = c
        
    def checkHit(self, x, y, click):
        if ((x > self.x) and (x < self.x + self.w)):
            if ((y > self.y) and (y < self.y + self.h)):
                return True
        return False
    
    def mouseUp(self):
        pass
    
    def newConf(self,x,y):
        
        cell_idx = (x - self.x) / self.step
        if cell_idx > (len(self.indices) - 1):
            cell_idx = len(self.indices) - 1
            
        if y > self.middle:
            #Lower region
            #print "Clicked low %d" % cell_idx
            if self.indices[cell_idx] > 0:
                self.indices[cell_idx] -= 1
            else:
                self.indices[cell_idx] = self.max
        else:
            #Upper region
            #print "Clicked high %d" % cell_idx
            if self.indices[cell_idx] == self.max:
                self.indices[cell_idx] = 0
            else:
                self.indices[cell_idx] += 1
        
#        for k in range(len(self.indices)):
#            self.indices[k] = random.randint(0,self.max)
        print " ========= Indices selected" + str(self.indices) + " ========="
    
    def draw(self):
        
        if (self.disabled):
            glColor3f(0.4, 0.4, 0.4)
        else:
            glColor3f(0.0, 0.0, 0.0)
        
        glColor3f(1, 1, 1)
        glRasterPos3f( self.x + self.w + 20, self.y + 20, 0 )
        for c in self.indices:
            glutBitmapCharacter( GLUT_BITMAP_TIMES_ROMAN_24, ord(str(c)) )
            glutBitmapCharacter( GLUT_BITMAP_TIMES_ROMAN_24, ord(' ') )

        glLineWidth(1)
        glColor3f(0.4, 0.4, 0.4)
        for k in range(1, len(self.indices)):
            glBegin(GL_LINE_STRIP)
            glVertex2i( self.x + k * self.step , self.y      )
            glVertex2i( self.x + k * self.step , self.y + self.h )
            glEnd()
        
        glBegin(GL_LINE_STRIP)
        glVertex2i( self.x, self.middle )
        glVertex2i( self.x + self.w, self.middle )
        glEnd()
        
        if (self.disabled):
            temp = (0.5 * x for x in self.c)
            glColor3f(*temp)
        else:
            if (self.highlighted):
                temp = (1.2 * x for x in self.c)
                glColor3f(*temp)
            else:
                glColor3f(*self.c)
    
        glBegin(GL_QUADS)
        glVertex2i( self.x          , self.y      )
        glVertex2i( self.x          , self.y + self.h )
        glVertex2i( self.x + self.w , self.y + self.h )
        glVertex2i( self.x + self.w , self.y      )
        glEnd();

        '''
        Draw an outline around the button with width 3
        '''
        glLineWidth(2)

        glColor3f(*self.oc)

        glBegin(GL_LINE_STRIP)
        glVertex2i( self.x + self.w , self.y      )
        glVertex2i( self.x          , self.y      )
        glVertex2i( self.x          , self.y + self.h )
        glEnd()

        bottomOutline = (0.5 * x for x in self.oc)
        glColor3f(*bottomOutline);

        glBegin(GL_LINE_STRIP)
        glVertex2i( self.x          , self.y + self.h )
        glVertex2i( self.x+self.w   , self.y + self.h )
        glVertex2i( self.x+self.w   , self.y      )
        glEnd()


        for k in range(len(self.indices)):
            glColor3f(*self.colors[k])
            
            glBegin(GL_QUADS)
            glVertex2i( self.x + k * self.step , self.y + self.h )
            glVertex2i( self.x + k * self.step + self.step , self.y + self.h )
            glVertex2i( self.x + k * self.step + self.step , self.y + self.h + 10 )
            glVertex2i( self.x + k * self.step , self.y + self.h + 10 )
            glEnd()
        
