'''
Created on 29/mag/2013

@author: Christian
'''

from operator import itemgetter
import numpy
import random
import scipy.spatial
import sys
import pickle
from random import choice

n_samples = 500
feat_len = 14

# FILES STRUCTURE 
# chain file: EPOCH<TAB>VALUE
# index file: VAR<TAB>BEG EPOCH<TAB>END EPOCH

def getDatabase(base_path, model_list, seg_num):
#    base_path = '../res/tele-aliens/data/'
#    model_list = [x for x in range(126, 138)]
#    model_list.extend([x for x in range(143, 201)])

    # D[i] = lista di adiacenza tra componenti dei segmenti i per ogni modello
    D_data = { }
    D_data_arr = { }
    D_data_final = { }
    # lista delle pcs features per componente di segmento i per modello
    C_data = { }
    C_data_map = { }
    
    for model_name in model_list:
        model_path = base_path + str(model_name) + '/'
        
        comp_list = []
        comp_file = open(model_path + 'list.txt', 'r')
        for line in comp_file:
            comp_list.append(line[:-1])
        comp_file.close()
        
        for comp in comp_list:
            path = comp.split('_')
            seg_number = int(path[3])
            comp_number = int(path[5][:-4])
            path = model_path + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"
                
            scale_arr = numpy.load(path + 'scale.npy')
            geom_arr = numpy.load(path + '70pca_geom.npy')
            lf_arr = numpy.load(path + '70pca_lf.npy')
            
            scale_arr = numpy.array([ numpy.linalg.norm(scale_arr[0]), numpy.linalg.norm(scale_arr[1]), numpy.linalg.norm(scale_arr[2]) ])
            c = numpy.concatenate((scale_arr, geom_arr, lf_arr))
            c_map = numpy.array([model_name, seg_number, comp_number])
            
            if seg_number in C_data:
                C_data_map[seg_number].append(c_map)
                C_data[seg_number].append(c)
            else:
                C_data[seg_number] = []
                C_data_map[seg_number] = []
                C_data_map[seg_number].append(c_map)
                C_data[seg_number].append(c)
                  
        
        adj_output = open(model_path + 'adj.pkl', 'r')
        adj2 = pickle.load(adj_output)
        adj_output.close()
        adjacency_matrix = adj2
        
        complete_adj = {  }
        for s in range(seg_num):
            complete_adj[s+1] = {}
            for s_2 in range(seg_num):
                complete_adj[s+1][s_2+1] = 0
            
        for k in adj2.keys():
            for k_2 in adj2.keys():
                complete_adj[k][k_2] = adjacency_matrix[k][k_2]
          
        for k in adj2.keys():
            s = "\t %d | " % k
            for k_2 in adj2.keys():
                s += " %d |" % (adj2[k][k_2])
    
        for k in complete_adj.keys():
            s = "\t %d | " % (k)
            d_item = ''
            for k_2 in complete_adj.keys():
                s += " %d |" % (complete_adj[k][k_2])
                d_item += str(complete_adj[k][k_2])
            
            if k in D_data:
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            else:
                D_data[k] = {}
                if d_item in D_data[k]:
                    D_data[k][d_item] += 1
                else:
                    D_data[k][d_item] = 1
            
            if k in D_data_arr:
                D_data_arr[k].append( complete_adj[k].values() )
            else:
                D_data_arr[k] = []
                D_data_arr[k].append( complete_adj[k].values() )
            
            if k in D_data_final:
                D_data_final[k].append(d_item)
            else:
                D_data_final[k] = []
                D_data_final[k].append(d_item)
            
    map = {}
    for seg in D_data.keys():
        for val in D_data[seg].keys():
            if val in map:
                map[val] += 1
            else:
                map[val] = 1
    
    map_final = {}
    v = 0
    for val in sorted(map.keys()):
        map_final[val] = v
        v+=1
    print "map final"
    print map_final
    for seg in D_data_final.keys():
        for i in xrange( len(D_data_final[seg]) ):
            D_data_final[seg][i] = map_final[D_data_final[seg][i]]
    
    for seg in C_data.keys():
        C_data[seg] = numpy.array(C_data[seg])
    for seg in D_data_final.keys():
        D_data_final[seg] = numpy.array(D_data_final[seg])
        
    return (C_data, C_data_map, map_final)

def getCategoricalSamples(path, styles, n_samples):
    file_chain = open(path, 'r')
    pred = numpy.zeros( (styles, n_samples) )
    for (idx, val) in enumerate(file_chain):
        pred[ int(idx / n_samples) ][ idx % n_samples ] = int(float(val.split('\t')[1][:-1]))
    return pred

def getMVNSamples(path, styles, n_samples, feat_len):
    
    file_chain = open(path, 'r')    
    print n_samples, feat_len * 2
    temp_pred = numpy.zeros( (n_samples, feat_len * 2) )
    for (idx, val) in enumerate(file_chain):
        sample_idx = idx % n_samples
        col_idx = int(idx / n_samples)
        temp_pred[ sample_idx, col_idx  ] = float(val.split('\t')[1][:-1])
    pred = numpy.zeros( (styles, n_samples, feat_len) )
    for i in range(n_samples):
        for j in range(len(temp_pred[i])):
            pred[ int(j / feat_len), i, j % feat_len ] = temp_pred[i, j]
    return pred

def initData(basePathBUGS, basePath, model_list, n_samples, R_styles, S_styles, feat_len, R, S):
    
    predN = {}
    for x in range(len(S)):
        predN[x+1] = getCategoricalSamples(basePathBUGS + 'fit_predN%dCODAchain1.txt' % (x+1), R_styles, n_samples)
    
    predD = {}
    for x in range(len(S)):
        predD[x+1] = getCategoricalSamples(basePathBUGS + 'fit_predD%dCODAchain1.txt' % (x+1), S_styles[x], n_samples)
    
    predC = {}
    for x in range(len(S)):
        predC[x+1] = getMVNSamples(basePathBUGS + 'fit_predC%dCODAchain1.txt' % (x+1), S_styles[x], n_samples, feat_len)
        
    samples_N = {}
    for x in range(len(S)):
        samples_N[x+1] = predN[x+1][R - 1]
    
    samples_D = {}
    for x in range(len(S)):
        samples_D[x+1] = predD[x+1][S[x] - 1]
    
    samples_C = {}
    for x in range(len(S)):
        samples_C[x+1] = predC[x+1][S[x] - 1]
    
    C_data, C_data_map, map_final = getDatabase(basePath, model_list, seg_num)

    return [ samples_N, samples_D, samples_C,
             C_data, C_data_map, map_final ]

def getSampleCandidates(samples_N1, samples_N2, samples_N3, samples_N4, 
                        samples_D1, samples_D2, samples_D3, samples_D4, 
                        samples_C1, samples_C2, samples_C3, samples_C4,
                        C_data, C_data_map, map_final):
    
    num_of_selected = 10
    random.seed()
    #For segment number 1 to expand to other 3
    chosenN1 = int(random.choice(samples_N1)) - 1
    print "Chosen N1 value: " + str(chosenN1)
    print
    print "Chosing for segment 1"
    bestCandidatesC1 = [None, ] * chosenN1
    for i in range(chosenN1):
        bestCandidatesC1[i] = []
        sample = random.choice(samples_C1)
        print "sample " + str(i)
        distances = []
        for sample_idx in range(len(C_data[1])):
            distances.append( (sample_idx, scipy.spatial.distance.cosine(sample, C_data[1][sample_idx])) )
        
        selected_C1 = sorted(distances, key=itemgetter(1))[:num_of_selected]     
        for c in selected_C1:
            print str(c[1]) + "\t" + str(C_data_map[1][c[0]])
            bestCandidatesC1[i].append(C_data_map[1][c[0]])
            
    chosenN2 = int(random.choice(samples_N2)) - 1
    print "Chosen N2 value: " + str(chosenN2)
    print
    print "Chosing for segment 2"
    bestCandidatesC2 = [None, ] * chosenN2
    #Per cercare di rispettare la simmetria
    sample2 = random.choice(samples_C2)
    for i in range(chosenN2):
        bestCandidatesC2[i] = []
        #sample = random.choice(samples_C2)
        print "sample " + str(i)
        distances = []
        for sample_idx in range(len(C_data[2])):
            distances.append( (sample_idx, scipy.spatial.distance.cosine(sample2, C_data[2][sample_idx])) )
        
        selected_C2 = sorted(distances, key=itemgetter(1))[:num_of_selected]     
        for c in selected_C2:
            print str(c[1]) + "\t" + str(C_data_map[2][c[0]])
            bestCandidatesC2[i].append(C_data_map[2][c[0]])
            
    chosenN3 = int(random.choice(samples_N3)) - 1
    while (chosenN3 == 0):
        chosenN3 = int(random.choice(samples_N3)) - 1
    chosenN3 = 1
    
    print "Chosen N3 value: " + str(chosenN3)
    print
    print "Chosing for segment 3"
    bestCandidatesC3 = [None, ] * chosenN3
    for i in range(chosenN3):
        bestCandidatesC3[i] = []
        sample = random.choice(samples_C3)
        print "sample " + str(i)
        distances = []
        for sample_idx in range(len(C_data[3])):
            distances.append( (sample_idx, scipy.spatial.distance.cosine(sample, C_data[3][sample_idx])) )
        
        selected_C3 = sorted(distances, key=itemgetter(1))[:num_of_selected]     
        for c in selected_C3:
            print str(c[1]) + "\t" + str(C_data_map[3][c[0]])
            bestCandidatesC3[i].append(C_data_map[3][c[0]])
            
    chosenN4 = int(random.choice(samples_N4)) - 1
    print "Chosen N4 value: " + str(chosenN4)
    print
    print "Chosing for segment 4"
    bestCandidatesC4 = [None, ] * chosenN4
    for i in range(chosenN4):
        bestCandidatesC4[i] = []
        sample = random.choice(samples_C4)
        print "sample " + str(i)
        distances = []
        for sample_idx in range(len(C_data[4])):
            distances.append( (sample_idx, scipy.spatial.distance.cosine(sample, C_data[4][sample_idx])) )
        
        selected_C4 = sorted(distances, key=itemgetter(1))[:num_of_selected]     
        for c in selected_C4:
            print str(c[1]) + "\t" + str(C_data_map[4][c[0]])
            bestCandidatesC4[i].append(C_data_map[4][c[0]])
    
    selected_list = { 1:bestCandidatesC1, 2:bestCandidatesC2, 3:bestCandidatesC3, 4:bestCandidatesC4 }

    return selected_list

def getSampleCandidates2(num_of_selected, simmetry, 
                         samples_N, samples_D, samples_C, 
                         C_data, C_data_map, map_final, ):
    
    random.seed()
    
    bestCandidatesC = {}
    selected_list = { }
    for x in samples_N.keys():
        if (simmetry[x-1] == False):
            
            maxN = numpy.max(samples_N[x]) - 1
            chosenN = random.randint(0, maxN)
            if chosenN == 0:
                selected_list[x] = []
            else:
                print "Chosen N%d value: %d" % ( x, chosenN)
                print
                bestCandidatesC[x] = [None, ] * chosenN
                for i in range(chosenN):
                    bestCandidatesC[x][i] = []
                    for c in range(num_of_selected):
                        bestCandidatesC[x][i].append(numpy.array(random.choice(C_data_map[x]), dtype=numpy.int32))
                                
                selected_list[x] = bestCandidatesC[x]
        else:
            maxN = numpy.max(samples_N[x]) - 1
            chosenN = random.randint(0, maxN)
            print "Chosen N%d value: %d" % (x, chosenN)
            print
            if chosenN == 0:
                selected_list[x] = []
            else:
                bestCandidatesC[x] = [None, ] * chosenN
                
                temp = [None, ] * num_of_selected
                for c in range(num_of_selected):
                    temp[c] =  numpy.array(random.choice(C_data_map[x]), dtype=numpy.int32)
                
                for i in range(chosenN):
                    bestCandidatesC[x][i] = temp[:]
                
                selected_list[x] = bestCandidatesC[x]

    return selected_list

if __name__ == "__main__":
    
    from os import walk
    
    basePath = '../res/tele-aliens/data/'
    basePathBUGS = basePath + "openBUGS/"
    
    meshesList = []
    meshesPath = []
    
    for (_, dirnames, filenames) in walk(basePath):
        meshesList.extend(dirnames)
        break
    #print meshesList
    
    meshesList = [ x for x in meshesList if (x != 'ld' and x != 'OBJ_files' and x != 'SDF_files' and x != 'openBUGS')]
    
    starter = open( basePathBUGS + "bestStarter.txt", "r")
    
    seg_num, bestR, bestS, feat_len = starter.read().split(' ')
    seg_num = int(seg_num)
    feat_len = int(feat_len)
    bestR = int(bestR)
    bestS = [ int(x) for x in bestS.split(',') ]
    
    print seg_num, bestR, bestS, feat_len * 2

    
    R = 3
    S = [2,2,2,2]
    n_samples = 500
    
#    data = initData(r_style, *s_style)
    data = initData(basePathBUGS, basePath, meshesList, n_samples, bestR, bestS, feat_len, R, S)

    numberOfSelected = 10
    numberOfSamples = 30
    finalSamples = []
    
    for x in range(numberOfSamples):
        selected_list = getSampleCandidates2(numberOfSelected, [False, False, False, False], *data)
        
        cand_count = 0
        print "candidates = ["
        for seg_key in selected_list.keys():
            for candidates in selected_list[seg_key]:
                cand_count += 1
                s = "[ "
                for el in candidates:
                    s += "[ %d, %d, %d ]," % (el[0], el[1], el[2]) 
                s = s[:-1]
                s += " ],"
                print s
        print "]"
        print "indices = " + str([0, ] * cand_count)
        
        flat_list = []
        for seg_key in selected_list.keys():
            for candidates in selected_list[seg_key]:
                flat_list.append(candidates)
                
        flat_list = numpy.array(flat_list)
        print flat_list
        finalSamples.append(flat_list)
        
    print "====================== RESULTS ======================"
    finalSamples = numpy.array(finalSamples)
    for sample in finalSamples:
        print sample
    
    numpy.save(basePathBUGS + "rand_samples_R%d_S%s" % (R, ''.join([str(x) for x in S]) ), finalSamples)
    
    