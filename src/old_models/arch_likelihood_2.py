'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot

n_R = 2
m = 35
f_len = 33

n_segments = 4
n_N = {1:0, 2:0 ,3:0 ,4:0}
n_S = {1:3, 2:3 ,3:2 ,4:2}
N_data = {}
D_data = {}
C_data = {}

for seg in n_N.keys():
    N_data[seg] = None
    D_data[seg] = None
    C_data[seg] = None

base_path = '../res/tele-aliens/data/'
N_max = np.load(base_path + 'n2N_max.npy')
for v in N_max:
    n_N[v[0]] = v[1]
    
for seg in n_N.keys():
    N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
    D_data[seg] = np.load(base_path + 'n2D' + str(seg) + '.npy')
    C_data[seg] = np.load(base_path + 'n2C' + str(seg) + '.npy')


C_max = np.max( np.load(base_path + 'n2C_max.npy') )
C_min = np.min( np.load(base_path + 'n2C_min.npy') )

alpha_N = {}
theta_N = {}
cpt_N = {}
N = {}
predN = {}
new_data_N = {}

alpha_S = {}
theta_S = {}
cpt_S = {}
S = {}

alpha_D = {}
theta_D = {}
cpt_D = {}
D = {}
predD = {}

taus_C = {}
n_mus_C = {}
n_tau_C = {}
t_t = {}
m_n = {}
t_n = {}
tau = {}
mu = {}
C = {}
predC = {}

def get_N_data(R, value):
    new_data = []
    for v in value:
        new_data.append(n_N[i] * R + v)
    return new_data

def get_D_data(S, value, m):
    new_data = []
    #print value
    for v in value:
        new_data.append(m * S + v)
    return new_data

# ======= R VARIABLE ========
cpt_R = mc.Dirichlet(name='cpt_R', theta = [1.0, ] * n_R)
R = mc.Categorical('R', p=cpt_R, value=0)


# ======= N VARIABLE ========

for i in n_N.keys():
    theta_N[i] = [1.0, ] * (n_N[i] * n_R)
    #print "length theta_N%i : %i" % ( i, n_N[i] * n_R ) 
    cpt_N[i] = mc.Dirichlet(name='cpt_N%i'%i, theta = theta_N[i])
    N[i] = mc.Categorical('N%i'%i, cpt_N[i], observed=True, value=get_N_data(R, N_data[i]))
    

# ======= S VARIABLE ========
for i in n_N.keys():
    theta_S[i] = [1.0, ] * (n_S[i] * n_R)
    #print "length theta_S%i : %i" % ( i, n_S[i] * n_R ) 
    cpt_S[i] = mc.Dirichlet(name='cpt_S%i'%i, theta = theta_S[i])
    S[i] = mc.Categorical('S%i'%i, cpt_S[i], value=0)



# ======= D VARIABLE ========
for i in n_N.keys():
    theta_D[i] = [1.0, ] * (m * n_S[i])
    #print "length theta_D%i : %i" % ( i, n_S[i] * m ) 
    cpt_D[i] = mc.Dirichlet(name='cpt_D%i'%i, theta = theta_D[i])
    D[i] = mc.Categorical('D%i'%i, cpt_D[i], observed=True, value=get_D_data(S[i] - (S[i] % n_S[i]), D_data[i], m))


# ======= C VARIABLE ========
for i in n_N.keys():
    taus_C[i] = [np.eye(f_len) for _ in range(n_S[i])]
    n_mus_C[i] = [mc.Uniform('mC%i%i'%(i,j), lower=C_min, upper=C_max) for j in range(n_S[i])]
    n_tau_C[i] = [mc.Uniform('tC%i%i'%(i,j), lower=0.0001, upper=5.0) for j in range(n_S[i])]
    
    t_t[i] = mc.Lambda('t_t%i'%i, lambda taus_C = taus_C[i], S=(S[i] - (S[i] % n_S[i]) - 1): taus_C[S])
    m_n[i] = mc.Lambda('m_n%i'%i, lambda n_mus_C = n_mus_C[i], S=(S[i] - (S[i] % n_S[i]) - 1): n_mus_C[S])
    t_n[i] = mc.Lambda('t_n%i'%i, lambda n_tau_C = n_tau_C[i], S=(S[i] - (S[i] % n_S[i]) - 1): n_tau_C[S])

    tau[i] = mc.Wishart('tau%i'%i, n=f_len + 1, Tau=t_t[i])
    mu[i] = mc.Normal('mu%i'%i, mu=m_n[i], tau=t_n[i], size=f_len)
    
    C[i] = mc.MvNormalCov('C%i'%i, mu=mu[i], C=tau[i], observed=True, value=C_data[i]) 


#@mc.stochastic(trace=True, observed=True, plot=False)
#def likeC1(value=C_data[1], mu=mu[1], C=tau[1]):
#    return mc.mv_normal_cov_like(value, mu, C)
#
#@mc.stochastic(trace=True, observed=True, plot=False)
#def likeC2(value=C_data[2], mu=mu[2], C=tau[2]):
#    return mc.mv_normal_cov_like(value, mu, C)
#
#@mc.stochastic(trace=True, observed=True, plot=False)
#def likeC3(value=C_data[3], mu=mu[3], C=tau[3]):
#    return mc.mv_normal_cov_like(value, mu, C)
#
#@mc.observed
#def likeC4(value=C_data[4], mu=mu[4], C=tau[4]):
#    return mc.mv_normal_cov_like(value, mu, C)

#A = mc.MCMC(locals())
#dag(A, format="png", name="test", path="graph/arch_likelihood/")

#MC = mc.MCMC(locals())
#MC.sample(iter=10000, burn=5000, thin=10)
#dag(MC, format="png", name="graph", path="graph/arch_likelihood")
#plot(MC, path="graph/arch_likelihood")
