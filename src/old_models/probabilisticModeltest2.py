'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot

n_R = 2
n_N = 4
n_S = 3

#Suppose R = {0,1}
alpha_R = mc.Uniform('alpha_R', lower= 1.0, upper=10.0, size=n_R)
cpt_R = mc.CompletedDirichlet( 'cpt_R', D = mc.Dirichlet(name='d_cpt_R', theta = alpha_R))
R = mc.Categorical('R', p=cpt_R)

#Suppose S = {0,1,2}
s_space = n_R * n_S
alpha_S = mc.Uniform('alpha_S', lower= 1.0, upper=10.0, size=s_space)
cpt_S = mc.CompletedDirichlet( 'cpt_S', D = mc.Dirichlet(name='d_cpt_S', theta = alpha_S))
S = mc.Categorical('S', p=cpt_S)

A = mc.MCMC(locals())
dag(A, format="png", name="newModel", path="graph/")

print "Done"