'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot

n_R = 2
n_N = 4
n_S = 4
m = 10
f_len = 10

fake_D_data = [random.randint(0, m-1) for _ in range(200)]
true_cov = np.eye(f_len) * 10.0
true_mean = np.ones(f_len) 
fake_C_data = np.random.multivariate_normal(true_mean, true_cov, 50)
fake_N_data = [random.randint(0, n_N - 1) for _ in range(200)]

#print "Initial data: " + str(fake_N_data)
counts = [0, ] * n_N
for x in fake_N_data:
    counts[x] += 1
print "Counts: " + str(counts)

#Suppose R = {0,1}
#alpha_R = mc.Uniform('alpha_R', lower= 1.0, upper=200.0, size=n_R+1)
#cpt_R = mc.CompletedDirichlet( name='cpt_R', D = mc.Dirichlet(name='d_cpt_R', theta = alpha_R) )
cpt_R = mc.Dirichlet(name='cpt_R', theta = [1.0, ] * n_R)
R = mc.Categorical('R', p=cpt_R)

#Suppose N = {0,1,2,3}
#alpha_N = [mc.Uniform('alpha_N_%i' % j, lower= 1.0, upper=200.0, size=n_N) for j in xrange(n_R + 1)]
#alpha_N = [mc.Poisson('alpha_N_%i' % j, mu = [1.0, ] * n_N) for j in xrange(n_R + 1)]
alpha_N = [ [1.0, ] * n_N for _ in xrange(n_R) ]
#etaN = [mc.CompletedDirichlet( name='etaN_%i' % j, D = mc.Dirichlet(name='d_etaN_%i' % j, theta = alpha_N[j]) ) for j in xrange(n_R)]
#etaN = [ mc.Dirichlet(name='etaN_%i' % j, theta = alpha_N[j]) for j in xrange(n_R)]
def n_select(alpha_N = alpha_N, R=R):
    #print "R value: " + str(R)
    return alpha_N[R]

theta_N = mc.Lambda('theta_N',n_select)
cpt_N = mc.Dirichlet(name='cpt_N', theta = theta_N)
N = mc.Categorical('N', cpt_N, observed=True, value=fake_N_data)

#Suppose S = {0,1}
#alpha_S = [mc.Uniform('alpha_S_%i' % j, lower= 1.0, upper=200.0, size=n_S) for j in xrange(n_R + 1)]
alpha_S = [ [1.0, ] * n_S for _ in xrange(n_R) ]
#etaS = [mc.CompletedDirichlet( name='etaS_%i' % j, D = mc.Dirichlet(name='d_etaS_%i' % j, theta = alpha_S[j]) ) for j in xrange(n_R)]
#etaS = [ mc.Dirichlet(name='etaS_%i' % j, theta = alpha_S[j]) for j in xrange(n_R)]
def s_select(alpha_S = alpha_S, R=R):
    return alpha_S[R]
theta_S = mc.Lambda('theta_S', s_select)
cpt_S = mc.Dirichlet(name='cpt_S', theta = theta_S)
S = mc.Categorical('S', cpt_S, value=0)

# TODO: MAP DATA TO CATEGORIES
# ALTERNATIVE: multinomial with n = component_number ^ 2

#alpha_D = [mc.Uniform('alpha_D_%i' % j, lower= 1.0, upper=200.0, size=m+1) for j in xrange(n_S)]
alpha_D = [ [1.0, ] * m for _ in xrange(n_S) ]
#etaD = [mc.CompletedDirichlet( name='etaD_%i' % j, D = mc.Dirichlet(name='d_etaD_%i' % j, theta = alpha_D[j]) ) for j in xrange(n_S)]
#etaD = [mc.Dirichlet(name='etaD_%i' % j, theta = alpha_D[j]) for j in xrange(n_S)]
def d_select(alpha_D = alpha_D, S=S):
    return alpha_D[S]
theta_D = mc.Lambda('theta_D', d_select)
cpt_D = mc.Dirichlet(name='cpt_D', theta = theta_D)
D = mc.Categorical('D', cpt_D, observed=True, value=fake_D_data)


taus_C = [np.eye(f_len) for i in range(n_S)]
#n_mus_C = [0.0 for i in range(n_S)]
#n_tau_C = [10.0 for i in range(n_S)]

n_mus_C = [mc.Uniform('mC%i'%i, lower=-20.0, upper=20.0) for i in range(n_S)]
n_tau_C = [mc.Uniform('tC%i'%i, lower=0.0001, upper=1.0) for i in range(n_S)]

#tau_C = [mc.Wishart('tau%i'%i, n=f_len + 1, Tau=np.eye(f_len)) for i in range(n_S)]
#mu_C =  [mc.Normal('mu%i'%i, mu=0.0, tau=1.0, size=f_len) for i in range(n_S)]

def t_t_select(taus_C = taus_C, S=S):
    return taus_C[S]
def m_n_select(n_mus_C=n_mus_C, S=S):
    return n_mus_C[S]
def t_n_select(n_tau_C=n_tau_C, S=S):
    return n_tau_C[S]
#def mu_C_select(mu_C=mu_C, S=S):
#    return mu_C[S]

t_t = mc.Lambda('t_t', t_t_select)
m_n = mc.Lambda('m_n', m_n_select)
t_n = mc.Lambda('t_n', t_n_select)


tau = mc.Wishart('tau', n=f_len + 1, Tau=t_t)
mu = mc.Normal('mu', mu=m_n, tau=t_n, size=f_len)
#mu = mc.Lambda('mu', mu_C_select)

C = mc.MvNormalCov('C', mu=mu, C=tau, observed=True, value=fake_C_data) 

#A = mc.MCMC(locals())
#dag(A, format="png", name="test", path="graph/")