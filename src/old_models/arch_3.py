'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot

n_R = 4
m = 33
f_len = 33

n_segments = 4
n_N = {1:0, 2:0 ,3:0 ,4:0}
n_S = {1:2, 2:2 ,3:2 ,4:2}
N_data = {}
D_data = {}
C_data = {}

for seg in n_N.keys():
    N_data[seg] = None
    D_data[seg] = None
    C_data[seg] = None

base_path = '../res/tele-aliens/data/'
N_max = np.load(base_path + 'nN_max.npy')
for v in N_max:
    n_N[v[0]] = v[1] + 1
    
for seg in n_N.keys():
    N_data[seg] = np.load(base_path + 'nN' + str(seg) + '.npy')
    D_data[seg] = np.load(base_path + 'nD' + str(seg) + '.npy')
    C_data[seg] = np.load(base_path + 'nC' + str(seg) + '.npy')


C_max = np.max( np.load(base_path + 'nC_max.npy') )
C_min = np.min( np.load(base_path + 'nC_min.npy') )

#print n_N
#print N_data
#print C_data
#print D_data

#fake_N_data = { 1: [random.randint(0, n_N[1] - 1) for _ in range(200)], 
#                2: [random.randint(0, n_N[2] - 1) for _ in range(200)], 
#                3: [random.randint(0, n_N[3] - 1) for _ in range(200)],
#                4: [random.randint(0, n_N[4] - 1) for _ in range(200)]
#              }
#
#fake_D_data = { 1: [random.randint(0, m-1) for _ in range(200)], 
#                2: [random.randint(0, m-1) for _ in range(200)], 
#                3: [random.randint(0, m-1) for _ in range(200)], 
#                4: [random.randint(0, m-1) for _ in range(200)]
#              }
#
#f_len = 30
#true_cov = np.eye(f_len) * 10.0
#true_mean = np.ones(f_len) 
#fake_C_data = { 1: np.random.multivariate_normal(true_mean, true_cov, 90), 
#                2: np.random.multivariate_normal(true_mean, true_cov, 90),  
#                3: np.random.multivariate_normal(true_mean, true_cov, 90),  
#                4: np.random.multivariate_normal(true_mean, true_cov, 90)
#              }

alpha_N = {}
theta_N = {}
cpt_N = {}
N = {}
predN = {}

alpha_S = {}
theta_S = {}
cpt_S = {}
S = {}

alpha_D = {}
theta_D = {}
cpt_D = {}
D = {}
predD = {}

taus_C = {}
n_mus_C = {}
n_tau_C = {}
t_t = {}
m_n = {}
t_n = {}
tau = {}
mu = {}
C = {}
predC = {}

# ======= R VARIABLE ========
cpt_R = mc.Dirichlet(name='cpt_R', theta = [1.0, ] * n_R)
R = mc.Categorical('R', p=cpt_R, value=0)


# ======= N VARIABLE ========
for i in n_N.keys():
    alpha_N[i] = [ [1.0, ] * n_N[i] for _ in range(n_R) ]
    theta_N[i] = mc.Lambda('theta_N%i'%i, lambda alpha_N=alpha_N[i], R=R: alpha_N[R])
    cpt_N[i] = mc.Dirichlet(name='cpt_N%i'%i, theta = theta_N[i])
    N[i] = mc.Categorical('N%i'%i, cpt_N[i], observed=True, value=N_data[i])
    predN[i] = mc.Categorical('predN%i'%i, cpt_N[i])

# ======= S VARIABLE ========
for i in n_N.keys():
    alpha_S[i] = [ [1.0, ] * n_S[i] for j in range(n_R) ]
    theta_S[i] = mc.Lambda('theta_S%i'%i, lambda alpha_S=alpha_S[i], R=R: alpha_S[R])
    cpt_S[i] = mc.Dirichlet(name='cpt_S%i'%i, theta = theta_S[i])
    S[i] = mc.Categorical('S%i'%i, cpt_S[i], value=0)

# ======= D VARIABLE ========
for i in n_N.keys():
    alpha_D[i] = [ [1.0, ] * m for _ in range(n_S[i]) ]
    theta_D[i] = mc.Lambda('theta_D%i'%i, lambda alpha_D=alpha_D[i], S=S[i]: alpha_D[S])
    cpt_D[i] = mc.Dirichlet(name='cpt_D%i'%i, theta = theta_D[i])
    D[i] = mc.Categorical('D%i'%i, cpt_D[i], observed=True, value=D_data[i])
    predD[i] = mc.Categorical('predD%i'%i, cpt_D[i])

# ======= C VARIABLE ========
for i in n_N.keys():
    taus_C[i] = [np.eye(f_len) for _ in range(n_S[i])]
    n_mus_C[i] = [mc.Uniform('mC%i%i'%(i,j), lower=C_min, upper=C_max) for j in range(n_S[i])]
    n_tau_C[i] = [mc.Uniform('tC%i%i'%(i,j), lower=0.0001, upper=1.0) for j in range(n_S[i])]
    
    t_t[i] = mc.Lambda('t_t%i'%i, lambda taus_C = taus_C[i], S=S[i]: taus_C[S])
    m_n[i] = mc.Lambda('m_n%i'%i, lambda n_mus_C = n_mus_C[i], S=S[i]: n_mus_C[S])
    t_n[i] = mc.Lambda('t_n%i'%i, lambda n_tau_C = n_tau_C[i], S=S[i]: n_tau_C[S])

    tau[i] = mc.Wishart('tau%i'%i, n=f_len + 1, Tau=t_t[i])
    mu[i] = mc.Normal('mu%i'%i, mu=m_n[i], tau=t_n[i], size=f_len)
    
    C[i] = mc.MvNormalCov('C%i'%i, mu=mu[i], C=tau[i], observed=True, value=C_data[i]) 
    predC[i] = mc.MvNormalCov('predC%i'%i, mu=mu[i], C=tau[i])

A = mc.MCMC(locals())
dag(A, format="png", name="testasdasd", path="graph/")