'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot

n_R = 2
m = 35
f_len = 33

n_segments = 4
n_N = {1:0, 2:0 ,3:0 ,4:0}
n_S = {1:2, 2:2 ,3:2 ,4:2}

N_data = {}
D_data = {}
C_data = {}
for seg in n_N.keys():
    N_data[seg] = None
    D_data[seg] = None
    C_data[seg] = None

base_path = '../res/tele-aliens/data/'
N_max = np.load(base_path + 'n2N_max.npy')
for v in N_max:
    n_N[v[0]] = v[1] + 1
    
for seg in n_N.keys():
    N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
    D_data[seg] = np.load(base_path + 'n2D' + str(seg) + '.npy')
    C_data[seg] = np.load(base_path + 'n2C' + str(seg) + '.npy')


C_max = np.max( np.load(base_path + 'n2C_max.npy') )
C_min = np.min( np.load(base_path + 'n2C_min.npy') )

alpha_N = {}
theta_N = {}
dir_N = {}
cpt_N = {}
N = {}
predN = {}

alpha_S = {}
theta_S = {}
cpt_S = {}
dir_S = {}
S = {}

alpha_D = {}
theta_D = {}
cpt_D = {}
dir_D = {}
D = {}
predD = {}

taus_C = {}
n_mus_C = {}
n_tau_C = {}

tau_Wish = {}
mu_Norm = {}
tau_Norm = {}

tau = {}
mu = {}
C = {}
predC = {}

init_R = 0.1
init_N = 0.1
init_D = 0.1
init_S = 0.1

a_N = {}
v_N = {}

# ======= R VARIABLE ========
#cpt_R = mc.Dirichlet(name='cpt_R', theta = [init_R, ] * n_R)
cpt_R = mc.CompletedDirichlet(name='cpt_R', D = [init_R, ] * (n_R - 1), doc="cpt_R")

#a_R = mc.Uniform('a_R', lower=0.5, upper=10)
#v_R = mc.Beta('v_R', alpha=1, beta=a_R, size=n_R)
#@mc.deterministic
#def cpt_R(v_R = v_R):
#    """ Calculate Dirichlet probabilities """
#    # Probabilities from betas
#    value = [ u * np.prod(1 - v_R[:i]) for i, u in enumerate(v_R)]
#    # Enforce sum to unity constraint
#    value[-1] = 1 - sum(value[:-1])
#    return value

R = mc.Categorical('R', p=cpt_R, value = 0)


# ======= N VARIABLE ========
for i in n_N.keys():
    #theta_N[i] = [ [init_N, ] * (n_N[i]) for j in range(n_R) ]
    #dir_N[i] = [ mc.Dirichlet( name = 'dir_N%i_R%i' % (i, j), theta = theta_N[i][j] ) for j in range(n_R) ]
    
    theta_N[i] = [ mc.Exponential(name = 'theta_N%i_R%i' % (i, j), beta = 0.1, size = n_N[i] + 1) for j in range(n_R) ]
    dir_N[i] = [ mc.Dirichlet( name = 'dir_N%i_R%i' % (i, j), theta = theta_N[i][j] ) for j in range(n_R) ]
    
    #dir_N[i] = [ mc.CompletedDirichlet( name = 'dir_N%i_R%i' % (i, j), D = theta_N[i][j] , doc='dir_N%i_R%i' % (i, j)) for j in range(n_R) ]
    
    #cpt_N[i] = mc.Lambda('cpt_N%i' % i, lambda dir_N = dir_N[i], R = R: dir_N[R] )
    cpt_N[i] = mc.Lambda('cpt_N%i' % i, lambda dir_N = dir_N[i], R = R: dir_N[R] )
    N[i] = mc.Categorical('N%i' % i, p = cpt_N[i], observed = True, value = N_data[i])
    predN[i] = mc.Categorical('predN%i' % i, cpt_N[i], value = 0)

# ======= S VARIABLE ========
for i in n_N.keys():
    theta_S[i] = [ [init_S, ] * (n_S[i] - 1) for j in range(n_R) ]
    #dir_S[i] = [ mc.Dirichlet(name='dir_S%i_R%i' % (i, j), theta = theta_S[i][j] ) for j in range(n_R) ]
    dir_S[i] = [ mc.CompletedDirichlet( name = 'dir_S%i_R%i' % (i, j), D = theta_S[i][j] , doc='dir_S%i_R%i' % (i, j)) for j in range(n_R) ]
    #cpt_S[i] = mc.Lambda('cpt_S%i' % i, lambda dir_S = dir_S[i], R = R: dir_S[R] )
    cpt_S[i] = mc.Lambda('cpt_S%i' % i, lambda dir_S = dir_S[i], R = R: dir_S[R] )
    S[i] = mc.Categorical('S%i' % i, cpt_S[i], value = 0)

# ======= D VARIABLE ========
for i in n_N.keys():
    theta_D[i] = [ [init_D, ] * (m) for j in range(n_S[i]) ]
    #dir_D[i] = [ mc.Dirichlet(name='dir_D%i_S%i' % (i, j), theta = theta_D[i][j] ) for j in range(n_S[i]) ]
    dir_D[i] = [ mc.CompletedDirichlet( name = 'dir_D%i_S%i' % (i, j), D = theta_D[i][j] , doc='dir_D%i_S%i' % (i, j)) for j in range(n_S[i]) ]
    #cpt_D[i] = mc.Lambda('cpt_D%i' % i, lambda dir_D = dir_D[i], S = S[i]: dir_D[S] )
    cpt_D[i] = mc.Lambda('cpt_D%i' % i, lambda  dir_D = dir_D[i], S = S[i]: dir_D[S] )
    D[i] = mc.Categorical('D%i' % i, cpt_D[i], observed = True, value = D_data[i])
    predD[i] = mc.Categorical('predD%i' % i, cpt_D[i], value = 0)

# ======= C VARIABLE ========
for i in n_N.keys():
    taus_C[i] = [np.eye(f_len) for _ in range(n_S[i])]
    n_mus_C[i] = [mc.Uniform('mC%i_S%i' % (i, j), lower=C_min, upper=C_max) for j in range(n_S[i])]
    n_tau_C[i] = [mc.Uniform('tC%i_S%i' % (i, j), lower=0.0001, upper=2.0) for j in range(n_S[i])]
    
#    t_t[i] = mc.Lambda('t_t%i'%i, lambda taus_C = taus_C[i], S=S[i]: taus_C[S])
#    m_n[i] = mc.Lambda('m_n%i'%i, lambda n_mus_C = n_mus_C[i], S=S[i]: n_mus_C[S])
#    t_n[i] = mc.Lambda('t_n%i'%i, lambda n_tau_C = n_tau_C[i], S=S[i]: n_tau_C[S])

    tau_Wish[i] = mc.Lambda('tau_Wish%i' % i, lambda taus_C = taus_C[i], S = S[i]: taus_C[S])
    mu_Norm[i] = mc.Lambda('mu_Norm%i' % i, lambda n_mus_C = n_mus_C[i], S = S[i]: n_mus_C[S])
    tau_Norm[i] = mc.Lambda('tau_Norm%i' % i, lambda n_tau_C = n_tau_C[i], S = S[i]: n_tau_C[S])
   
    tau[i] = mc.Wishart('tau%i' % i, n = (f_len + 1), Tau = tau_Wish[i])
    mu[i] = mc.Normal('mu%i' % i, mu = mu_Norm[i], tau = tau_Norm[i], size = f_len)
    
    C[i] = mc.MvNormalCov('C%i' % i, mu=mu[i], C=tau[i], observed=True, value=C_data[i]) 
    predC[i] = mc.MvNormalCov('predC%i' % i, mu=mu[i], C=tau[i])

#A = mc.MCMC(locals())
#dag(A, format="png", name="test_arch_dir", path="graph/")