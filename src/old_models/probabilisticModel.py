'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import pylab as pl

#theta = np.asarray([0.5, 0.5])
#cpt_R = mc.Dirichlet("cpt_R", theta) 
#R = mc.Categorical('R', p=cpt_R)
#
#theta_n = np.asarray([0.5, 0.5])
#cpt_N = mc.Dirichlet("cpt_R", theta_n)
#print theta_n + R



pi = [0.3,0.4,0.2,0.1]
X = mc.Categorical('X', p=pi)

mu =  np.array([1,2,3,4])
tau = np.array([1,2,1,2])

tau_i = mc.Lambda('tau_i', lambda X=X: tau[X])
mu_i = mc.Lambda('mu_i', lambda X=X: mu[X])

W = mc.Normal('W', mu=mu_i, tau=tau_i, value=mydata, observed=True) 



theta = np.asarray([0.5, 0.5])
R = mc.Dirichlet("R", theta)
X = mc.Categorical('X', p=R)








G_obs = [1.]
N = len(G_obs)

theta = np.asarray([0.5, 0.5])
R = mc.Dirichlet("R", theta)

N_obs = [ 0, 0, 1, 2, 3, 2, 1, 1, 0 ]
N_mod = []
for x in N_obs:
    N_mod.append([x, R])

print N_mod

theta_s = np.asarray([0.01,] * (len(N_obs) -1))
#S = mc.Dirichlet("S", theta=theta_s, value=N_mod, observed=True)
S2 = mc.dirichlet_like(x=N_mod, theta=theta_s)

#p_S = mc.Lambda('p_S', lambda R=R: pl.where(R, .01, .4),
#                doc='Pr[S|R]')
#S = mc.Bernoulli('S', p_S, value=pl.ones(N))

#p_G = mc.Lambda('p_G', lambda S=S, R=R:
#                pl.where(S, pl.where(R, .99, .9), pl.where(R, .8, 0.)),
#                doc='Pr[G|S,R]')
#G = mc.Bernoulli('G', p_G, value=G_obs, observed=True)

print "Model completed"