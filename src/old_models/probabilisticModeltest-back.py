'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot

n_R = 2
n_N = 4
n_S = 3
m = 10
f_len = 20

fake_D_data = [random.randint(0, m-1) for _ in range(500)]
fake_C_data = [[random.normalvariate(mu=0.0, sigma=30.0) for _ in range(f_len)] for _ in range(500)]
fake_N_data = [random.randint(0, n_N - 1) for _ in range(500)]

print "Initial data: " + str(fake_N_data)
counts = [0, ] * n_N
for x in fake_N_data:
    counts[x] += 1
print "Counts: " + str(counts)

#Suppose R = {0,1}
#alpha_R = mc.Poisson('alpha_R', mu = [1.0, ] * n_R)
alpha_R = mc.Uniform('alpha_R', lower= 1.0, upper=100.0, size=n_R)
cpt_R = mc.CompletedDirichlet( name='cpt_R', D = mc.Dirichlet(name='d_cpt_R', theta = alpha_R) )
#cpt_R = mc.Dirichlet(name='cpt_R', theta = alpha_R)
R = mc.Categorical('R', p=cpt_R, value=0)

#Suppose N = {0,1,2,3}
#alpha_N = [mc.Poisson('alpha_N_%i' % j, mu = [1.0, ] * n_N) for j in xrange(n_R)]
alpha_N = [mc.Uniform('alpha_N_%i' % j, lower= 1.0, upper=100.0, size=n_N) for j in xrange(n_R)]
etaN = [mc.CompletedDirichlet( name='etaN_%i' % j, D = mc.Dirichlet(name='d_etaN_%i' % j, theta = alpha_N[j]) ) for j in xrange(n_R)]
#etaN = [ mc.Dirichlet(name='etaN_%i' % j, theta = alpha_N[j]) for j in xrange(n_R)]
def n_select(etaN = etaN, R=R):
    #print "R value: " + str(R)
    return etaN[R]
cpt_N = mc.Lambda('cpt_N',n_select)
N = mc.Categorical('N', cpt_N, observed=True, value=fake_N_data)

#Suppose S = {0,1}
alpha_S = [mc.Uniform('alpha_S_%i' % j, lower= 1.0, upper=100.0, size=n_S) for j in xrange(n_R)]
etaS = [mc.CompletedDirichlet( name='etaS_%i' % j, D = mc.Dirichlet(name='d_etaS_%i' % j, theta = alpha_S[j]) ) for j in xrange(n_R)]
#etaS = [ mc.Dirichlet(name='etaS_%i' % j, theta = alpha_S[j]) for j in xrange(n_R)]
def s_select(etaS = etaS, R=R):
    return etaS[R]
cpt_S = mc.Lambda('cpt_S', s_select)
S = mc.Categorical('S', cpt_S, value=0)

# TODO: MAP DATA TO CATEGORIES
# ALTERNATIVE: multinomial with n = component_number ^ 2
#fake_m_data = [ [1,1,0], [1,0,2], [1,0,0], [2,1,0], [2,0,2], [1,1,1], [1,1,0], [1,1,2], [1,0,2], [2,1,1], [0,1,2], [1,0,0], [1,1,0], [1,0,2], [1,0,0] ]
alpha_D = [mc.Uniform('alpha_D_%i' % j, lower= 1.0, upper=100.0, size=m+1) for j in xrange(n_S)]
#etaD = [mc.CompletedDirichlet( name='etaD_%i' % j, D = mc.Dirichlet(name='d_etaD_%i' % j, theta = alpha_D[j]) ) for j in xrange(n_S)]
etaD = [mc.Dirichlet(name='etaD_%i' % j, theta = alpha_D[j]) for j in xrange(n_S)]
def d_select(etaD = etaD, S=S):
    return etaD[S]
cpt_D = mc.Lambda('cpt_D', d_select)
D = mc.Categorical('D', cpt_D, observed=True, value=fake_D_data)


tau_C = [mc.WishartCov('tau%i'%i, n=f_len, C=np.eye(f_len)) for i in range(n_S)]
mu_C =  [mc.Normal('mu%i'%i, mu=0, tau=1e-5, size=f_len) for i in range(n_S)]

def tau_C_select(tau_C=tau_C, S=S):
    return tau_C[S]
def mu_C_select(mu_C=mu_C, S=S):
    return mu_C[S]

tau = mc.Lambda('tau', tau_C_select)
mu = mc.Lambda('mu', mu_C_select)

C = mc.MvNormalCov('C', mu, tau, observed=True, value=fake_C_data) 

A = mc.MCMC(locals())
dag(A, format="png", name="test", path="graph/")