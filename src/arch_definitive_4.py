'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot
import os
  
def getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ):
    n_R = input_R
    m = 35
    f_len = 33
    
    n_segments = 4
    n_N = {1:0, 2:0 ,3:0 ,4:0}
    n_S = input_S
    N_data = {}
    D_data = {}
    C_data = {}
    
    for seg in n_N.keys():
        N_data[seg] = None
        D_data[seg] = None
        C_data[seg] = None
    
    base_path = '../res/tele-aliens/data/'
    N_max = np.load(base_path + 'n2N_max.npy')
    for v in N_max:
        n_N[v[0]] = v[1] + 1
        
    for seg in n_N.keys():
        N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
        D_data[seg] = np.load(base_path + 'n2D' + str(seg) + '.npy')
        C_data[seg] = np.load(base_path + 'n2C' + str(seg) + '.npy')
    
    
    C_max = np.max( np.load(base_path + 'n2C_max.npy') )
    C_min = np.min( np.load(base_path + 'n2C_min.npy') )
    
    alpha_N = {}
    theta_N = {}
    cpt_N_R = {}
    cpt_N = {}
    N = {}
    predN = {}
    
    alpha_S = {}
    theta_S = {}
    cpt_S = {}
    cpt_S_R = {}
    S = {}
    
    alpha_D = {}
    theta_D = {}
    cpt_D = {}
    cpt_D_S = {}
    D = {}
    predD = {}
    
    taus_C = {}
    n_mus_C = {}
    n_tau_C = {}
    t_t = {}
    m_n = {}
    t_n = {}
    tau = {}
    mu = {}
    C = {}
    predC = {}
    
    # ======= R VARIABLE ========
    #cpt_R = mc.Dirichlet(name='cpt_R', theta = [0.1, ] * (n_R + 1), value = [ 1.0 / (n_R), ] * (n_R))
    cpt_R = mc.Dirichlet(name='cpt_R', theta = [2.0, ] * (n_R))
    #cpt_R = mc.CompletedDirichlet(name = 'cpt_R', D = [ 0.1, ] * (n_R - 1), doc = 'cpt_R')
    R = mc.Categorical('R', p = cpt_R)
    
    print n_N
    print
    # ======= N VARIABLE ========
    for i in n_N.keys():
        cpt_N_R_i = [ mc.Dirichlet(name = 'cpt_N%i_R%i' % (i, j), 
                                   theta = [1.0, ] * (n_N[i] + 1), 
                                   value = [1.0 / (n_N[i]) , ] * (n_N[i]) 
                                   ) 
                     for j in range(n_R + 1)]
                
        @mc.deterministic(name = 'cpt_N%i'%i)
        def cpt_N_i(cpts = cpt_N_R_i, R = R):
            return cpts[R]
        
        N[i] = mc.Categorical('N%i'%i, cpt_N_i, observed = True, value = N_data[i])
        predN[i] = mc.Categorical('predN%i'%i, cpt_N_i)
    
    # ======= S VARIABLE ========
    for i in n_N.keys():
        cpt_S_R_i = [ mc.Dirichlet(
                                   name = 'cpt_S%i_R%i' % (i, j), 
                                   theta = [2.0, ] * (n_S[i]), 
                                   value = [1.0 / (n_S[i]) , ] * (n_S[i] - 1) 
                                   ) 
                     for j in range(n_R + 1)]
        
        @mc.deterministic(name = 'cpt_S%i'%i)
        def cpt_S_i(cpts = cpt_S_R_i, R = R):
            return cpts[R]
        
        S[i] = mc.Categorical('S%i'%i, cpt_S_i, value = 0)
    
    d_arr = [1.0 / (m + 2) , ] * (m - 1)
    d_arr.append( 1.0 - np.sum(d_arr) )
    
    # ======= D VARIABLE ========
    for i in n_N.keys():
        cpt_D_S_i = [ mc.Dirichlet(name = 'cpt_D%i_S%i' % (i, j), 
                                   theta = [1.0, ] * (m + 1), 
                                   value = d_arr 
                                   ) for j in range(n_S[i])]
        @mc.deterministic(name = 'cpt_D%i'%i)
        def cpt_D_i(cpts = cpt_D_S_i, S = S[i]):
            return cpts[S]
        
        D[i] = mc.Categorical('D%i'%i, cpt_D_i, observed=True, value=D_data[i])
        predD[i] = mc.Categorical('predD%i'%i, cpt_D_i)
    
    # ======= C VARIABLE ========
#    for i in n_N.keys():
#        taus_C[i] = [np.eye(f_len) for _ in range(n_S[i])]
#        n_mus_C[i] = [mc.Uniform('mC%i%i'%(i,j), lower=C_min, upper=C_max) for j in range(n_S[i])]
#        n_tau_C[i] = [mc.Uniform('tC%i%i'%(i,j), lower=0.0001, upper=1.0) for j in range(n_S[i])]
#        
#        t_t[i] = mc.Lambda('t_t%i'%i, lambda taus_C = taus_C[i], S=S[i]: taus_C[S])
#        m_n[i] = mc.Lambda('m_n%i'%i, lambda n_mus_C = n_mus_C[i], S=S[i]: n_mus_C[S])
#        t_n[i] = mc.Lambda('t_n%i'%i, lambda n_tau_C = n_tau_C[i], S=S[i]: n_tau_C[S])
#        
#        tau[i] = mc.Wishart('tau%i'%i, n=f_len + 1, Tau=t_t[i])
#        mu[i] = mc.Normal('mu%i'%i, mu=m_n[i], tau=t_n[i], size=f_len)
#        
#        C[i] = mc.MvNormalCov('C%i'%i, mu=mu[i], C=tau[i], observed=True, value=C_data[i]) 
#        predC[i] = mc.MvNormalCov('predC%i'%i, mu=mu[i], C=tau[i])
    
    #print locals()
    return locals()
    
if __name__ == "__main__":
    A = mc.Model(getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ))
    dag(A, format="png", name="arch_def_3", path="graph/")
#    
    mod = mc.MAP(getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ))
    mod.fit( iterlim = 3000 )
    print "AIC: " + str(mod.AIC)
    print "BIC: " + str(mod.BIC)
    print
    
    mcmcSampler = mc.MCMC( mod.variables , db='pickle', dbname = 'prova_beta.pickle')
    #mcmcSampler = mc.MCMC( getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ) , db='pickle', dbname = 'prova_beta.pickle')
    mcmcSampler.sample(iter=20000, burn=10000, thin=10)
    #mcmcSampler.sample(iter=5000)
    mcmcSampler.db.close()
    
    directory = 'graph/prova_beta'
    if( not os.path.isdir(directory) ):
        os.mkdir(directory)
    
    plot(mcmcSampler, path=directory)
    
    