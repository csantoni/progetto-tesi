'''
Created on 21/mag/2013

@author: Christian
'''

import pymc as mc
import numpy as np
from pymc.graph import dag
from pymc.Matplot import plot
import os
import winsound

base_path = '../res/tele-aliens/data/'
n_R = 2
n_S = {1:2, 2:2 ,3:2 ,4:2}
n_N = {1:0, 2:0 ,3:0 ,4:0}
N_data = {}
C_data = {}
D_data = {}

D_feat_num = 35
C_feat_len = 14


for seg in n_N.keys():
    N_data[seg] = None
N_max = np.load(base_path + 'n2N_max.npy')
for v in N_max:
    n_N[v[0]] = v[1] + 1

print n_N

for seg in n_N.keys():
    N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
    D_data[seg] = np.load(base_path + 'n2D' + str(seg) + '.npy')
    C_data[seg] = np.load(base_path + '70n2C' + str(seg) + '.npy')

for arr in C_data[4]:
    s = ""
    for val in arr:
        s += "%.10f," % (val) 
    s += "" 
    print s
    print

print "Sample C2: " + str(len(C_data[4]))

exit(0)

p_R = mc.Dirichlet( name ='p_R', theta = [1.0, ] * n_R, trace = True, plot = True)
R = mc.Categorical('R', p = p_R)
R = mc.DiscreteUniform('R', lower = 0, upper = n_R - 1)

p_N1 = [ mc.Dirichlet( name ='p_N1_R%i' % j, theta = [1.0,] * (n_N[1] + 1) , trace = True, plot = True) for j in range(n_R) ]

@mc.observed
@mc.stochastic(dtype=int)
def N1(value = N_data[1], R = R, p_N1 = p_N1):
    def logp(value, R, p_N1):
        return mc.categorical_like(value, np.append(p_N1[R], ( 1.0 - np.sum(p_N1[R]) )))
    def random(R, p_N1):
        return mc.rcategorical(np.append(p_N1[R], ( 1.0 - np.sum(p_N1[R]) )))

p_N2 = [ mc.Dirichlet( name ='p_N2_R%i' % j, theta = [1.0,] * (n_N[2] + 1) , trace = True, plot = True) for j in range(n_R) ]

@mc.observed
@mc.stochastic(dtype=int)
def N2(value = N_data[2], R = R, p_N2 = p_N2):
    def logp(value, R, p_N2):
        return mc.categorical_like(value, np.append(p_N2[R], ( 1.0 - np.sum(p_N2[R]) )))
    def random(R, p_N2):
        return mc.rcategorical(np.append(p_N2[R], ( 1.0 - np.sum(p_N2[R]) )))
    
p_N3 = [ mc.Dirichlet( name ='p_N3_R%i' % j, theta = [1.0,] * (n_N[3] + 1) , trace = True, plot = True) for j in range(n_R) ]

@mc.observed
@mc.stochastic(dtype=int)
def N3(value = N_data[3], R = R, p_N3 = p_N3):
    def logp(value, R, p_N3):
        return mc.categorical_like(value, np.append(p_N3[R], ( 1.0 - np.sum(p_N3[R]) )))
    def random(R, p_N3):
        return mc.rcategorical(np.append(p_N3[R], ( 1.0 - np.sum(p_N3[R]) )))
    
p_N4 = [ mc.Dirichlet( name ='p_N4_R%i' % j, theta = [1.0,] * (n_N[4] + 1) , trace = True, plot = True) for j in range(n_R) ]

@mc.observed
@mc.stochastic(dtype=int)
def N4(value = N_data[4], R = R, p_N4 = p_N4):
    def logp(value, R, p_N4):
        return mc.categorical_like(value, np.append(p_N4[R], ( 1.0 - np.sum(p_N4[R]) )))
    def random(R, p_N4):
        return mc.rcategorical(np.append(p_N4[R], ( 1.0 - np.sum(p_N4[R]) )))

#p_S1 = [ mc.Dirichlet( name ='p_S1_R%i' % j, theta = [1.0,] * (n_S[1]) , trace = True, plot = True) for j in range(n_R) ]
# 
#@mc.stochastic(dtype=int)
#def S1(value = 0, R = R, p_S1 = p_S1):
#    def logp(value, R, p_S1):
#        return mc.categorical_like(value, np.append(p_S1[R], ( 1.0 - np.sum(p_S1[R]) )))
#    def random(R, p_S1):
#        return mc.rcategorical(np.append(p_S1[R], ( 1.0 - np.sum(p_S1[R]) )))
#
#p_D1 = [ mc.Dirichlet( name ='p_D1_S%i' % j, theta = [1.0,] * D_feat_num , trace = True, plot = False) for j in range(n_S[1]) ]
#
#@mc.observed
#@mc.stochastic(dtype=int)
#def D1(value = D_data[1], S1 = S1, p_D1 = p_D1):
#    def logp(value, S1, p_D1):
#        return mc.categorical_like(value, np.append(p_D1[S1], ( 1.0 - np.sum(p_D1[S1]) )))
#    def random(S1, p_D1):
#        return mc.rcategorical(np.append(p_D1[S1], ( 1.0 - np.sum(p_D1[S1]) )))
#
## ======= C VARIABLE ========
#tauC1_list = [ mc.Wishart('tauC1_S%i' % j, n = C_feat_len + 1, Tau = np.eye(C_feat_len) , trace = True, plot = False) for j in range(n_S[1])]
#muC1_list  = [ mc.Normal('muC1_S%i' % j, mu = 1.0, tau = 0.1, size = C_feat_len , trace = True, plot = False) for j in range(n_S[1])]
#
#muC1 = mc.Lambda('muC1' , lambda muC1_list = muC1_list, S1 = S1: muC1_list[S1], trace = True, plot = False)
#tauC1 = mc.Lambda('tauC1', lambda tauC1_list = tauC1_list, S1 = S1: tauC1_list[S1], trace = True, plot = False)
#
#C1 = mc.MvNormalCov('C1', mu = muC1, C = tauC1, observed = True, value = C_data[1])
 
#predC1_S0 = mc.MvNormalCov('predC1_S0', mu=muC1_list[0], C=tauC1_list[0], trace = True, plot = True)
#predC1_S1 = mc.MvNormalCov('predC1_S1', mu=muC1_list[1], C=tauC1_list[1], trace = True, plot = True)

A = locals()

sampler = mc.MCMC(A)
sampler.sample(iter = 10000)
mod = mc.Model(A)
dag(mod, format="png", name="arch_toy_2", path="graph/")
    
directory = 'graph/prova_toy_2'
if( not os.path.isdir(directory) ):
    os.mkdir(directory)

plot(sampler, path=directory)

print "done"
Freq = 2400 # Set Frequency To 2500 Hertz
Dur = 400 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)
winsound.Beep(Freq,Dur)