'''
Created on 30/apr/2013

@author: Christian
'''

import arch_definitive
import arch_definitive_2

from pymc import MCMC, MAP
from pymc.graph import dag
from pymc.Matplot import plot
import pymc
import numpy
import winsound
import time
import os
import pylab

configurations = {
                  0 : { 'R':2, 'S': [2,2,2,2] , 'basename': 'arch_conf_'},
                  1 : { 'R':2, 'S': [3,2,2,2] , 'basename': 'arch_conf_'},
                  2 : { 'R':2, 'S': [3,3,2,2] , 'basename': 'arch_conf_'},
                  3 : { 'R':2, 'S': [3,3,3,2] , 'basename': 'arch_conf_'},
                  4 : { 'R':2, 'S': [3,3,3,3] , 'basename': 'arch_conf_'},
                  5 : { 'R':3, 'S': [2,2,2,2] , 'basename': 'arch_conf_'},
                  6 : { 'R':3, 'S': [3,2,2,2] , 'basename': 'arch_conf_'},
                  7 : { 'R':3, 'S': [3,3,2,2] , 'basename': 'arch_conf_'},
                  8 : { 'R':3, 'S': [3,3,3,2] , 'basename': 'arch_conf_'},
                  9 : { 'R':3, 'S': [3,3,3,3] , 'basename': 'arch_conf_'},
                  10: { 'R':2, 'S': [2,3,2,2] , 'basename': 'arch_conf_'},
                  11: { 'R':2, 'S': [2,2,3,2] , 'basename': 'arch_conf_'},
                  12: { 'R':2, 'S': [2,2,2,3] , 'basename': 'arch_conf_'},
                  13: { 'R':3, 'S': [2,3,2,2] , 'basename': 'arch_conf_'},
                  14: { 'R':3, 'S': [2,2,3,2] , 'basename': 'arch_conf_'},
                  15: { 'R':3, 'S': [2,2,2,3] , 'basename': 'arch_conf_'},
                  16: { 'R':3, 'S': [3,3,2,3] , 'basename': 'arch_conf_'},
                  17: { 'R':3, 'S': [2,2,2,4] , 'basename': 'arch_conf_'},
                  }

for key in [0,1]:
    input_S = {1:0, 2:0, 3:0, 4:0}
    for x in range(len(configurations[key]['S'])):
        input_S[x+1] = configurations[key]['S'][x]
    
    signature = str(configurations[key]['R']) + '_' 
    for x in range(len(configurations[key]['S'])):
        signature += str(configurations[key]['S'][x])
    #model_name = configurations[key]['basename'] + signature
    model_name = "v2_" + configurations[key]['basename'] + signature
     
    print "Sampling model: " + model_name
    
    db_loaded = pymc.database.pickle.load( model_name + '.pickle' )
    mcmcSampler = MCMC( arch_definitive_2.getModel( configurations[key]['R'] , input_S) , db = db_loaded)
        
    directory = 'graph/' + model_name
#    if( not os.path.isdir(directory) ):
#        os.mkdir(directory)
    
    #dag(mcmcSampler, format = "png", name = "graph", path = directory)
    plot(mcmcSampler, path=directory)

    print "Plotting finished"
    print
    
    mcmcSampler = None

print "done"
Freq = 2400 # Set Frequency To 2500 Hertz
Dur = 400 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)
winsound.Beep(Freq,Dur)