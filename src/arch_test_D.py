'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot
import os
import winsound

def getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ):
    n_R = input_R
    m = 35
    f_len = 33
    
    n_segments = 4
    n_N = {1:0, 2:0 ,3:0 ,4:0}
    n_S = input_S
    N_data = {}
    D_data = {}
    C_data = {}
    
    for seg in n_N.keys():
        N_data[seg] = None
        D_data[seg] = None
        C_data[seg] = None
    
    base_path = '../res/tele-aliens/data/'
    N_max = np.load(base_path + 'n2N_max.npy')
    for v in N_max:
        n_N[v[0]] = v[1] + 1
        
    for seg in n_N.keys():
        N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
        D_data[seg] = np.load(base_path + 'n2D' + str(seg) + '.npy')
        print D_data[seg]
        print 
        C_data[seg] = np.load(base_path + 'n2C' + str(seg) + '.npy')
    
    
    C_max = np.max( np.load(base_path + 'n2C_max.npy') )
    C_min = np.min( np.load(base_path + 'n2C_min.npy') )
    
    alpha_N = [None, ] * len(n_N.keys())
    theta_N = [None, ] * len(n_N.keys())
    cpt_N = [None, ] * len(n_N.keys())
    N = [None, ] * len(n_N.keys())
    
    alpha_S = [None, ] * len(n_N.keys())
    theta_S = [None, ] * len(n_N.keys())
    cpt_S = [None, ] * len(n_N.keys())
    S = [None, ] * len(n_N.keys())
    
    alpha_D = [None, ] * len(n_N.keys())
    theta_D = [None, ] * len(n_N.keys())
    cpt_D = [None, ] * len(n_N.keys())
    D = [None, ] * len(n_N.keys())
    
    taus_C = {}
    n_mus_C = {}
    n_tau_C = {}
    t_t = {}
    m_n = {}
    t_n = {}
    
    tau = [None, ] * len(n_N.keys())
    mu = [None, ] * len(n_N.keys())
    taus = [None, ] * len(n_N.keys())
    mus = [None, ] * len(n_N.keys())
    C = [None, ] * len(n_N.keys())
    
    
    predN = {}
    predD = {}
    predC = {}
    
    dirichs_D = {}
    categs_D = {}
    pred_categs_D = {}
    for seg in n_N.keys():
        dirichs_D[seg] = []
        categs_D[seg] = []
        pred_categs_D[seg] = []
        for i in range(n_S[seg]):
            dirichs_D[seg].append(mc.Dirichlet(name='cpt_D%i_S%i' % (seg, i), theta = [2.0 ] * m , plot = False))
            categs_D[seg].append( mc.Categorical( name='D%i_S%i' % (seg, i), p = dirichs_D[seg][i], observed = True, value = D_data[seg]) )
            pred_categs_D[seg].append( mc.Categorical( name='predD%i_S%i' % (seg, i), p = dirichs_D[seg][i]) )
            
    return locals()
    
if __name__ == "__main__":
    locals = getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} )
    
    A = mc.Model(locals)
    dag(A, format="png", name="test_D", path="graph/")
    
    mcmcSampler = mc.MCMC(locals)
    mcmcSampler.sample(iter = 5000)
    
    print mcmcSampler.step_method_dict
    print
    
    directory = 'graph/test_D'
    if( not os.path.isdir(directory) ):
        os.mkdir(directory)
    
    plot(mcmcSampler, path=directory)
    #plot(mcmcSampler.trace("R"), path=directory)
#    plot(mcmcSampler.trace("Ntry"), path=directory)
    
    print "done"
    Freq = 2400 # Set Frequency To 2500 Hertz
    Dur = 400 # Set Duration To 1000 ms == 1 second
    winsound.Beep(Freq,Dur)
    winsound.Beep(Freq,Dur)