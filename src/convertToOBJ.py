'''
Created on 10/apr/2013

@author: Christian
'''

import subprocess
from os import walk
import numpy

program = '../res/meshconv.exe'
main_path = 'C:\\Users\\Christian\\workspace\\progetto-tesi-new\\'

def convertMeshes(directory):
    #Check dei sdf files
    f = []
    for (dirpath, dirname, filenames) in walk(directory):
        f.extend(filenames)
        break
    
    l = open(directory + 'list.txt', 'wb')
    for file in f:
        filename_parts = file.split('.') 
        if len(filename_parts) == 3:
            if filename_parts[-2] == 'sdf' and filename_parts[-1] == 'off':
                print directory + file
                win_directory = '\\'.join(directory.split('/')[1:])
                print main_path + win_directory + file
                print
                
                arguments = (main_path + win_directory + file, '-c', 'obj')
                subprocess.call((program, ) + arguments )
                
                l.write('.'.join(filename_parts[:-1]) + '\n')
                
    l.close()
    
if __name__ == "__main__":
    basePath = "../res/tele-aliens/data/"
    model_list = [x for x in range(126, 201)]
    for model in model_list:
        convertMeshes(basePath + str(model) + "/")
    print "done!"