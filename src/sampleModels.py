'''
Created on 30/apr/2013

@author: Christian
'''

import arch_definitive
import arch_definitive_2

from pymc import MCMC, MAP
from pymc.graph import dag
from pymc.Matplot import plot
import pymc
import numpy
import winsound
import time
import os
import pylab
#from pylab import hist, show

def get_logp(model, db):
    n_samples = db.trace('deviance').length()
    logp = numpy.empty(n_samples, numpy.double)
    #loop over all samples
    print model.stochastics
    for i_sample in xrange(n_samples):
        #set the value of all stochastic to their 'i_sample' value
        for stochastic in model.stochastics:
            try:
                value = db.trace(stochastic.__name__)[i_sample]
                stochastic.value = value
            except KeyError:
                print "No trace available for %s. " % stochastic.__name__
        #get logp
        logp[i_sample] = model.logp
    return logp

def get_divided_logp(model, db, N_data, D_data, C_data, path):
    
    N_traced = [ 'predN1', 'predN2', 'predN3', 'predN4' ]
    D_traced = [ 'predD1', 'predD2', 'predD3', 'predD4' ]
    C_traced = [ 'predC1', 'predC2', 'predC3', 'predC4' ]
    
    n_samples = db.trace('deviance').length()
    logp = [None, ] * ( len(N_traced) + len(D_traced) + len(C_traced) )
    for i in range(len(logp)):
        logp[i] = numpy.empty(n_samples, numpy.double)
        
    #loop over all samples
    print model.stochastics
    for i_sample in xrange(n_samples):
        print "Sample #" + str(i_sample)
        #set the value of all stochastic to their 'i_sample' value
        for stochastic in model.stochastics:
            try:
                value = db.trace(stochastic.__name__)[i_sample]
                stochastic.value = value
            except KeyError:
                print "No trace available for %s. " % stochastic.__name__
        #get logp
        for idx, n_stoch in enumerate(N_traced):
            for stochastic in model.stochastics:
                #print "Comparing " + stochastic.__name__ + " and " + n_stoch 
                if stochastic.__name__ == n_stoch:
                    #print str(i_sample) + " - Tracing: " + str(stochastic.__name__)
                    temp_logp = []
                    for data in N_data[idx+1]:
                        stochastic.value = data
                        temp_logp.append( stochastic.logp )
                    #print "\t |"+ n_stoch +"| Data entries: "+ str(len(N_data[idx+1])) + "  Mean: " + str(numpy.mean(temp_logp)) + " -- Last: " + str(temp_logp[-1])
                    logp[idx][i_sample] = numpy.mean(temp_logp)
                    
        for idx, d_stoch in enumerate(D_traced):
            for stochastic in model.stochastics:
                #print "Comparing " + stochastic.__name__ + " and " + n_stoch 
                if stochastic.__name__ == d_stoch:
                    #print str(i_sample) + " - Tracing: " + str(stochastic.__name__)
                    temp_logp = []
                    for data in D_data[idx+1]:
                        stochastic.value = data
                        temp_logp.append( stochastic.logp )
                    #print "\t |"+ d_stoch +"| Data entries: "+ str(len(D_data[idx+1])) + "  Mean: " + str(numpy.mean(temp_logp)) + " -- Last: " + str(temp_logp[-1])
                    logp[idx + len(N_traced)][i_sample] = numpy.mean(temp_logp)
                    
        for idx, c_stoch in enumerate(C_traced):
            for stochastic in model.stochastics:
                #print "Comparing " + stochastic.__name__ + " and " + n_stoch 
                if stochastic.__name__ == c_stoch:
                    #print str(i_sample) + " - Tracing: " + str(stochastic.__name__)
                    temp_logp = []
                    for data in C_data[idx+1]:
                        stochastic.value = data
                        temp_logp.append( stochastic.logp )
                    #print "\t |"+ c_stoch +"| Data entries: "+ str(len(C_data[idx+1])) + "  Mean: " + str(numpy.mean(temp_logp)) + " -- Last: " + str(temp_logp[-1])
                    logp[idx + len(N_traced) + len(D_traced)][i_sample] = numpy.mean(temp_logp)
    print
    
    summed_logp = numpy.empty(n_samples, numpy.double)
    for i_s in range(n_samples):
        for i in range(len(logp)):
            summed_logp[i_s] += logp[i][i_s]
    
    numpy_summed_logp = numpy.array(summed_logp)
    numpy.save(path + "/summed_logp", numpy_summed_logp)
#    print "Traced logp: "
    
#    pylab.figure(0)
#    for i in range(len(N_traced)):
#        #print logp[i][-10:]
#        pylab.plot(logp[i], label=N_traced[i])
#    pylab.legend(loc = "upper left", shadow = True)
#    
#    pylab.figure(1)
#    for i in range(len(D_traced)):
#        #print logp[i + len(N_traced)][-10:]
#        pylab.plot(logp[i + len(N_traced)], label=D_traced[i])
#    pylab.legend(loc = "upper left", shadow = True)
#    
#    pylab.figure(2)
#    for i in range(len(C_traced)):
#        #print logp[i + len(D_traced) + len(N_traced)][-10:]
#        pylab.plot(logp[i + len(D_traced) + len(N_traced)], label=C_traced[i])
#    pylab.legend(loc = "upper left", shadow = True)
#    
    pylab.figure(0)
    pylab.plot(summed_logp)
    
    #pylab.show()
    return summed_logp

def get_bayesian_factor(logp1, logp2):
    from numpy import exp, log
    print '\t first  term: ' + str(pymc.flib.logsum(-logp1) - log(len(logp1)))
    print '\t second term: ' + str(pymc.flib.logsum(-logp2) - log(len(logp2)))
    print '\t difference: ' + str(pymc.flib.logsum(-logp1) - log(len(logp1)) - ( pymc.flib.logsum(-logp2) - log(len(logp2)) ))
    K = exp( pymc.flib.logsum(-logp1) - log(len(logp1)) - ( pymc.flib.logsum(-logp2) - log(len(logp2)) ) )
    print '\t K value: ' + str(K)
    return K

configurations = {
                  0 : { 'R':2, 'S': [2,2,2,2] , 'basename': 'arch_conf_'},
                  1 : { 'R':2, 'S': [3,2,2,2] , 'basename': 'arch_conf_'},
                  2 : { 'R':2, 'S': [3,3,2,2] , 'basename': 'arch_conf_'},
                  3 : { 'R':2, 'S': [3,3,3,2] , 'basename': 'arch_conf_'},
                  4 : { 'R':2, 'S': [3,3,3,3] , 'basename': 'arch_conf_'},
                  5 : { 'R':3, 'S': [2,2,2,2] , 'basename': 'arch_conf_'},
                  6 : { 'R':3, 'S': [3,2,2,2] , 'basename': 'arch_conf_'},
                  7 : { 'R':3, 'S': [3,3,2,2] , 'basename': 'arch_conf_'},
                  8 : { 'R':3, 'S': [3,3,3,2] , 'basename': 'arch_conf_'},
                  9 : { 'R':3, 'S': [3,3,3,3] , 'basename': 'arch_conf_'},
                  10: { 'R':2, 'S': [2,3,2,2] , 'basename': 'arch_conf_'},
                  11: { 'R':2, 'S': [2,2,3,2] , 'basename': 'arch_conf_'},
                  12: { 'R':2, 'S': [2,2,2,3] , 'basename': 'arch_conf_'},
                  13: { 'R':3, 'S': [2,3,2,2] , 'basename': 'arch_conf_'},
                  14: { 'R':3, 'S': [2,2,3,2] , 'basename': 'arch_conf_'},
                  15: { 'R':3, 'S': [2,2,2,3] , 'basename': 'arch_conf_'},
                  16: { 'R':3, 'S': [3,3,2,3] , 'basename': 'arch_conf_'},
                  17: { 'R':3, 'S': [2,2,2,4] , 'basename': 'arch_conf_'},
                  }

# === SAMPLING ===

#    begin = time.time()
#    mod = MAP(arch_dir_1)
#    mod.fit(iterlim=5, tol=0.001)
#    print "AIC: " + str(mod.AIC)
#    print "BIC: " + str(mod.BIC)
#    print
#    print "total time: " + str( (time.time() - begin) / 60.0 )
for key in [ 0,1,5,6 ]:
    input_S = {1:0, 2:0, 3:0, 4:0}
    for x in range(len(configurations[key]['S'])):
        input_S[x+1] = configurations[key]['S'][x]
    
    signature = str(configurations[key]['R']) + '_' 
    for x in range(len(configurations[key]['S'])):
        signature += str(configurations[key]['S'][x])
    #model_name = configurations[key]['basename'] + signature
    model_name = "v2_" + configurations[key]['basename'] + signature 
    
    print "Sampling model: " + model_name
    mcmcSampler = MCMC( arch_definitive_2.getModel( configurations[key]['R'] , input_S) , db='pickle', dbname = model_name + '.pickle')
    mcmcSampler.sample(iter=20000, burn=5000, thin=50)
    mcmcSampler.db.close()
    
    directory = 'graph/' + model_name
    if( not os.path.isdir(directory) ):
        os.mkdir(directory)
    
    dag(mcmcSampler, format = "png", name = "graph", path = directory)
    #plot(mcmcSampler, path=directory)
    print "Plotting finished"
    print
    
    mcmcSampler = None


# ==== TRACING ====
idx_to_load = []
single_logp = False

db_loaded =     [None, ] * len(idx_to_load)
model_loaded =  [None, ] * len(idx_to_load)
logp_loaded =   [None, ] * len(idx_to_load)

n_N = {1:0, 2:0 ,3:0 ,4:0}
N_data = {}
D_data = {}
C_data = {}

for seg in n_N.keys():
    N_data[seg] = None
    D_data[seg] = None
    C_data[seg] = None

base_path = '../res/tele-aliens/data/'
N_max = numpy.load(base_path + 'n2N_max.npy')
for v in N_max:
    n_N[v[0]] = v[1] + 1
    
for seg in n_N.keys():
    N_data[seg] = numpy.load(base_path + 'n2N' + str(seg) + '.npy')
    D_data[seg] = numpy.load(base_path + 'n2D' + str(seg) + '.npy')
    C_data[seg] = numpy.load(base_path + 'n2C' + str(seg) + '.npy')

for i in range(len(idx_to_load)):
    key = idx_to_load[i]
    
    input_S = {1:0, 2:0, 3:0, 4:0}
    for x in range(len(configurations[key]['S'])):
        input_S[x+1] = configurations[key]['S'][x]
    
    signature = str(configurations[key]['R']) + '_' 
    for x in range(len(configurations[key]['S'])):
        signature += str(configurations[key]['S'][x])
    model_name = configurations[key]['basename'] + signature
    
    print "Loading model: " + model_name
    db_loaded[i] = pymc.database.pickle.load( model_name + '.pickle' )
    #model_loaded[i] = MCMC( arch_definitive.getModel( configurations[key]['R'] , input_S) , db = db_loaded[i])
    model_loaded[i] = pymc.Model(arch_definitive_2.getModel( configurations[key]['R'] , input_S))
    print "deviance " + str(i)
    print db_loaded[i].trace('deviance')[-10:]
    

    if(single_logp):
        logp_loaded[i] = get_logp(model_loaded[i], db_loaded[i])
        print "log p " + str(i)
        print logp_loaded[i][-10:]
        print
    else:
        logp_loaded[i] = get_divided_logp(model_loaded[i], db_loaded[i], N_data, D_data, C_data, 'graph/' + model_name)

#results =  [ [None,] * len(idx_to_load), ] * len(idx_to_load)
if (single_logp): 
    results = numpy.zeros((len(idx_to_load), len(idx_to_load)))
    for x in range(len(idx_to_load)):
        for y in range(len(idx_to_load)):
            print "Comparing %i, %i" % (x, y)
            results[x,y] = get_bayesian_factor( logp_loaded[x], logp_loaded[y] )
    
    for row in results:
        print row
else:
    for x in range(len(idx_to_load)):
        print logp_loaded[x][-5:]
        pylab.figure(1)
        pylab.plot(logp_loaded[x], label="Model %i" %x)
        
    #pylab.legend(loc = "upper left", shadow = True)
    #pylab.show()
    
print "done"
Freq = 2400 # Set Frequency To 2500 Hertz
Dur = 400 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)
winsound.Beep(Freq,Dur)