'''
Created on 06/giu/2013

@author: Christian
'''

from os import walk
from os import rename
from os import remove
from shutil import copy
import random

meshesDirectory = 'C:/Users/Christian/Desktop/Tesi/Modelli/chairs-large/shapes/'
segmentsDirectory = 'C:/Users/Christian/Desktop/Tesi/Modelli/chairs-large/gt/'

targetMeshesDir     = '../res/chairs/shapes/'
targetSegmentsDir   = '../res/chairs/gt/'

meshesList = []
meshesPath = []

meshesToBeSelected = 100

for (dirpath, dirname, filenames) in walk(meshesDirectory):
    meshesList.extend(filenames)
    break

for f in meshesList:
    path = meshesDirectory + f
    meshesPath.append(path)

selectedMesh = random.sample( meshesPath, meshesToBeSelected )


print selectedMesh
for m in selectedMesh:
    temp = m.split('/')[-1]
    temp = temp.split('.')[0]
    print temp
    copy(meshesDirectory + temp + '.off', targetMeshesDir + temp + '.off')
    copy(segmentsDirectory + temp + '.seg', targetSegmentsDir + temp + '.seg')
    
