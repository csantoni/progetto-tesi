'''
Created on 17/apr/2013

@author: Christian
'''

import pymc as mc
import numpy as np
import random
from pymc.graph import dag
from pymc.Matplot import plot
import os
import winsound

def getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} ):
    n_R = input_R
    m = 35
    f_len = 33
    
    n_segments = 4
    n_N = {1:0, 2:0 ,3:0 ,4:0}
    n_S = input_S
    N_data = {}
    D_data = {}
    C_data = {}
    
    for seg in n_N.keys():
        N_data[seg] = None
        D_data[seg] = None
        C_data[seg] = None
    
    base_path = '../res/tele-aliens/data/'
    N_max = np.load(base_path + 'n2N_max.npy')
    for v in N_max:
        n_N[v[0]] = v[1] + 1
        
    for seg in n_N.keys():
        N_data[seg] = np.load(base_path + 'n2N' + str(seg) + '.npy')
        D_data[seg] = np.load(base_path + 'n2D' + str(seg) + '.npy')
        print D_data[seg]
        print 
        C_data[seg] = np.load(base_path + 'n2C' + str(seg) + '.npy')
    
    
    C_max = np.max( np.load(base_path + 'n2C_max.npy') )
    C_min = np.min( np.load(base_path + 'n2C_min.npy') )
    
    alpha_N = [None, ] * len(n_N.keys())
    theta_N = [None, ] * len(n_N.keys())
    cpt_N = [None, ] * len(n_N.keys())
    N = [None, ] * len(n_N.keys())
    
    alpha_S = [None, ] * len(n_N.keys())
    theta_S = [None, ] * len(n_N.keys())
    cpt_S = [None, ] * len(n_N.keys())
    S = [None, ] * len(n_N.keys())
    
    alpha_D = [None, ] * len(n_N.keys())
    theta_D = [None, ] * len(n_N.keys())
    cpt_D = [None, ] * len(n_N.keys())
    D = [None, ] * len(n_N.keys())
    
    taus_C = {}
    n_mus_C = {}
    n_tau_C = {}
    t_t = {}
    m_n = {}
    t_n = {}
    
#    tau = [None, ] * len(n_N.keys())
#    mu = [None, ] * len(n_N.keys())
#    taus = [None, ] * len(n_N.keys())
#    mus = [None, ] * len(n_N.keys())
#    C = [None, ] * len(n_N.keys())
    
    tau = {}
    mu = {}
    taus = {}
    mus = {}
    C = {}
    
    predN = {}
    predD = {}
    predC = {}
    
    # ======= R VARIABLE ========
    cpt_R = mc.Dirichlet( name ='cpt_R', theta = [1.0, ] * n_R)
    R = mc.Categorical('R', p = cpt_R)
        
    dirichs_N = {}
    categs_N = {}
    pred_categs_N = {}
    for seg in n_N.keys():
        dirichs_N[seg] = []
        categs_N[seg] = []
        pred_categs_N[seg] = []
        for i in range(n_R):
            dirichs_N[seg].append(mc.Dirichlet(name='cpt_N%i_R%i' % (seg, i), theta = [1.0 ] * n_N[seg] , plot = False))
            categs_N[seg].append( mc.Categorical(name='N%i_R%i' % (seg, i), p = dirichs_N[seg][i], observed=True, value=N_data[seg]) )
            pred_categs_N[seg].append( mc.Categorical( name='predN%i_R%i' % (seg, i), p = dirichs_N[seg][i], plot = True) )
            
#    @mc.deterministic(name = 'N1')
#    def N1(catsN1 = categs_N[1], Rval = R):
#        return catsN1[Rval]
#    
#    @mc.deterministic(name = 'N2')
#    def N2(catsN2 = categs_N[2], Rval = R):
#        return catsN2[Rval]
#    
#    @mc.deterministic(name = 'N3')
#    def N3(catsN3 = categs_N[3], Rval = R):
#        return catsN3[Rval]
#    
#    @mc.deterministic(name = 'N4')
#    def N4(catsN4 = categs_N[4], Rval = R):
#        return catsN4[Rval]
    
    dirichs_S = {}
    categs_S = {}
    for seg in n_N.keys():
        dirichs_S[seg] = []
        categs_S[seg] = []
        for i in range(n_R):
            dirichs_S[seg].append(mc.Dirichlet(name='cpt_S%i_R%i' % (seg, i), theta = [1.0 ] * n_S[seg]))
            categs_S[seg].append( mc.Categorical(name='S%i_R%i' % (seg, i), p = dirichs_S[seg][i], plot = False) )
        
    @mc.deterministic(name = 'S1')
    def S1(catsS1 = categs_S[1], Rval = R):
        return catsS1[Rval]
    
    @mc.deterministic(name = 'S2')
    def S2(catsS2 = categs_S[2], Rval = R):
        return catsS2[Rval]
    
    @mc.deterministic(name = 'S3')
    def S3(catsS3 = categs_S[3], Rval = R):
        return catsS3[Rval]
    
    @mc.deterministic(name = 'S4')
    def S4(catsS4 = categs_S[4], Rval = R):
        return catsS4[Rval]
    
    dirichs_D = {}
    categs_D = {}
    pred_categs_D = {}
    for seg in n_N.keys():
        dirichs_D[seg] = []
        categs_D[seg] = []
        pred_categs_D[seg] = []
        for i in range(n_S[seg]):
            dirichs_D[seg].append(mc.Dirichlet(name='cpt_D%i_S%i' % (seg, i), theta = [1.0 ] * m , plot = False))
            categs_D[seg].append( mc.Categorical( name='D%i_S%i' % (seg, i), p = dirichs_D[seg][i], observed = True, value = D_data[seg]) )
            pred_categs_D[seg].append( mc.Categorical( name='predD%i_S%i' % (seg, i), p = dirichs_D[seg][i], plot = True) )
        
        
#    @mc.deterministic(name = 'D1')
#    def D1(catsD1 = categs_D[1], Sval = S1):
#        return catsD1[Sval]
#    
#    @mc.deterministic(name = 'D2')
#    def D2(catsD2 = categs_D[2], Sval = S2):
#        return catsD2[Sval]
#    
#    @mc.deterministic(name = 'D3')
#    def D3(catsD3 = categs_D[3], Sval = S3):
#        return catsD3[Sval]
#    
#    @mc.deterministic(name = 'D4')
#    def D4(catsD4 = categs_D[4], Sval = S4):
#        return catsD4[Sval]


    S_arr = {1:S1,2:S2,3:S3,4:S4}
        
    mu_arr = {}
    tau_arr = {}
    C_arr = {}
    for seg in n_N.keys():
        mu_arr[seg] = []
        tau_arr[seg] = []
        C_arr[seg] = []
        for i in range(n_S[seg]):
            mu_arr[seg].append( mc.Normal( 'mu_C%i_S%i' % (seg, i), mu = 0.0 , tau = 1e-5, size = f_len) )
            tau_arr[seg].append( mc.Wishart('tau_C%i_S%i' % (seg, i),  n = f_len + 1, Tau = np.eye(f_len) ) )
            C_arr[seg].append( mc.MvNormal('C%i_S%i' % (seg, i), mu = mu_arr[seg][i], tau = tau_arr[seg][i], observed = True, value = C_data[seg]) )

#    @mc.deterministic(name = 'C1', plot = True)
#    def C1(C_arr = C_arr[1], Sval = S1):
#        return C_arr[Sval]
#    
#    @mc.deterministic(name = 'C2')
#    def C2(C_arr = C_arr[2], Sval = S2):
#        return C_arr[Sval]
#    
#    @mc.deterministic(name = 'C3')
#    def C3(C_arr = C_arr[3], Sval = S3):
#        return C_arr[Sval]
#    
#    @mc.deterministic(name = 'C4')
#    def C4(C_arr = C_arr[4], Sval = S4):
#        return C_arr[Sval]
    
    return locals()
    
if __name__ == "__main__":
    locals = getModel( input_R = 2, input_S = {1:2, 2:2 ,3:2 ,4:2} )
    
    A = mc.Model(locals)
    dag(A, format="png", name="test_modelli", path="graph/")
    
    mcmcSampler = mc.MCMC(locals)
    
    #mcmcSampler.use_step_method(mc.DiscreteMetropolis, mcmcSampler.R, proposal_distribution='Normal')
    
    mcmcSampler.sample(iter = 3000)
    
    print mcmcSampler.step_method_dict
    print
    
    directory = 'graph/test_modelli'
    if( not os.path.isdir(directory) ):
        os.mkdir(directory)
    
    plot(mcmcSampler, path=directory)
    #plot(mcmcSampler.trace("R"), path=directory)
#    plot(mcmcSampler.trace("Ntry"), path=directory)
    
    print "done"
    Freq = 2400 # Set Frequency To 2500 Hertz
    Dur = 400 # Set Duration To 1000 ms == 1 second
    winsound.Beep(Freq,Dur)
    winsound.Beep(Freq,Dur)