'''
Created on 10/apr/2013

@author: Christian
'''

import numpy

def saveGeometricFeatureVector(directory):
    comp_file = open(directory + 'list.txt', 'r')
    comp_list = []
    for line in comp_file:
        comp_list.append(line[:-1])
    
    print comp_list
    
    for comp in comp_list:
        path = comp.split('_')
        path = directory + "_".join([path[2], path[3]]) + "/" + "_".join([path[4], path[5][:-4]]) + "/"

        #[[MIN_4, MIN_8, MIN_16], [MAX_4, MAX_8, MAX_16]]
        [[[_, min4],[_, min8],[_, min16]],[[_, max4],[_, max8],[_, max16]]] = numpy.load(path + 'curv.npy')
        flat_curv = numpy.concatenate((min4, min8, min16, max4, max8, max16))
               
        sd_4 = numpy.load(path + '/sd_hist4.npy')
        sd_8 = numpy.load(path + '/sd_hist8.npy')
        sd_16 = numpy.load(path + '/sd_hist16.npy')
        log_sd_4 = numpy.load(path + '/sd_log_hist4.npy')
        log_sd_8 = numpy.load(path + '/sd_log_hist8.npy')
        log_sd_16 = numpy.load(path + '/sd_log_hist16.npy')
        flat_sdf = numpy.concatenate((sd_4, sd_8, sd_16, log_sd_4, log_sd_8, log_sd_16, ))
        
        eigen_features = numpy.load(path + '/eigen.npy')
        geom_vector = numpy.concatenate((flat_curv, flat_sdf, eigen_features))
        
        numpy.save(path + "geom", geom_vector)
        
if __name__ == "__main__":
    basePath = "../res/tele-aliens/data/"
    model_list = [x for x in range(126, 201)]
    for model in model_list:
        saveGeometricFeatureVector(basePath + str(model) + "/")
    print "done!"