'''
Created on 07/giu/2013

@author: Christian
'''

import numpy
import sys
import pickle

from os import walk
from os import mkdir
from os.path import isdir

from time import time

def writePredModel(seg_num):
    
    model = open( basePathBUGS + "modelPred.txt", "wb")
    
    model.write("model\n")
    model.write("{\n")
    model.write("  for( i in 1 : N_samples ){\n")
    model.write("    R[i] ~ dcat( p_R[ ] )\n")
    for k in range(seg_num):
        model.write("    N%d[i] ~ dcat( p_N%d[ R[i], ] )\n" % ( k+1, k+1 ))
    model.write("\n")
    for k in range(seg_num):
        model.write("    S%d[i] ~ dcat( p_S%d[ R[i], ] )\n" % ( k+1, k+1 ))
    model.write("\n")    
    for k in range(seg_num):
        model.write("    D%d[i] ~ dcat( p_D%d[ S%d[i], ] )\n" % ( k+1, k+1, k+1 ))
    model.write("\n")    
    for k in range(seg_num):
        model.write("    for ( j in (sub_C%d[i] + 1) : sub_C%d[i+1] ){\n" % ( k+1, k+1 ))
        model.write("        C%d[ j , 1:C_feat_len] ~ dmnorm( mu_C%d[ S%d[i], ], tau_C%d[ S%d[i], , ] )\n" % ( k+1, k+1 , k+1 , k+1 , k+1 ))
        model.write("    }\n")
    model.write("  }\n")
    model.write("\n")
    
    model.write("  # ========== Predictive Posterior ==========\n")
    model.write("  predR ~ dcat( p_R[ ] )\n")
    model.write("\n")
    model.write("  for ( i in 1 : R_styles ){\n")
    for k in range(seg_num):
        model.write("      predN%d[i] ~ dcat( p_N%d[ i, ] )\n" % (k+1, k+1))
    model.write("\n")
    for k in range(seg_num):
        model.write("      predS%d[i] ~ dcat( p_S%d[ i, ] )\n" % (k+1, k+1))
    model.write("  }\n")
    model.write("\n")
    for k in range(seg_num):
        model.write("  for ( i in 1 : S_styles[%d] ){\n" % (k+1))
        model.write("      predD%d[i] ~ dcat( p_D%d[ i, ] )\n" % (k+1, k+1))
        model.write("      predC%d[i, 1:C_feat_len] ~ dmnorm( mu_C%d[ i, ], tau_C%d[ i, , ] )\n" % (k+1, k+1, k+1))
        model.write("  }\n")
    model.write("\n")
    model.write("  # ========== VARIABLE R ==========\n")
    
    model.write("  for ( i in 1 : R_styles ){\n")
    model.write("          alpha_R[i] <- 1\n")
    model.write("  }\n")
    model.write("  p_R[1 : R_styles] ~ ddirich(alpha_R[ ])\n")    
    model.write("\n")
    
    model.write("  # ========== VARIABLES N AND S ==========\n")
    
    model.write("  for ( i in 1 : segment_number ){\n")
    model.write("      for ( j in 1 : R_styles ){\n")
    model.write("          for ( k in 1 : N_max[i] ){\n")
    model.write("              alpha_N[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("          for ( k in 1 : S_styles[i] ){\n")
    model.write("              alpha_S[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("      }\n")
    model.write("  }\n")
    
    model.write("  for ( k in 1:R_styles){\n")
    for k in range(seg_num):
        model.write("    p_N%d[k, 1 : N_max[%d] ] ~ ddirich( alpha_N[ %d, k,1 : N_max[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
    model.write("\n")
    for k in range(seg_num):
        model.write("    p_S%d[k, 1 : S_styles[%d] ] ~ ddirich( alpha_S[%d, k,1 : S_styles[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
    model.write("  }\n")
    
    model.write("\n")
    model.write("\n")
    
    model.write("  # ========== VARIABLES D AND C ==========\n")
    
    model.write("  for ( i in 1 : segment_number ){\n")
    model.write("      for ( j in 1 : S_styles[i] ){\n")
    model.write("          for ( k in 1 : D_max[i] ){\n")
    model.write("              alpha_D[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("      }\n")
    model.write("  }\n")
    model.write("\n")
    for k in range(seg_num):
        model.write("  for ( k in 1 : S_styles[%d]){\n" % (k+1))
        model.write("      p_D%d[k, 1 : D_max[%d] ] ~ ddirich( alpha_D[ %d, k,1 : D_max[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
        model.write("\n")
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          mu_norm_C%d[k, i] <- 0\n" % ( k+1 ))
        model.write("      }\n")
        model.write("\n")    
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          for ( j in 1 : C_feat_len ){\n")
        model.write("              prec_norm_C%d[k, i , j] <- equals(i,j) * 0.001\n" % ( k+1 ))
        model.write("          }\n")
        model.write("      }\n")
        model.write("      mu_C%d[k, 1: C_feat_len ] ~ dmnorm( mu_norm_C%d[ k, ], prec_norm_C%d[ k, , ] )\n" % ( k+1, k+1, k+1 ))
        model.write("\n")    
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          for ( j in 1 : C_feat_len ){\n")
        model.write("              phi_wish_C%d[k, i , j] <- equals(i,j) * 0.0001\n" % ( k+1 ))
        model.write("          }\n")
        model.write("      }\n")
        model.write("      tau_C%d[k, 1: C_feat_len, 1: C_feat_len ] ~ dwish(phi_wish_C%d[k, , ],  wish_nu )\n" % ( k+1, k+1 ))
        model.write("  }\n")
        model.write("\n")
    model.write("  wish_nu <- C_feat_len + 1\n")
    model.write("}")
    
    model.close()

def writePredScript(seg_num, absolutePath):
    
    scriptPred = open( basePathBUGS + "scriptPred.txt", "wb")
    
    scriptPred.write("modelDisplay('log')\n")
    scriptPred.write("\n")
    scriptPred.write("modelCheck('" + absolutePath + "modelPred.txt')\n")
    scriptPred.write("modelData('" + absolutePath + "features_NCD.txt')\n")
    scriptPred.write("modelData('" + absolutePath + "bestConf.txt')\n")
    scriptPred.write("modelCompile(1)\n")
    scriptPred.write("modelGenInits()   \n")
    scriptPred.write("\n")
    scriptPred.write("# Burn-in\n")
    scriptPred.write("modelUpdate(500)\n")
    scriptPred.write("\n")
    scriptPred.write("#Learning\n")
    scriptPred.write("samplesSet('deviance')\n")
    scriptPred.write("modelUpdate(3000)\n")
    scriptPred.write("samplesStats('deviance')\n")
    scriptPred.write("samplesHistory('deviance')\n")
    scriptPred.write("\n")
    scriptPred.write("#Sampling from posterior\n")
    scriptPred.write("samplesSet('p_R')\n")
    scriptPred.write("samplesSet('predR')\n")
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesSet('predN%d')\n" % (k+1))
    scriptPred.write("\n")    
    for k in range(seg_num):
        scriptPred.write("samplesSet('predS%d')\n" % (k+1))
    scriptPred.write("\n")    
    for k in range(seg_num):
        scriptPred.write("samplesSet('p_S%d')\n" % (k+1))
    scriptPred.write("\n")    
    for k in range(seg_num):
        scriptPred.write("samplesSet('predD%d')\n" % (k+1))
    scriptPred.write("\n")    
    for k in range(seg_num):
        scriptPred.write("samplesSet('predC%d')\n" % (k+1))
    scriptPred.write("\n")
    scriptPred.write("modelUpdate(500)\n")
    scriptPred.write("\n")
    scriptPred.write("\n")
    scriptPred.write("samplesStats('p_R')\n")
    scriptPred.write("samplesDensity('predR')\n")
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesStats('predN%d')\n" % (k+1))
        scriptPred.write("samplesDensity('predN%d')\n" % (k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesStats('p_S%d')\n" % (k+1))
        scriptPred.write("samplesDensity('predS%d')\n" % (k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesStats('predD%d')\n" % (k+1))
        scriptPred.write("samplesDensity('predD%d')\n" % (k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesStats('predC%d')\n" % (k+1))
        scriptPred.write("samplesDensity('predC%d')\n" % (k+1))
    scriptPred.write("\n")
    scriptPred.write("modelSaveLog('" + absolutePath + "fittedLog.odc')\n")
    scriptPred.write("samplesCoda( 'deviance' , '" + absolutePath + "fit_dev' )\n") 
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesCoda( 'predN%d' , '%sfit_predN%d' )\n" % (k+1, absolutePath, k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesCoda( 'predD%d' , '%sfit_predD%d' )\n" % (k+1, absolutePath, k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesCoda( 'predC%d' , '%sfit_predC%d' )\n" % (k+1, absolutePath, k+1))
    scriptPred.write("\n")
    scriptPred.write("modelQuit('yes')")
    
    scriptPred.close()

if __name__ == "__main__":
    
    meshesDirectory     = '../res/chairs/data/'
    
    basePath = '../res/chairs/data/'
    basePathBUGS = basePath + "openBUGS/"
    absolutePath = "C:/Users/Christian/workspace/progetto-tesi-new/res/chairs/data/openBUGS/"
    
    starter = open( basePathBUGS + "starter.txt", "r")
    
    s = starter.read().split(' ')
    seg_num, n_max, d_max, n_samples, feat_len = ( int(s[0]), int(s[1]), int(s[2]), int(s[3]), int(s[4]) )
    
    #Starting configuration
    R = 2
    S = [2, ] * seg_num
    
    openBUGSPath = 'C:\Program Files (x86)\OpenBUGS\OpenBUGS322\OpenBUGS.exe'
    print openBUGSPath
    scriptPath = '\\'.join((absolutePath + "script.txt").split('/'))
    
    
    maxFail = (seg_num + 1) * 2
    failCount = 0
    S_index = 0
    BIC = 1e6
    bestR = 0
    bestS = []

    #Scrittura modello e script per predictive posterior sampling
    writePredModel(seg_num)
    writePredScript(seg_num, absolutePath)
