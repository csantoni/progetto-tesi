'''
Created on 28/mag/2013

@author: Christian
'''

import math

def computePenaltyFactor( R_styles, S_styles, N_max = (7,7,7,7), D_max = (35,35,35,35), C_feat_len = 14, n_samples = 70 ):
    
    res = R_styles
    
    for seg in range(len(N_max)):
        res += R_styles * N_max[seg]
        res += R_styles * S_styles[seg]
        res += D_max[seg] * S_styles[seg]
        res += C_feat_len * S_styles[seg]
        res += 2 * C_feat_len * C_feat_len * S_styles[seg]
    
    penalties = [ res * math.log(n_samples), 2 * res ] # BIC, AIC
    
    return penalties    
        
if __name__ == "__main__":
    R = (2,3,4,5)
    S = (2,3,4,5)
    
    for r in R:
        print "R styles: " + str(r)
        for s1 in S:
            for s2 in S:
                for s3 in S:
                    s = ''
                    for s4 in S:
                        p = computePenaltyFactor( r, (s1,s2,s3,s4) )
                        s += ' (%d, %d, %d, %d) = (%d, %d)  |' % ( s1, s2, s3, s4, p[0], p[1] )
                    print s