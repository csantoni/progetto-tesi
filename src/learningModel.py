'''
Created on 06/giu/2013

@author: Christian
'''

import numpy
import subprocess
import winsound
import math

from os import system
from time import time

def writeConfiguration(basePathBUGS, isBest, seg_num, n_max, d_max, n_samples, feat_len, R, S):
    
    #conf = open( basePathBUGS + "conf_R%d_S%s.txt" % ( R, ''.join( [str(x) for x in S] ) ), "wb" )
    if isBest:
        conf = open( basePathBUGS + "bestConf.txt", "wb" )
    else:
        conf = open( basePathBUGS + "conf.txt", "wb" )
        
    conf.write("list(\n")
    conf.write("  segment_number = %d,\n" % seg_num)
    conf.write("\n")
    conf.write("  R_styles = %d,\n" % R)
    conf.write("\n")
    conf.write("  S_styles = c(%s),\n" % (','.join([str(x) for x in S])) )
    conf.write("\n")
    conf.write("  N_samples = %d,\n" % n_samples)
    conf.write("  N_max = c(%s),\n" % ( ','.join( [str(n_max), ] * seg_num ) ) )
    conf.write("\n")
    conf.write("  D_max = c(%s),\n" % ( ','.join( [str(d_max), ] * seg_num ) ) )
    conf.write("\n")
    conf.write("  C_feat_len = %d\n" % feat_len)
    conf.write(")")
    
    conf.close()
    
def writeScript(basePathBUGS, absolutePath, R, S):
    
    conf = open( basePathBUGS + "script.txt", "wb" )
    
    conf.write("modelDisplay('log')\n")
    conf.write("modelCheck('" + absolutePath + "model.txt')\n")
    conf.write("modelData('" + absolutePath + "features_NCD.txt')\n")
    conf.write("modelData('" + absolutePath + "conf.txt')\n")
    conf.write("modelCompile(1)\n")
    conf.write("modelGenInits()\n")
    conf.write("modelUpdate(1000)\n")
    conf.write("samplesSet('deviance')\n")
    conf.write("modelUpdate(5000)\n")
    conf.write("samplesStats('deviance')\n")
    conf.write("samplesHistory('deviance')\n")
    conf.write("modelSaveState('" + absolutePath + "state')\n")
    conf.write("modelSaveLog('" + absolutePath + "log.odc')\n")
    conf.write("samplesCoda( 'deviance' , ('" + absolutePath + "dev' )\n") 
    conf.write("modelQuit('yes')")
    
    conf.close()

def computePenaltyFactor( R_styles, S_styles, N_max, D_max, C_feat_len, n_samples ):
    res = R_styles
    
    for seg in range(len(N_max)):
        res += R_styles * N_max[seg]
        res += R_styles * S_styles[seg]
        res += D_max[seg] * S_styles[seg]
        res += C_feat_len * S_styles[seg]
        res += 2 * C_feat_len * C_feat_len * S_styles[seg]
    
    penalties = [ res * math.log(n_samples), 2 * res ] # BIC, AIC
    
    return penalties  

def computeBIC(basePathBUGS, n_max, d_max, n_samples, feat_len, R, S):
    #Open deviance trace
    dev_trace = open(basePathBUGS + 'devCODAchain1.txt', 'r')
    dev_values = []
    
    for line in dev_trace:
        dev_values.append( float(line.split('\t')[1]) )
    dev_values = numpy.array(dev_values)
    
    dev_mean = numpy.mean(dev_values)
#    print "Mean: %f" % dev_mean
    
    # BIC = DEV + K * LN(N_SAMPLES)
    
    #Computing number of free parameters
    k = computePenaltyFactor(R, S, n_max, d_max, feat_len, n_samples)
#    print "BIC: %f" % ( dev_mean + k[0] )
#    print "AIC: %f" % ( dev_mean + k[1] )
#    print "Penalties: %f, %f" % (k[0], k[1])
    
    print "\tBIC: %.2f --- AIC: %.2f --- Mean: %.2f" % ( dev_mean + k[0], dev_mean + k[1], dev_mean )
    
    return dev_mean + k[0]

def writePredModel(seg_num):
    
    model = open( basePathBUGS + "modelPred.txt", "wb")
    
    model.write("model\n")
    model.write("{\n")
    model.write("  for( i in 1 : N_samples ){\n")
    model.write("    R[i] ~ dcat( p_R[ ] )\n")
    for k in range(seg_num):
        model.write("    N%d[i] ~ dcat( p_N%d[ R[i], ] )\n" % ( k+1, k+1 ))
    model.write("\n")
    for k in range(seg_num):
        model.write("    S%d[i] ~ dcat( p_S%d[ R[i], ] )\n" % ( k+1, k+1 ))
    model.write("\n")    
    for k in range(seg_num):
        model.write("    D%d[i] ~ dcat( p_D%d[ S%d[i], ] )\n" % ( k+1, k+1, k+1 ))
    model.write("\n")    
    for k in range(seg_num):
        model.write("    for ( j in (sub_C%d[i] + 1) : sub_C%d[i+1] ){\n" % ( k+1, k+1 ))
        model.write("        C%d[ j , 1:C_feat_len] ~ dmnorm( mu_C%d[ S%d[i], ], tau_C%d[ S%d[i], , ] )\n" % ( k+1, k+1 , k+1 , k+1 , k+1 ))
        model.write("    }\n")
    model.write("  }\n")
    model.write("\n")
    
    model.write("  # ========== Predictive Posterior ==========\n")
    model.write("  predR ~ dcat( p_R[ ] )\n")
    model.write("\n")
    model.write("  for ( i in 1 : R_styles ){\n")
    for k in range(seg_num):
        model.write("      predN%d[i] ~ dcat( p_N%d[ i, ] )\n" % (k+1, k+1))
    model.write("\n")
    for k in range(seg_num):
        model.write("      predS%d[i] ~ dcat( p_S%d[ i, ] )\n" % (k+1, k+1))
    model.write("  }\n")
    model.write("\n")
    for k in range(seg_num):
        model.write("  for ( i in 1 : S_styles[%d] ){\n" % (k+1))
        model.write("      predD%d[i] ~ dcat( p_D%d[ i, ] )\n" % (k+1, k+1))
        model.write("      predC%d[i, 1:C_feat_len] ~ dmnorm( mu_C%d[ i, ], tau_C%d[ i, , ] )\n" % (k+1, k+1, k+1))
        model.write("  }\n")
    model.write("\n")
    model.write("  # ========== VARIABLE R ==========\n")
    
    model.write("  for ( i in 1 : R_styles ){\n")
    model.write("          alpha_R[i] <- 1\n")
    model.write("  }\n")
    model.write("  p_R[1 : R_styles] ~ ddirich(alpha_R[ ])\n")    
    model.write("\n")
    
    model.write("  # ========== VARIABLES N AND S ==========\n")
    
    model.write("  for ( i in 1 : segment_number ){\n")
    model.write("      for ( j in 1 : R_styles ){\n")
    model.write("          for ( k in 1 : N_max[i] ){\n")
    model.write("              alpha_N[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("          for ( k in 1 : S_styles[i] ){\n")
    model.write("              alpha_S[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("      }\n")
    model.write("  }\n")
    
    model.write("  for ( k in 1:R_styles){\n")
    for k in range(seg_num):
        model.write("    p_N%d[k, 1 : N_max[%d] ] ~ ddirich( alpha_N[ %d, k,1 : N_max[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
    model.write("\n")
    for k in range(seg_num):
        model.write("    p_S%d[k, 1 : S_styles[%d] ] ~ ddirich( alpha_S[%d, k,1 : S_styles[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
    model.write("  }\n")
    
    model.write("\n")
    model.write("\n")
    
    model.write("  # ========== VARIABLES D AND C ==========\n")
    
    model.write("  for ( i in 1 : segment_number ){\n")
    model.write("      for ( j in 1 : S_styles[i] ){\n")
    model.write("          for ( k in 1 : D_max[i] ){\n")
    model.write("              alpha_D[ i, j, k ] <- 1\n")
    model.write("          }\n")
    model.write("      }\n")
    model.write("  }\n")
    model.write("\n")
    for k in range(seg_num):
        model.write("  for ( k in 1 : S_styles[%d]){\n" % (k+1))
        model.write("      p_D%d[k, 1 : D_max[%d] ] ~ ddirich( alpha_D[ %d, k,1 : D_max[%d] ] )\n" % ( k+1, k+1, k+1, k+1 ))
        model.write("\n")
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          mu_norm_C%d[k, i] <- 0\n" % ( k+1 ))
        model.write("      }\n")
        model.write("\n")    
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          for ( j in 1 : C_feat_len ){\n")
        model.write("              prec_norm_C%d[k, i , j] <- equals(i,j) * 0.001\n" % ( k+1 ))
        model.write("          }\n")
        model.write("      }\n")
        model.write("      mu_C%d[k, 1: C_feat_len ] ~ dmnorm( mu_norm_C%d[ k, ], prec_norm_C%d[ k, , ] )\n" % ( k+1, k+1, k+1 ))
        model.write("\n")    
        model.write("      for ( i in 1 : C_feat_len ){\n")
        model.write("          for ( j in 1 : C_feat_len ){\n")
        model.write("              phi_wish_C%d[k, i , j] <- equals(i,j) * 0.0001\n" % ( k+1 ))
        model.write("          }\n")
        model.write("      }\n")
        model.write("      tau_C%d[k, 1: C_feat_len, 1: C_feat_len ] ~ dwish(phi_wish_C%d[k, , ],  wish_nu )\n" % ( k+1, k+1 ))
        model.write("  }\n")
        model.write("\n")
    model.write("  wish_nu <- C_feat_len + 1\n")
    model.write("}")
    
    model.close()

def writePredScript(seg_num, absolutePath):
    
    scriptPred = open( basePathBUGS + "scriptPred.txt", "wb")
    
    scriptPred.write("modelDisplay('log')\n")
    scriptPred.write("\n")
    scriptPred.write("modelCheck('" + absolutePath + "modelPred.txt')\n")
    scriptPred.write("modelData('" + absolutePath + "features_NCD.txt')\n")
    scriptPred.write("modelData('" + absolutePath + "bestConf.txt')\n")
    scriptPred.write("modelCompile(1)\n")
    scriptPred.write("modelGenInits()   \n")
    scriptPred.write("\n")
    scriptPred.write("# Burn-in\n")
    scriptPred.write("modelUpdate(500)\n")
    scriptPred.write("\n")
    scriptPred.write("#Learning\n")
    scriptPred.write("samplesSet('deviance')\n")
    scriptPred.write("modelUpdate(3000)\n")
    scriptPred.write("samplesStats('deviance')\n")
    scriptPred.write("samplesHistory('deviance')\n")
    scriptPred.write("\n")
    scriptPred.write("#Sampling from posterior\n")
    scriptPred.write("samplesSet('p_R')\n")
    scriptPred.write("samplesSet('predR')\n")
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesSet('predN%d')\n" % (k+1))
    scriptPred.write("\n")    
    for k in range(seg_num):
        scriptPred.write("samplesSet('predS%d')\n" % (k+1))
    scriptPred.write("\n")    
    for k in range(seg_num):
        scriptPred.write("samplesSet('p_S%d')\n" % (k+1))
    scriptPred.write("\n")    
    for k in range(seg_num):
        scriptPred.write("samplesSet('predD%d')\n" % (k+1))
    scriptPred.write("\n")    
    for k in range(seg_num):
        scriptPred.write("samplesSet('predC%d')\n" % (k+1))
    scriptPred.write("\n")
    scriptPred.write("modelUpdate(500)\n")
    scriptPred.write("\n")
    scriptPred.write("\n")
    scriptPred.write("samplesStats('p_R')\n")
    scriptPred.write("samplesDensity('predR')\n")
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesStats('predN%d')\n" % (k+1))
        scriptPred.write("samplesDensity('predN%d')\n" % (k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesStats('p_S%d')\n" % (k+1))
        scriptPred.write("samplesDensity('predS%d')\n" % (k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesStats('predD%d')\n" % (k+1))
        scriptPred.write("samplesDensity('predD%d')\n" % (k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesStats('predC%d')\n" % (k+1))
        scriptPred.write("samplesDensity('predC%d')\n" % (k+1))
    scriptPred.write("\n")
    scriptPred.write("modelSaveLog('" + absolutePath + "fittedLog.odc')\n")
    scriptPred.write("samplesCoda( 'deviance' , '" + absolutePath + "fit_dev' )\n") 
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesCoda( 'predN%d' , '%sfit_predN%d' )\n" % (k+1, absolutePath, k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesCoda( 'predD%d' , '%sfit_predD%d' )\n" % (k+1, absolutePath, k+1))
    scriptPred.write("\n")
    for k in range(seg_num):
        scriptPred.write("samplesCoda( 'predC%d' , '%sfit_predC%d' )\n" % (k+1, absolutePath, k+1))
    scriptPred.write("\n")
    scriptPred.write("modelQuit('yes')")
    
    scriptPred.close()

if __name__ == "__main__":
    
    meshesDirectory     = '../res/chairs/data/'
    
    basePath = '../res/chairs/data/'
    basePathBUGS = basePath + "openBUGS/"
    absolutePath = "C:/Users/Christian/workspace/progetto-tesi-new/res/chairs/data/openBUGS/"
    
    starter = open( basePathBUGS + "starter.txt", "r")
    
    s = starter.read().split(' ')
    seg_num, n_max, d_max, n_samples, feat_len = ( int(s[0]), int(s[1]), int(s[2]), int(s[3]), int(s[4]) )
    
    #Starting configuration
    R = 2
    S = [2, ] * seg_num
    
    #writeConfiguration( basePathBUGS, seg_num, n_max, d_max, n_samples, feat_len, R, S )
    writeScript(basePathBUGS, absolutePath, R, S)
    openBUGSPath = 'C:\Program Files (x86)\OpenBUGS\OpenBUGS322\OpenBUGS.exe'
    print openBUGSPath
    scriptPath = '\\'.join((absolutePath + "script.txt").split('/'))
    
    
    maxFail = (seg_num + 1) * 2
    failCount = 0
    S_index = 0
    BIC = 1e6
    bestR = 0
    bestS = []
    
    beginLearning = time()
    while failCount <= (seg_num + 1) * 2:
    #while failCount <= 4:   
        print
        print "Learning model with configuration R=%d S=%s" % (R, ','.join([str(x) for x in S]) ) 
        print "\tFail count at: %d" % failCount 
        print "\tCurrent best BIC: %f" % BIC
        print "\tCurrent best configuration: R = %d --- S = %s" % (bestR, ','.join([str(x) for x in bestS]))
        
        partialStart = time()
        writeConfiguration( basePathBUGS, False, seg_num, n_max, d_max, n_samples, feat_len, R, S )
        system('"C:\Program Files (x86)\OpenBUGS\OpenBUGS322\OpenBUGS.exe" /PAR C:/Users/Christian/workspace/progetto-tesi-new/res/chairs/data/openBUGS/script.txt /HEADLESS')
        partialEnd = time()
        print "\tModel learned in %f" % (partialEnd - partialStart)
        
        newBIC = computeBIC(basePathBUGS, [n_max, ] * seg_num, [d_max, ] * seg_num, n_samples, feat_len, R, S)
        if newBIC < BIC:
            BIC = newBIC
            bestR = R
            bestS = S[:]
            if S_index == -1:
                S_index = 0
                S[S_index] += 1
            else:
                S[S_index] += 1
            #Continue on this road
            failCount = 0
        else:
            if S_index == -1:
                S_index += 1
                S[S_index] += 1
            elif S_index == len(S) - 1:
                S_index = -1
                R += 1
                S = [2, ] * seg_num
            else:
                S[S_index] -= 1
                S_index += 1
                S[S_index] += 1
            
            failCount += 1
        
    endLearning = time()
    
    print "Learning done in: " + str( endLearning - beginLearning )
    print "Best configuration: R = %d --- S = %s" % (bestR, ','.join([str(x) for x in bestS]))
    print "BIC value: %f" % float(BIC)
    
    #Scrittura modello e script per predictive posterior sampling
    writeConfiguration( basePathBUGS, True, seg_num, n_max, d_max, n_samples, feat_len, bestR, bestS )
    writePredModel(seg_num)
    writePredScript(seg_num, absolutePath)
    
    starter = open( basePathBUGS + "bestStarter.txt", "wb")
    starter.write( "%d %d %s %d" % ( seg_num, bestR, ','.join([str(x) for x in bestS]), feat_len ) )
    starter.close()
    
    #Generating samples from predictive posterior, plus other info about density
    predStart = time()
    system('"C:\Program Files (x86)\OpenBUGS\OpenBUGS322\OpenBUGS.exe" /PAR C:/Users/Christian/workspace/progetto-tesi-new/res/chairs/data/openBUGS/scriptPred.txt /HEADLESS')
    predEnd = time()
    print "Sampling done in: " + str( predEnd - predStart )
    
    Freq = 2400 # Set Frequency To 2500 Hertz
    Dur = 400 # Set Duration To 1000 ms == 1 second
    winsound.Beep(Freq,Dur)
    winsound.Beep(Freq,Dur)
        