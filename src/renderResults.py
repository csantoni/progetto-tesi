'''
Created on 30/mag/2013

@author: Christian
'''

from OpenGL.GLUT import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.arrays import vbo
from OpenGL.GL.ARB.vertex_buffer_object import *

from utility.check_extension import *

from utility.mvertex    import mVertex
from utility.mnormal    import mNormal
from utility.mtexcoord  import mTexCoord
from utility.mface      import mFace
from utility.mmesh      import mMesh

from utility.drawfunctions import *
from utility.geometryfunctions import *
from utility.vtkfunctions import *
from utility.transformations import *

from utility.gui.button import Button
from utility.gui.multibutton import MultiButton
from utility.gui.slider import Slider

from mouseInteractor import MouseInteractor

from numpy import *
import math
import vtk
from time import time
from utility import transformations


'''
    ========== GLOBAL VARIABLES & CONSTANTS ==========
'''
color_map = [
             [1.0, 0.0, 0.0],
             [0.0, 1.0, 0.0],
             [0.0, 0.0, 1.0],
             [1.0, 1.0, 0.0],
             [1.0, 0.0, 1.0],
             [0.0, 1.0, 1.0],
             [1.0, 1.0, 1.0],
             [0.2, 0.2, 0.2],
             [1.0, 1.0, 0.4],
             [0.4, 1.0, 1.0]
             ]

ESCAPE = '\033'             # Octal value for 'esc' key
SCREEN_SIZE = (800, 600)
SHAPE = ''
lastx=0
lasty=0

meshes = []

# Rendered objects
meshes_loaded = []
meshes_filler = []
contactTriangles = []
gui_objects = []

#GUI Variables
thread = None
buttonThread = None

#Render variables
drawContactPoints = False
drawContactTriangles = False

'''
    ========== CANDIDATES ==========
'''
allSamples = None
candidates = None
indices = []

# ================== R3 S2222 - HIGHEST PROBABILLITY CONFIGURATION ================== 

#candidates =   [
#    [ [ 187, 1, 2 ],[ 154, 1, 0 ],[ 168, 1, 0 ],[ 165, 1, 2 ],[ 198, 1, 0 ],[ 190, 1, 0 ],[ 164, 1, 2 ],[ 187, 1, 0 ],[ 167, 1, 0 ],[ 165, 1, 0 ] ],
#    [ [ 169, 1, 0 ],[ 179, 1, 1 ],[ 176, 1, 1 ],[ 152, 1, 0 ],[ 152, 1, 1 ],[ 194, 1, 1 ],[ 177, 1, 1 ],[ 129, 1, 1 ],[ 128, 1, 1 ],[ 171, 1, 1 ] ],
#    [ [ 176, 2, 1 ],[ 176, 2, 0 ],[ 178, 2, 0 ],[ 177, 2, 0 ],[ 177, 2, 1 ],[ 168, 2, 0 ],[ 193, 2, 0 ],[ 167, 2, 0 ],[ 193, 2, 1 ],[ 194, 2, 1 ] ],
#    [ [ 176, 2, 1 ],[ 176, 2, 0 ],[ 178, 2, 0 ],[ 177, 2, 0 ],[ 177, 2, 1 ],[ 168, 2, 0 ],[ 193, 2, 0 ],[ 167, 2, 0 ],[ 193, 2, 1 ],[ 194, 2, 1 ] ],
#    [ [ 149, 3, 0 ],[ 151, 3, 0 ],[ 129, 3, 0 ],[ 182, 3, 0 ],[ 150, 3, 0 ],[ 128, 3, 0 ],[ 146, 3, 0 ],[ 158, 3, 0 ],[ 147, 3, 1 ],[ 130, 3, 0 ] ],
#    [ [ 177, 4, 0 ],[ 145, 4, 3 ],[ 196, 4, 1 ],[ 154, 4, 0 ],[ 170, 4, 0 ],[ 143, 4, 0 ],[ 169, 4, 1 ],[ 178, 4, 1 ],[ 128, 4, 0 ],[ 195, 4, 3 ] ],
#    [ [ 196, 4, 1 ],[ 177, 4, 0 ],[ 169, 4, 1 ],[ 167, 4, 1 ],[ 145, 4, 3 ],[ 171, 4, 1 ],[ 128, 4, 0 ],[ 190, 4, 0 ],[ 193, 4, 1 ],[ 143, 4, 0 ] ]
#]
#indices = [0,1,6,8,2,0,0]
#indices = [0,1,7,5,2,0,0]
#indices = [9,9,7,1,7,2,0]

# ================== R3 S2122 ==================     
#candidates = [
#[ [ 144, 1, 0 ],[ 189, 1, 0 ],[ 132, 1, 1 ],[ 143, 1, 0 ],[ 131, 1, 1 ],[ 133, 1, 1 ],[ 185, 1, 1 ],[ 145, 1, 0 ],[ 160, 1, 0 ],[ 131, 1, 0 ] ],
#[ [ 153, 1, 1 ],[ 199, 1, 1 ],[ 153, 1, 0 ],[ 154, 1, 1 ],[ 199, 1, 0 ],[ 188, 1, 0 ],[ 188, 1, 1 ],[ 196, 1, 0 ],[ 137, 1, 3 ],[ 171, 1, 1 ] ],
#[ [ 154, 2, 1 ],[ 168, 2, 0 ],[ 154, 2, 0 ],[ 177, 2, 0 ],[ 167, 2, 0 ],[ 168, 2, 1 ],[ 176, 2, 0 ],[ 132, 2, 0 ],[ 195, 2, 2 ],[ 194, 2, 0 ] ],
#[ [ 190, 3, 1 ],[ 147, 3, 1 ],[ 147, 3, 0 ],[ 148, 3, 1 ],[ 146, 3, 0 ],[ 131, 3, 0 ],[ 148, 3, 0 ],[ 132, 3, 0 ],[ 133, 3, 0 ],[ 190, 3, 0 ] ]
#]
#indices = [7, 0, 2, 0]



# ================== R3 S2112 ================== 
candidates = [
[ [ 190, 1, 1 ],[ 198, 1, 1 ],[ 148, 1, 1 ],[ 162, 1, 0 ],[ 147, 1, 1 ],[ 198, 1, 0 ],[ 190, 1, 0 ],[ 146, 1, 1 ],[ 166, 1, 1 ],[ 148, 1, 0 ] ],
[ [ 168, 1, 0 ],[ 167, 1, 0 ],[ 154, 1, 0 ],[ 164, 1, 2 ],[ 198, 1, 0 ],[ 190, 1, 0 ],[ 187, 1, 2 ],[ 195, 1, 1 ],[ 197, 1, 1 ],[ 165, 1, 0 ] ],
[ [ 145, 2, 1 ],[ 151, 2, 4 ],[ 149, 2, 4 ],[ 151, 2, 0 ],[ 149, 2, 5 ],[ 150, 2, 3 ],[ 150, 2, 0 ],[ 144, 2, 0 ],[ 150, 2, 2 ],[ 150, 2, 5 ] ],
[ [ 186, 3, 0 ],[ 135, 3, 0 ],[ 136, 3, 0 ],[ 188, 3, 0 ],[ 134, 3, 0 ],[ 199, 3, 0 ],[ 154, 3, 0 ],[ 153, 3, 0 ],[ 152, 3, 0 ],[ 128, 3, 0 ] ],
[ [ 173, 4, 0 ],[ 194, 4, 1 ],[ 187, 4, 1 ],[ 181, 4, 1 ],[ 189, 4, 3 ],[ 175, 4, 0 ],[ 198, 4, 0 ],[ 176, 4, 1 ],[ 165, 4, 2 ],[ 128, 4, 2 ] ],
[ [ 167, 4, 0 ],[ 191, 4, 0 ],[ 196, 4, 1 ],[ 169, 4, 1 ],[ 154, 4, 0 ],[ 167, 4, 1 ],[ 170, 4, 1 ],[ 191, 4, 1 ],[ 191, 4, 3 ],[ 158, 4, 0 ] ],
]
indices = [0, 0, 0, 4, 0, 0]

candidates = [
[ [ 134, 1, 0 ],[ 183, 1, 1 ],[ 127, 1, 1 ],[ 159, 1, 1 ],[ 126, 1, 1 ],[ 158, 1, 1 ],[ 126, 1, 0 ],[ 160, 1, 1 ],[ 165, 1, 0 ],[ 165, 1, 1 ] ],
[ [ 164, 1, 0 ],[ 197, 1, 0 ],[ 165, 1, 0 ],[ 165, 1, 1 ],[ 187, 1, 0 ],[ 164, 1, 1 ],[ 187, 1, 1 ],[ 137, 1, 0 ],[ 182, 1, 0 ],[ 126, 1, 0 ] ],
[ [ 189, 1, 0 ],[ 133, 1, 1 ],[ 131, 1, 0 ],[ 143, 1, 0 ],[ 145, 1, 0 ],[ 185, 1, 1 ],[ 167, 1, 1 ],[ 175, 1, 0 ],[ 173, 1, 0 ],[ 151, 1, 0 ] ],
[ [ 151, 2, 3 ],[ 191, 2, 3 ],[ 191, 2, 2 ],[ 150, 2, 2 ],[ 149, 2, 2 ],[ 191, 2, 4 ],[ 191, 2, 1 ],[ 151, 2, 5 ],[ 191, 2, 5 ],[ 170, 2, 2 ] ],
[ [ 151, 2, 3 ],[ 191, 2, 3 ],[ 191, 2, 2 ],[ 150, 2, 2 ],[ 149, 2, 2 ],[ 191, 2, 4 ],[ 191, 2, 1 ],[ 151, 2, 5 ],[ 191, 2, 5 ],[ 170, 2, 2 ] ],
[ [ 179, 3, 0 ],[ 155, 3, 0 ],[ 157, 3, 0 ],[ 185, 3, 0 ],[ 191, 3, 0 ],[ 169, 3, 0 ],[ 168, 3, 0 ],[ 166, 3, 0 ],[ 143, 3, 0 ],[ 187, 3, 0 ] ],
[ [ 127, 4, 0 ],[ 183, 4, 0 ],[ 157, 4, 0 ],[ 153, 4, 0 ],[ 156, 4, 0 ],[ 200, 4, 0 ],[ 200, 4, 1 ],[ 157, 4, 2 ],[ 157, 4, 3 ],[ 182, 4, 0 ] ],
[ [ 144, 4, 3 ],[ 165, 4, 0 ],[ 144, 4, 2 ],[ 194, 4, 0 ],[ 173, 4, 0 ],[ 187, 4, 0 ],[ 194, 4, 1 ],[ 166, 4, 0 ],[ 164, 4, 0 ],[ 174, 4, 0 ] ],
]
indices = [0, 0, 0, 1, 2, 0, 0, 0]

#candidates = [
#[ [ 133, 1, 0 ],[ 196, 1, 1 ],[ 132, 1, 0 ],[ 169, 1, 1 ],[ 185, 1, 0 ],[ 166, 1, 0 ],[ 184, 1, 1 ],[ 185, 1, 1 ],[ 163, 1, 0 ],[ 190, 1, 0 ] ],
#[ [ 158, 1, 1 ],[ 165, 1, 0 ],[ 155, 1, 0 ],[ 165, 1, 1 ],[ 164, 1, 0 ],[ 187, 1, 2 ],[ 197, 1, 0 ],[ 166, 1, 0 ],[ 198, 1, 1 ],[ 190, 1, 1 ] ],
#[ [ 171, 2, 1 ],[ 150, 2, 0 ],[ 149, 2, 5 ],[ 150, 2, 5 ],[ 151, 2, 5 ],[ 151, 2, 0 ],[ 151, 2, 4 ],[ 149, 2, 4 ],[ 149, 2, 2 ],[ 133, 2, 0 ] ],
#[ [ 171, 2, 1 ],[ 150, 2, 0 ],[ 149, 2, 5 ],[ 150, 2, 5 ],[ 151, 2, 5 ],[ 151, 2, 0 ],[ 151, 2, 4 ],[ 149, 2, 4 ],[ 149, 2, 2 ],[ 133, 2, 0 ] ],
#[ [ 188, 3, 0 ],[ 154, 3, 0 ],[ 153, 3, 0 ],[ 199, 3, 0 ],[ 152, 3, 0 ],[ 185, 3, 0 ],[ 173, 3, 0 ],[ 174, 3, 0 ],[ 191, 3, 0 ],[ 171, 3, 0 ] ],
#[ [ 200, 4, 1 ],[ 157, 4, 1 ],[ 177, 4, 0 ],[ 171, 4, 1 ],[ 169, 4, 1 ],[ 191, 4, 0 ],[ 145, 4, 3 ],[ 167, 4, 1 ],[ 151, 4, 2 ],[ 150, 4, 2 ] ],
#]
#indices = [8, 1, 3, 3, 0, 0]
#indices = [0, 9, 4, 1, 7, 9]
#indices = [9, 8, 3, 1, 4, 6]

candidates = [
[ [ 177, 1, 0 ],[ 177, 1, 1 ],[ 178, 1, 1 ],[ 178, 1, 0 ],[ 176, 1, 0 ],[ 193, 1, 0 ],[ 179, 1, 0 ],[ 173, 1, 0 ],[ 173, 1, 1 ],[ 128, 1, 0 ] ],
[ [ 166, 1, 0 ],[ 165, 1, 1 ],[ 137, 1, 2 ],[ 137, 1, 1 ],[ 183, 1, 0 ],[ 155, 1, 0 ],[ 165, 1, 0 ],[ 126, 1, 1 ],[ 164, 1, 2 ],[ 126, 1, 0 ] ],
[ [ 150, 4, 1 ],[ 182, 4, 1 ],[ 160, 4, 1 ],[ 149, 4, 1 ],[ 151, 4, 1 ],[ 151, 4, 0 ],[ 158, 4, 1 ],[ 149, 4, 0 ],[ 148, 4, 2 ],[ 200, 4, 3 ] ],
[ [ 170, 4, 0 ],[ 195, 4, 0 ],[ 178, 4, 1 ],[ 143, 4, 0 ],[ 195, 4, 3 ],[ 189, 4, 0 ],[ 128, 4, 0 ],[ 129, 4, 0 ],[ 130, 4, 0 ],[ 170, 4, 1 ] ],
]
indices = [0, 0, 0, 0]

'''
    =========== MAIN FUNCTIONS ============
'''

def loadComponent(m, path, segment_number):
    m.loadAsComponent(path, segment_number)

def loadVBO(m):
    m.VBOVertices = vbo.VBO(m.seqVertices)
    m.VBONormals = vbo.VBO(m.normals)
    m.VBOColors = vbo.VBO(m.colors)

def loadModel(m, path, segpath):
    m.loadModel(path, segpath)
    m.VBOVertices = vbo.VBO(m.seqVertices)
    m.VBONormals = vbo.VBO(m.normals)
    m.VBOColors = vbo.VBO(m.colors)

def drawModel(m, quadric):
    global drawContactPoints, drawContactTriangles
    
    if m.VBOVertices is not None:
        m.VBOVertices.bind()
        glEnableClientState(GL_VERTEX_ARRAY)
        glVertexPointer(3, GL_FLOAT, 0, m.VBOVertices)
    
        m.VBONormals.bind()
        glEnableClientState(GL_NORMAL_ARRAY)
        glNormalPointer(GL_FLOAT, 0, m.VBONormals)
            
        if (m.VBOColors is not None):
            m.VBOColors.bind()
            glEnableClientState(GL_COLOR_ARRAY)
            glColorPointer(3, GL_FLOAT, 0, m.VBOColors)
        else:
            glDisableClientState(GL_COLOR_ARRAY)
            pass
        
        glDrawArrays(GL_TRIANGLES, 0, len(m.seqVertices))
        
        if drawContactPoints:
            points = m.contactSlots
            for data in points:
                color = color_map[ data['seg'] - 1 ]
                for p in data['points']:
                    glPushMatrix()
                    glTranslatef(*p)
                    glColor(color)
                    gluSphere(quadric,0.1,16,16)
                    glPopMatrix()
                    
        if drawContactTriangles:
            tris = m.contactSlots
            for data in tris:
                color = color_map[ data['seg'] - 1 ]
                glPushMatrix()
                glColor(color)
                glBegin(GL_TRIANGLES)
                for p in data['triangle']:
                    glVertex3f(*p)
                glEnd()
                glPopMatrix()
    
def drawBBoxes(m):
#    for s in m.components.keys():
#        for c in m.components[s]:
#    
    drawBBox(m.bbox)

def initLightning():
    #Define lightning
    lP = 7
    lA = 0.1
    lD = 0.2
    lS = 0.3
    glEnable( GL_LIGHTING )
    glEnable( GL_LIGHT0 )
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, [0.8, 0.8, 0.8, 1] )
    glLightfv( GL_LIGHT0, GL_POSITION, [lP, lP, lP, 1] )
    glLightfv( GL_LIGHT0, GL_AMBIENT, [lA, lA, lA, 1] )
    glLightfv( GL_LIGHT0, GL_DIFFUSE, [lD, lD, lD, 1] )
    glLightfv( GL_LIGHT0, GL_SPECULAR, [lS, lS, lS, 1] )
    
    glEnable( GL_LIGHT1 )
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, [0.4, 0.4, 0.4, 1] )
    glLightfv( GL_LIGHT1, GL_POSITION, [-lP, lP, lP, 1] )
    glLightfv( GL_LIGHT1, GL_AMBIENT, [lA, lA, lA, 1] )
    glLightfv( GL_LIGHT1, GL_DIFFUSE, [lD, lD, lD, 1] )
    glLightfv( GL_LIGHT1, GL_SPECULAR, [lS, lS, lS, 1] )
    
    glMaterialfv( GL_FRONT_AND_BACK, GL_AMBIENT, [lA, lA, lA, 1] )
    glMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, [lA, lA, lA, 1] )
    glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, [0.8, 0.8, 0.8, 1] )
    glMaterialf( GL_FRONT_AND_BACK, GL_SHININESS, 20 )

def loadFinalCandidate(candidates, indices, base_path):
    
    global meshes, meshes_filler, contactTriangles
    
    components = []
    for x in range(len(indices)):
        components.append(candidates[x][indices[x]])
    
    components_path = []
    
    for c in components:
        model_path = base_path + str(c[0]) + '/'
        path = model_path + "_".join(["seg", str(c[1])]) + "/" + "_".join(["comp", str(c[2])]) + "/"
        components_path.append(path)
    
    print components_path
    
    meshes = [None, ] * len(components)
    for x in range(len(components)):
        meshes[x] = mMesh(g_fVBOSupported)
    
    for x in range(len(components)):
        loadComponent(meshes[x], components_path[x], components[x][1] - 1)

    contactTriangles = {}
    for x in range(len(components)):
        for slot in meshes[x].contactSlots:
            slot['triangle'] = findBiggestTriangle(slot['points'])
    
    #Find candidates for 'glueing'
    triangleList = []
    for x in range(len(components)):
        for slot_idx, slot in enumerate(meshes[x].contactSlots):
            item = dict()
            item['componentId'] = x
            item['slotIdx'] = slot_idx
            item['from'] = components[x][1]
            item['to'] = slot['seg']
            item['matched'] = False
            item['matchedTo'] = -1
            item['moved'] = False
            triangleList.append(item)
    
    for idx_from in range(len(triangleList) - 1):
        el_from = triangleList[idx_from]
        for idx_to in range(idx_from + 1, len(triangleList)):
            el_to = triangleList[idx_to]
            if el_to['matched'] == False:
                if el_to['to'] == el_from['from']:
                    if el_from['to'] == el_to['from']:
                        el_from['matched'] = True
                        el_from['matchedTo'] = idx_to
                        el_to['matched'] = True
                        el_to['matchedTo'] = idx_from
                        break
    
    for idx, el in enumerate(triangleList):
        print str(idx) + ": " + str(el)
    
    pivotType = 2 #Selecting the 'body' segments to start the glueing process
    #pivotIdx = 0
    pivotId = 0
    for idx, el in enumerate(triangleList):
        if el['from'] == pivotType:
            #pivotIdx = idx
            pivotId = triangleList[idx]['componentId']
            break
        
    meshes_filler = []
    #First pass
    secondPassList = []
    
    toKeep = [pivotId]
    
    for el in triangleList:
        if el['componentId'] == pivotId:
            if el['moved'] == False and triangleList[ el['matchedTo'] ]['moved'] == False:
                el['moved'] = True
                triangleList[ el['matchedTo'] ]['moved'] = True
                moveComponent(el, triangleList[ el['matchedTo'] ])
                print "matched %d and %d" % (el['componentId'], triangleList[ el['matchedTo'] ]['componentId'])
                toKeep.append(triangleList[ el['matchedTo'] ]['componentId'])
                if triangleList[ el['matchedTo'] ]['componentId'] not in secondPassList:
                    secondPassList.append( triangleList[ el['matchedTo'] ]['componentId'])
    
    print "pass finished"  
    print "secondPassList"
    print secondPassList                  
    for pivId in secondPassList:
        for el in triangleList:
            if el['componentId'] == pivId:
                if el['moved'] == False:
                    el['moved'] = True
                    triangleList[ el['matchedTo'] ]['moved'] = True
                    moveComponent(el, triangleList[ el['matchedTo'] ])
                    print "matched %d and %d" % (el['componentId'], triangleList[ el['matchedTo'] ]['componentId'])
                    toKeep.append(triangleList[ el['matchedTo'] ]['componentId'])
                    
    #Cleaning un-glued
    for x in range(len(components)):
        if x not in toKeep:
            meshes[x] = None 
        
    meshes = [ m for m in meshes if m is not None ]

    

def changeCand():
    import random
    global meshes, meshes_filler, meshes_loaded, candidates, indices, gui_objects, basePath
    
    max = len(candidates[0]) - 1
    
    indices = []
    for _ in range(len(candidates)):
        indices.append( random.randint(0,max) )
    print " ========= Indices selected" + str(indices) + " ========="
    gui_objects[1].setIndices(indices)
    loadFinalCandidate(candidates, indices, basePath)
    
    for x in range(len(meshes)):
        loadVBO(meshes[x])
    
    for x in range(len(meshes_filler)):
        loadVBO(meshes_filler[x])
        
    meshes_loaded = []
    for x in range(len(meshes)):
        meshes_loaded.append(meshes[x])

def changeCand2():
    print "In change Cand2"
    global meshes, meshes_filler, meshes_loaded, candidates, basePath
    
    loadFinalCandidate(candidates, indices, basePath)
    
    for x in range(len(meshes)):
        loadVBO(meshes[x])
    
    for x in range(len(meshes_filler)):
        loadVBO(meshes_filler[x])
        
    meshes_loaded = []
    for x in range(len(meshes)):
        meshes_loaded.append(meshes[x])
    

def init(width, height):

    global g_fVBOSupported, meshes_loaded, gui_objects, mouseInteractor, quadric, meshes_filler, basePath
    
    #Check for VBOs Support
    g_fVBOSupported = IsExtensionSupported("GL_ARB_vertexbuffer_object")
    quadric = gluNewQuadric()
    #Defining all interface objects
    buttonColor = (0.7, 0.7, 0.7)
    buttonOutlineColor = (0.8, 0.8, 0.8)
    
    b1 = Button( 20, 30, 130, 30, buttonColor, buttonOutlineColor, 'Randomize')
    b1.setCallback(changeCand)
    
    b3 = Button( 160, 30, 60, 30, buttonColor, buttonOutlineColor, 'Save')
    b3.setCallback(writeToFile)
    
    global indices, candidates
    b2 = MultiButton( 230, 30, 200, 40, buttonColor, buttonOutlineColor, 'Select styles')
    b2.setIndices(indices)
    seg_list = []
    for c in candidates:
        seg_list.append( c[0][1] )
    b2.setColors(seg_list)    
    b2.setCallback(changeCand2)
    
#    sl1 = Slider( 150, SCREEN_SIZE[0] - 50, 50 )
    
    gui_objects.append(b1)
    gui_objects.append(b2)
    gui_objects.append(b3)
#    gui_objects.append(sl1)
    
    glClearColor(0.1, 0.1, 0.2, 0.0)
    
    #Define openGL rendering behaviours
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)
    glEnable(GL_COLOR_MATERIAL)

    initLightning()
    
    mouseInteractor = MouseInteractor( .01, 1 , gui_objects)

    #LOAD MODEL
    start = time()

    loadFinalCandidate(candidates, indices, basePath)
    
    for x in range(len(meshes)):
        loadVBO(meshes[x])
    
    for x in range(len(meshes_filler)):
        loadVBO(meshes_filler[x])
    
    for x in range(len(meshes)):
        meshes_loaded.append(meshes[x])
    
    print "Models loaded in %f" %(time() - start)
    print

def moveComponent(item_base, item_to_move):
    
    global meshes
    
        #  ======================= SCALING =======================
#    t_b    = meshes[ item_base['componentId'] ].contactSlots[ item_base['slotIdx'] ]['triangle']
#    t_t_m = meshes[ item_to_move['componentId'] ].contactSlots[ item_to_move['slotIdx'] ]['triangle']
#    area_b = triangleArea(*t_b)
#    area_t_m = triangleArea(*t_t_m)
#    
#    #Scaling factor
#    s_f = math.sqrt(area_b / area_t_m)
#    if s_f > 1.3:
#        s_f = 1.3
#    #s_f = 1.0
#    #print " (%d, %d)= %f" % (item_base['componentId'], item_to_move['componentId'],s_f)
#    scaling_matrix = array([
#                                  [s_f, 0.0, 0.0],
#                                  [0.0, s_f, 0.0],
#                                  [0.0, 0.0, s_f]
#                                  ])
#
#    for idx, p in enumerate(meshes[ item_to_move['componentId'] ].seqVertices):
#        meshes[ item_to_move['componentId'] ].seqVertices[idx] = dot( scaling_matrix, p ) 
#    
#    for slot in meshes[ item_to_move['componentId'] ].contactSlots:
#        for idx, p1 in enumerate(slot['points']):
#            slot['points'][idx] = dot( scaling_matrix, p1 )
#        slot['triangle'] = findBiggestTriangle(slot['points'])
    
    #  ======================= TRANSLATION =======================
    t_b    = meshes[ item_base['componentId'] ].contactSlots[ item_base['slotIdx'] ]['points']
    t_t_m = meshes[ item_to_move['componentId'] ].contactSlots[ item_to_move['slotIdx'] ]['points']
    
#    centroid_base = array([ (t_b[0][0] + t_b[1][0] + t_b[2][0]) / 3.0, 
#                                  (t_b[0][1] + t_b[1][1] + t_b[2][1]) / 3.0, 
#                                  (t_b[0][2] + t_b[1][2] + t_b[2][2]) / 3.0 ] 
#                                )
#    centroid_t_m  = array([ (t_t_m[0][0] + t_t_m[1][0] + t_t_m[2][0]) / 3.0, 
#                                  (t_t_m[0][1] + t_t_m[1][1] + t_t_m[2][1]) / 3.0, 
#                                  (t_t_m[0][2] + t_t_m[1][2] + t_t_m[2][2]) / 3.0 ]
#                                )
    
    centroid_base = mean(t_b, axis=0)
    centroid_t_m = mean(t_t_m, axis=0)
    move_vector = centroid_base - centroid_t_m 
    
    for p in meshes[ item_to_move['componentId'] ].seqVertices:
        p += move_vector
    for slot in meshes[ item_to_move['componentId'] ].contactSlots:
        for p1 in slot['points']:
            p1 += move_vector
    
    #  ======================= ROTATION =======================
#
#    t_b    = meshes[ item_base['componentId'] ].contactSlots[ item_base['slotIdx'] ]['triangle']
#    t_t_m = meshes[ item_to_move['componentId'] ].contactSlots[ item_to_move['slotIdx'] ]['triangle']
#
#    v1 = t_b
#    v0 = t_t_m
#    m = transformations.affine_matrix_from_points(v0, v1, shear=False, scale=False)
#    dec = transformations.decompose_matrix(m)
#    print "dec"
#    print dec
#    m = m[:3,:3]
#    
#    moved = dot(m, v0)
#    
#    print "Base"
#    print v1
#    print "To move"
#    print v0
#    print "Affine"
#    print m
#    print "Moved"
#    print moved
#    
#    pointsToMove = mat(meshes[ item_to_move['componentId'] ].seqVertices)
#    
#    trPoints = dot(m, pointsToMove.T)
#    trPoints = trPoints.T
#    for idx, p in enumerate(meshes[ item_to_move['componentId'] ].seqVertices):
#        meshes[ item_to_move['componentId'] ].seqVertices[idx] = trPoints[idx]

#    closeGaps(  meshes[ item_base['componentId'] ].contactSlots[ item_base['slotIdx'] ]['points'],
#                meshes[ item_to_move['componentId'] ].contactSlots[ item_to_move['slotIdx'] ]['points'],
#                item_to_move['from'] - 1)

def closeGaps(points1, points2, segNo):
    
    global meshes_filler
    
    points1 = array(points1)
    points2 = array(points2)
    points1 = concatenate( (points1, points2) )
    pts, verts = arrayToPolyPoints( points1 )
    
    polyData = vtk.vtkPolyData()
    polyData.SetPoints(pts)
    polyData.SetPolys(verts)
    
    #Clean the polydata. This will remove duplicate points that may be
    #present in the input data.
    cleaner = vtk.vtkCleanPolyData()
    cleaner.SetInput(polyData)
 
    #Generate a tetrahedral mesh from the input points. By
    #default, the generated volume is the convex hull of the points.
    delaunay3D = vtk.vtkDelaunay3D()
    delaunay3D.SetInputConnection(cleaner.GetOutputPort())
    delaunay3D.Update()
    
    surfaceFilter = vtk.vtkDataSetSurfaceFilter()
    surfaceFilter.SetInput(delaunay3D.GetOutput())
    surfaceFilter.Update() 
    polydata = surfaceFilter.GetOutput()

    triFilter = vtk.vtkTriangleFilter()
    triFilter.SetInput(polydata)
    triFilter.Update()
    polydata = triFilter.GetOutput()
    
    smoothFilter = vtk.vtkSmoothPolyDataFilter()
    smoothFilter.SetInput(polydata)
    smoothFilter.SetNumberOfIterations(2)
    smoothFilter.Update()
    polydata = smoothFilter.GetOutput()
    
    verts, faces = polyDataToSequentialArray(polydata)
    
    m = mMesh(g_fVBOSupported)
    m.loadAsFiller( verts, faces, segNo )
    meshes_filler.append( m )

def writeToFile():
    global meshes, meshes_filler
    
    final_verts = []
    final_faces = []
    
    vIndex = 0
    fIndex = 0
    
    for m in meshes:
        for idx_v in range(len(m.seqVertices)):
            if idx_v % 3 == 0:
                final_verts.append( m.seqVertices[idx_v] )
                final_verts.append( m.seqVertices[idx_v + 1] )
                final_verts.append( m.seqVertices[idx_v + 2] )
                final_faces.append( [vIndex, vIndex + 1, vIndex + 2] )
                vIndex += 3
                fIndex += 1
              
    for m in meshes_filler:
        for idx_v in range(len(m.seqVertices)):
            if idx_v % 3 == 0:
                final_verts.append( m.seqVertices[idx_v] )
                final_verts.append( m.seqVertices[idx_v + 1] )
                final_verts.append( m.seqVertices[idx_v + 2] )
                final_faces.append( [vIndex, vIndex + 1, vIndex + 2] )
                vIndex += 3
                fIndex += 1
                
    print " ===== Final mesh info ===== "
    print "Vertices: %d" % len(final_verts)
    print "Faces: %d" % len(final_faces)
    
    import random
    global sample_sig
    path = "../res/new-models/" + sample_sig + "_model_" + str(random.randrange(1000000, 9999999)) + ".off"
    f = open( path, "w" )
    
    f.write("OFF\n")
    f.write("%d %d 0\n" % (len(final_verts), len(final_faces)))

    for v in final_verts:
        f.write("%f %f %f\n" % (v[0], v[1], v[2]))
        
    for face in final_faces:
        f.write("3 %d %d %d\n" % (face[0], face[1], face[2]))
        
    f.close()
    
    print "Mesh saved in: " + path
    
def debugStuff():
    pass

def drawScene():
    
    global gui_objects, meshes_loaded, quadric
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_BLEND)
    glEnable(GL_LINE_SMOOTH)
    glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE)

    glMatrixMode( GL_PROJECTION )
    glLoadIdentity()
    xSize, ySize = glutGet( GLUT_WINDOW_WIDTH ), glutGet( GLUT_WINDOW_HEIGHT )
    gluPerspective(60, float(xSize) / float(ySize), 0.1, 1000)
    glMatrixMode( GL_MODELVIEW )
    glLoadIdentity()

    glTranslatef( 0, 0, -10 )
    mouseInteractor.applyTransformation()

    #Draw axis (for reference)
    drawAxis()
    
    #Draw all the stuff here
    x_offsets = []
    n_comp = len(meshes_loaded)
    dist = 15
    if n_comp % 2 == 0:
        start = - dist * ((n_comp)/2 + 0.5)
    else:
        start = - dist * ((n_comp -1)/2)
        
    x_offsets.append(start)
    for k in range(n_comp-1):
        x_offsets.append(start + (k+1) * dist)
            
    for m in meshes_loaded:
        glPushMatrix()
        #glTranslatef( x_offsets[i], 0, 0 )
        drawModel(m, quadric)
        if (False):
            drawBBoxes(m)
        glPopMatrix()
    
    for m in meshes_filler:
        glPushMatrix()
        drawModel(m, quadric)
        glPopMatrix()
    
    debugStuff()    
       
    #Draw all the interface here
    glDisable( GL_LIGHTING )
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0, float(xSize), float(ySize), 0, -1, 1)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    for obj in gui_objects:
        obj.draw()
    glEnable( GL_LIGHTING )
        
    glutSwapBuffers()

def resizeWindow(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0 , float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)

def mainLoop():
    glutInit(sys.argv)
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH )
    glutInitWindowSize(*SCREEN_SIZE)
    glutInitWindowPosition(1000, 200)

    window = glutCreateWindow("MyMesh 0.1")
    
    init(*SCREEN_SIZE)
    mouseInteractor.registerCallbacks()

    glutDisplayFunc(drawScene)
    glutIdleFunc(drawScene)
    glutReshapeFunc(resizeWindow)

    glutMainLoop()
    
if __name__ == "__main__":
    
    basePath = "../res/chairs/data/"
    basePathBUGS = "../res/chairs/data/openBUGS/"
    #global allSamples, candidates, indices
    
#    allSamples = load("samples/samples_R3_S2222.npy") 
#    allSamples = load("samples/samples_R3_S2212.npy")
#    allSamples = load("samples/samples_R1_S2122.npy") # 1 
#    allSamples = load("samples/samples_R1_S2222.npy") #3 5 
#    allSamples = load("samples/samples_R1_S2121.npy")
    
    allSamples = load(basePathBUGS + "samples_R2_S122.npy") #
    
    global sample_sig
    sample_sig = "chairs_R2_S122"
    
    sampleNumber = 20
    candidates = allSamples[sampleNumber]
    indices = [0, ] * (len(candidates))
    
    #indices = [2, 5, 3, 2, 4, 6, 7] #0
    print "Sample loaded: " + str(sampleNumber)
    
    for c in candidates:
        s = ""
        for e in c:
            s += str(e) + " "
        print s
    print "Configuration: " + str(indices)
    
    mainLoop()
