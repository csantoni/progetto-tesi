'''
Created on 30/mag/2013

@author: Christian
'''

from OpenGL.GLUT import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.arrays import vbo
from OpenGL.GL.ARB.vertex_buffer_object import *

from utility.check_extension import *

from utility.mvertex    import mVertex
from utility.mnormal    import mNormal
from utility.mtexcoord  import mTexCoord
from utility.mface      import mFace
from utility.mmesh      import mMesh

from utility.drawfunctions import *
from utility.geometryfunctions import *
from utility.vtkfunctions import *

from utility.gui.button import Button
from utility.gui.multibutton import MultiButton
from utility.gui.slider import Slider

from mouseInteractor import MouseInteractor

from numpy import *
import math
import vtk
from time import time
from os import walk


'''
    ========== GLOBAL VARIABLES & CONSTANTS ==========
'''
color_map = [
             [1.0, 0.0, 0.0],
             [0.0, 1.0, 0.0],
             [0.0, 0.0, 1.0],
             [1.0, 1.0, 0.0],
             [1.0, 0.0, 1.0],
             [0.0, 1.0, 1.0],
             [1.0, 1.0, 1.0],
             [0.2, 0.2, 0.2],
             [1.0, 1.0, 0.4],
             [0.4, 1.0, 1.0]
             ]

ESCAPE = '\033'             # Octal value for 'esc' key
SCREEN_SIZE = (800, 600)
SHAPE = ''
lastx=0
lasty=0

#modelDirectory = "../res/new-models/R1_S2122/"
modelDirectory = "../res/new-models/R3_S2222/"
modelDirectory = "../res/chairs/shapes/"
meshes = []

# Rendered objects
meshes_loaded = []
meshes_filler = []
contactTriangles = []
gui_objects = []

#GUI Variables
thread = None
buttonThread = None

#Render variables
drawContactPoints = False
drawContactTriangles = False

'''
    ========== CANDIDATES ==========
'''
allSamples = None
candidates = None
indices = []

'''
    =========== MAIN FUNCTIONS ============
'''

def loadComponent(m, path, color):
    #m.loadAsComponent(path, segment_number)
    m.loadAsGenModel(path, color)

def loadVBO(m):
    m.VBOVertices = vbo.VBO(m.seqVertices)
    m.VBONormals = vbo.VBO(m.normals)
    m.VBOColors = vbo.VBO(m.colors)

def loadModel(m, path, segpath):
    m.loadModel(path, segpath)
    m.VBOVertices = vbo.VBO(m.seqVertices)
    m.VBONormals = vbo.VBO(m.normals)
    m.VBOColors = vbo.VBO(m.colors)

def drawModel(m, quadric):
    global drawContactPoints, drawContactTriangles
    
    if m.VBOVertices is not None:
        m.VBOVertices.bind()
        glEnableClientState(GL_VERTEX_ARRAY)
        glVertexPointer(3, GL_FLOAT, 0, m.VBOVertices)
    
        m.VBONormals.bind()
        glEnableClientState(GL_NORMAL_ARRAY)
        glNormalPointer(GL_FLOAT, 0, m.VBONormals)
            
        if (m.VBOColors is not None):
            m.VBOColors.bind()
            glEnableClientState(GL_COLOR_ARRAY)
            glColorPointer(3, GL_FLOAT, 0, m.VBOColors)
        else:
            glDisableClientState(GL_COLOR_ARRAY)
            pass
        
        glDrawArrays(GL_TRIANGLES, 0, len(m.seqVertices))
        
        if drawContactPoints:
            points = m.contactSlots
            for data in points:
                color = color_map[ data['seg'] - 1 ]
                for p in data['points']:
                    glPushMatrix()
                    glTranslatef(*p)
                    glColor(color)
                    gluSphere(quadric,0.1,16,16)
                    glPopMatrix()
                    
        if drawContactTriangles:
            tris = m.contactSlots
            for data in tris:
                color = color_map[ data['seg'] - 1 ]
                glPushMatrix()
                glColor(color)
                glBegin(GL_TRIANGLES)
                for p in data['triangle']:
                    glVertex3f(*p)
                glEnd()
                glPopMatrix()
    
def drawBBoxes(m):
    for s in m.components.keys():
        for c in m.components[s]:
            drawBBox(c.bbox)

def initLightning():
    #Define lightning
    lP = 7
    lA = 0.1
    lD = 0.2
    lS = 0.3
    glEnable( GL_LIGHTING )
    glEnable( GL_LIGHT0 )
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, [0.8, 0.8, 0.8, 1] )
    glLightfv( GL_LIGHT0, GL_POSITION, [lP, lP, lP, 1] )
    glLightfv( GL_LIGHT0, GL_AMBIENT, [lA, lA, lA, 1] )
    glLightfv( GL_LIGHT0, GL_DIFFUSE, [lD, lD, lD, 1] )
    glLightfv( GL_LIGHT0, GL_SPECULAR, [lS, lS, lS, 1] )
    
    glEnable( GL_LIGHT1 )
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, [0.4, 0.4, 0.4, 1] )
    glLightfv( GL_LIGHT1, GL_POSITION, [-lP, lP, lP, 1] )
    glLightfv( GL_LIGHT1, GL_AMBIENT, [lA, lA, lA, 1] )
    glLightfv( GL_LIGHT1, GL_DIFFUSE, [lD, lD, lD, 1] )
    glLightfv( GL_LIGHT1, GL_SPECULAR, [lS, lS, lS, 1] )
    
    glMaterialfv( GL_FRONT_AND_BACK, GL_AMBIENT, [lA, lA, lA, 1] )
    glMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, [lA, lA, lA, 1] )
    glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, [0.8, 0.8, 0.8, 1] )
    glMaterialf( GL_FRONT_AND_BACK, GL_SHININESS, 20 )

def init(width, height):

    global g_fVBOSupported, meshes_loaded, gui_objects, mouseInteractor, quadric, meshes_filler
    
    #Check for VBOs Support
    g_fVBOSupported = IsExtensionSupported("GL_ARB_vertexbuffer_object")
    quadric = gluNewQuadric()
    #Defining all interface objects
    buttonColor = (0.7, 0.7, 0.7)
    buttonOutlineColor = (0.8, 0.8, 0.8)
    
    sl1 = Slider( 300,  20, 20 )
    
    gui_objects.append(sl1)
    
    glClearColor(0.1, 0.1, 0.2, 0.0)
    
    #Define openGL rendering behaviours
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)
    glEnable(GL_COLOR_MATERIAL)

    initLightning()
    
    mouseInteractor = MouseInteractor( .01, 1 , gui_objects)

    #LOAD MODEL
    start = time()

    global modelDirectory

    print "Loading from " + modelDirectory
    fileList = []
    components_paths = []
    for (dirpath, dirname, filenames) in walk(modelDirectory):
        fileList.extend(filenames)
        break

    print fileList
    
    for f in fileList:
        path = modelDirectory + f
        print path
        components_paths.append(path)
        
    meshes = [None, ] * len(components_paths)
    for x in range(len(components_paths)):
        meshes[x] = mMesh(g_fVBOSupported)
    
    for x in range(len(components_paths)):
        loadComponent(meshes[x], components_paths[x], ( 0.5, 0.5, 0.5 ) )

    for x in range(len(meshes)):
        loadVBO(meshes[x])
    
    for x in range(len(meshes)):
        meshes_loaded.append(meshes[x])
    
    print "Models loaded in %f" %(time() - start)
    print

def debugStuff():
    pass

def drawScene():
    
    global gui_objects, meshes_loaded, quadric
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_BLEND)
    glEnable(GL_LINE_SMOOTH)
    glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE)

    glMatrixMode( GL_PROJECTION )
    glLoadIdentity()
    xSize, ySize = glutGet( GLUT_WINDOW_WIDTH ), glutGet( GLUT_WINDOW_HEIGHT )
    gluPerspective(60, float(xSize) / float(ySize), 0.1, 1000)
    glMatrixMode( GL_MODELVIEW )
    glLoadIdentity()

    glTranslatef( 0, 0, -40 )
    mouseInteractor.applyTransformation()

    #Draw axis (for reference)
    drawAxis()
    
    #Draw all the stuff here
    x_offsets = []
    n_comp = len(meshes_loaded)
    dist = 5
    n_comp = 12
    if n_comp % 2 == 0:
        start = - dist * ((n_comp)/2 + 0.5)
    else:
        start = - dist * ((n_comp -1)/2)
        
    x_offsets.append(start)
    for k in range(n_comp-1):
        x_offsets.append(start + (k+1) * dist)
    
    i = 0       
    for m in meshes_loaded:
        glPushMatrix()
        glTranslatef( x_offsets[i % n_comp], int(i / n_comp) * 10, 0 )
        i += 1
        drawModel(m, quadric)
        if (False):
            drawBBoxes(m)
        glPopMatrix()
    
    debugStuff()    
       
    #Draw all the interface here
    glDisable( GL_LIGHTING )
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0, float(xSize), float(ySize), 0, -1, 1)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    for obj in gui_objects:
        obj.draw()
    glEnable( GL_LIGHTING )
        
    glutSwapBuffers()

def resizeWindow(width, height):
    if height == 0:
        height = 1

    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0 , float(width)/float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)

def mainLoop():
    glutInit(sys.argv)
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH )
    glutInitWindowSize(*SCREEN_SIZE)
    glutInitWindowPosition(1000, 200)

    window = glutCreateWindow("MyMesh 0.1")
    
    init(*SCREEN_SIZE)
    mouseInteractor.registerCallbacks()

    glutDisplayFunc(drawScene)
    glutIdleFunc(drawScene)
    glutReshapeFunc(resizeWindow)

    glutMainLoop()
    
if __name__ == "__main__":
    mainLoop()
